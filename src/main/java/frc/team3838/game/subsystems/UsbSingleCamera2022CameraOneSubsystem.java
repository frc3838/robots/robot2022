package frc.team3838.game.subsystems;

import javax.annotation.Nonnull;

import frc.team3838.core.subsystems.Abstract3838SingleUsbCameraSubsystem;



public class UsbSingleCamera2022CameraOneSubsystem extends Abstract3838SingleUsbCameraSubsystem
{
    
    private static UsbSingleCamera2022CameraOneSubsystem INSTANCE;
    
    
    private UsbSingleCamera2022CameraOneSubsystem()
    {
        super(1);
    }
    
    
    @Nonnull
    public static UsbSingleCamera2022CameraOneSubsystem getInstance()
    {
        // "Double Checked Locking" implementation that provides quick access but with thread safe initialization,
        // and eliminates potential subtle initialization sequence bugs Eager initialization may cause
        // See Method 4 at https://www.geeksforgeeks.org/singleton-design-pattern/
    
        // Fast (non-synchronized) check to reduce overhead of acquiring a lock when it's not needed
        if (INSTANCE == null)
        {
            // Make thread safe
            synchronized (UsbDualCamera2022Subsystem.class)
            {
                // Check nullness again as multiple threads can reach above null check
                if (INSTANCE == null)
                {
                    INSTANCE = new UsbSingleCamera2022CameraOneSubsystem();
                }
            }
        }
        return INSTANCE;
    }
}

