package frc.team3838.game.subsystems

import com.revrobotics.CANSparkMax
import com.revrobotics.CANSparkMaxLowLevel
import frc.team3838.core.hardware.createCanSparkMax
import frc.team3838.core.subsystems.Abstract3838Subsystem
import frc.team3838.core.utils.NO_OP
import frc.team3838.game.RobotMap


object BallCollectorSubsystem: Abstract3838Subsystem()
{
    // MITCH: Set intake speed here
    const val AUTO_DEFAULT_INTAKE_MOTOR_SPEED = 0.60
    const val TELEOP_DEFAULT_INTAKE_MOTOR_SPEED = 1.00

    private var intakeMotor: CANSparkMax? = null

    override fun initSubsystem()
    {
        intakeMotor = createCanSparkMax(RobotMap.CANs.COLLECTOR_INTAKE_MOTOR, invert = true, idleMode = CANSparkMax.IdleMode.kBrake, motorType = CANSparkMaxLowLevel.MotorType.kBrushless)
        BallSystems.tab.apply {

        }
    }

    override fun initDefaultCommandImpl() = NO_OP()


    @JvmOverloads
    fun startIntakeMotor(intakeSpeed: Double = TELEOP_DEFAULT_INTAKE_MOTOR_SPEED)
    {
        intakeMotor?.set(intakeSpeed)
    }

    fun reverseIntakeMotor()
    {
        intakeMotor?.set(-TELEOP_DEFAULT_INTAKE_MOTOR_SPEED)
    }

    fun stopIntakeMotor()
    {
        intakeMotor?.stopMotor()
    }
}