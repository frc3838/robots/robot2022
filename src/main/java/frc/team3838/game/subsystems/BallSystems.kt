package frc.team3838.game.subsystems

import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab


object BallSystems
{
    @Suppress("MemberVisibilityCanBePrivate")
    const val tabName = "Ball System"

    @JvmField
    val tab: ShuffleboardTab = Shuffleboard.getTab(tabName)
}