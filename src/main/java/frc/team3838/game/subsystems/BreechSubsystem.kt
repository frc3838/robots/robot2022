package frc.team3838.game.subsystems

import com.revrobotics.CANSparkMax
import com.revrobotics.CANSparkMaxLowLevel
import com.revrobotics.REVLibError
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab
import frc.team3838.core.dashboard.addBooleanBox
import frc.team3838.core.hardware.LimitSwitch
import frc.team3838.core.hardware.createCanSparkMax
import frc.team3838.core.subsystems.Abstract3838Subsystem
import frc.team3838.core.utils.NO_OP
import frc.team3838.core.utils.PIDSetting
import frc.team3838.core.utils.format
import frc.team3838.game.RobotMap


object BreechSubsystem: Abstract3838Subsystem()
{
    @JvmField
    val tab: ShuffleboardTab = Shuffleboard.getTab("Breech") // Shooter2022Subsystem.tab

    /** The number of rotations for a full cycle of the cam on the breech. */
    private const val rotationsPerFullCycle: Double = 5.0
    /** The next -- during a shoot sequence -- or last -- after a shoot sequence --  target position of the cam for a full shoot cycle. */
    private var targetPosition: Double = 0.0


    /** The ± delta  the current position must be within to be considered a succesful full cycle by the [completedLastCycle] function. */
    private val positionSensitivity = 0.2
    private val breechMotorPidSetting = PIDSetting("Breech", 0.1, 0.00, 0.0, 0.0)
    private const val breachPidMaxOutput = 0.15
    private const val breachPidMinOutput = -breachPidMaxOutput
    private var breechMotor: CANSparkMax? = null
    private var breechArmSensor:LimitSwitch = LimitSwitch.createNoOp(false)

    var lastShootStartTime = 0L
        private set
    // Average shot time is about 240. We set to 250 as a little buffer of being in one more 20ms loop
    private const val shotDurationTimeInMs = 250L

    //private val executor = Executors.newScheduledThreadPool(1)

    //    /** The 'exact' position we want to move to. */
//    private var desiredPosition = 0.0
    var currentPosition: Double = 0.0
        private set
        get() = breechMotor?.encoder?.position ?: 0.0

    /** 'SI Shooter Ball In' Sensor that indicates the ball is in the shooter breech/chamber. */
    private var ballInSensor: LimitSwitch = LimitSwitch.createNoOp(false)

    override fun initDefaultCommandImpl() = NO_OP()

    override fun initSubsystem()
    {
        ballInSensor = LimitSwitch.create(RobotMap.DIOs.BALL_IN_SHOOTER_IR_SENSOR, invert = true)
        breechArmSensor = LimitSwitch.create(RobotMap.DIOs.BREECH_ARM_HOME_POSITION_SENSOR, invert = false)
        breechMotor = createCanSparkMax(RobotMap.CANs.BREECH_LIFT_MOTOR, CANSparkMaxLowLevel.MotorType.kBrushless, invert = false, CANSparkMax.IdleMode.kBrake)
        configureBreechPid()
        breechMotor?.encoder?.position = 0.0
        tab.apply {
            addBooleanBox("isShooting") { isShooting() }
            addBooleanBox("haveBall") { haveBall() }
            addBooleanBox("breechHome") { breechIsHome() }
            addString("breechTargetPosition") { targetPosition.format(3) }
            addString("breechCurrentPosition") { currentPosition.format(3) }
//            addString("desiredPosition") { desiredPosition.format(3) }
            addString("breechSpeed") { (breechMotor?.get() ?: 0.0).format(3) }
        }

        // Check to determine how long it takes to shoot. Average is 240ms
        /*val r = object: Runnable {
            private var startTime = 0L

            override fun run()
            {
                if (startTime == 0L && isShooting())
                {
                    startTime = System.currentTimeMillis()
                }
                else if (startTime != 0L && !isShooting())
                {
                    val endTime = System.currentTimeMillis()
                    logger.info { "Shot took ${endTime - startTime}" }
                    startTime = 0L
                }
            }
        }
        executor.scheduleAtFixedRate(r, 2, 2, TimeUnit.MILLISECONDS)*/

    }

    /**
     * Resets the encoder's position to zero, iff the motor is stopped (to prevent a loss of state in knowing where the arm is).
     *
     * @param force If `true`, forces a reset of the encoder's position to zero, regardless if the motor is moving or not.
     *              Default is `false`. This parameter must be used with care and is primarily meant for testing and/or homing.
     */
    @JvmOverloads
    fun resetPosition(force: Boolean = false)
    {
        if (force || breechMotor?.get() == 0.0)
        {
            breechMotor?.encoder?.position = 0.0
        }
    }

    fun stopMotor()
    {
        breechMotor?.stopMotor()
        // TODO: do we need to set the pidController reference to the current position to prevent a restart?
    }

    fun breechIsHome(): Boolean = breechArmSensor.isActivated

    fun haveBall(): Boolean = ballInSensor.isActivated

    fun isShooting() : Boolean = breechMotor?.get() != 0.0 || ((breechMotor?.outputCurrent ?: 0.0) > 0.5)

    fun completedLastCycle(): Boolean
    {
//        val min = targetPosition - positionSensitivity
//        val max = targetPosition + positionSensitivity
//        val result = currentPosition in min..max
//        logger.debug { "Completed last cycle calculation: is currentPosition of: ${currentPosition.format(3)} between ${min.format(3)} and ${max.format(3)}. (targetPosition: ${targetPosition.format(3)} Result: $result" }
//        return result
        return System.currentTimeMillis() - lastShootStartTime >= shotDurationTimeInMs
    }

    fun completedLastCycleAndNotShooting(): Boolean = !isShooting() && completedLastCycle()

    /**
     * Starts the shooting sequence, iff one is not currently running. If a shoot sequence is already running, no action is taken.
     * If the previous cycle completed successfully, a new target position is calculated and used. Otherwise, the cycle continues to
     * the previous target position.
     *
     *  @param forceStart If `true`, forces the shoot sequence to occur. This can result in the loss of state of knowing the correct
     *                    position of the shooter cam, and therefore must be used with care and is primarily meant for testing and/or homing.
     *
     * @param forceNewTargetPosition If `true`
     */
    @JvmOverloads
    fun cycleBreechToShoot(forceStart: Boolean = false, forceNewTargetPosition: Boolean = false)
    {
        val completedLastCycle = completedLastCycle()
        val isShooting = isShooting()
        logger.debug { "Breech cycling starting. On entry: isShooting: $isShooting  completedLastCycle: $completedLastCycle forceStart: $forceStart forceNewTargetPosition: $forceNewTargetPosition" }
        if (!isShooting || forceStart)
        {
            // Set the setpoint/reference for a full cycle
            //targetPosition = if (completedLastCycle || forceNewTargetPosition) currentPosition + rotationsPerFullCycle else targetPosition
            targetPosition = if (completedLastCycle || forceNewTargetPosition) targetPosition + rotationsPerFullCycle  else targetPosition
            // the pidController will start the motor upon getting the new position
            setPosition(targetPosition)
            lastShootStartTime = System.currentTimeMillis()
            logger.debug { "Breech cycling starting with target position of: ${targetPosition.format(3)} isShooting: ${isShooting()} " }
        }
    }

    private fun setPosition(targetPosition: Double)
    {
        val revLibError = breechMotor?.pidController?.setReference(targetPosition, CANSparkMax.ControlType.kPosition)
        if (revLibError != REVLibError.kOk)
        {
            logger.error { "An error occurred when attempting to set breech position to ${targetPosition.format(3)}. Error Code: $revLibError" }
        }
    }

    private fun configureBreechPid()
    {
        val pidController = breechMotor!!.pidController
        pidController.p = breechMotorPidSetting.p
        pidController.i = breechMotorPidSetting.i
        pidController.d = breechMotorPidSetting.d
        pidController.ff = breechMotorPidSetting.f
        pidController.setOutputRange(breachPidMinOutput, breachPidMaxOutput)
    }
}

