package frc.team3838.game.subsystems

import com.google.common.collect.ImmutableSet
import frc.team3838.core.commands.AbstractInstant3838Command
import frc.team3838.core.subsystems.I3838Subsystem

fun addAllMotorTestCommands()
{
    MotorTestSubsystem.tab.apply {
        add(StartTestMotorAtConfiguredSpeedCommand)
        add(StopTestMotorCommand)
    }
}

abstract class AbstractMotorTestInstantCommand(name: String? = null) : AbstractInstant3838Command(name)
{
    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of(MotorTestSubsystem)
}
/** Starts the motor at the speed configured via the 'Speed' setting box on the Shuffleboard */
object StartTestMotorAtConfiguredSpeedCommand: AbstractMotorTestInstantCommand("StartMotorAtSpeed")
{
    override fun executeImpl() = MotorTestSubsystem.startMotor()
}
object StopTestMotorCommand: AbstractMotorTestInstantCommand("StopMotor")
{
    override fun executeImpl() = MotorTestSubsystem.stopMotor()
}


//class StartMotorTestUpperMotorCommand: AbstractMotorTestInstantCommand()
//{
//
//    override fun executeImpl()
//    {
//        logger.info { "Calling startUpperMotor()" }
//        MotorTestSubsystem.startUpperMotor()
//    }
//
//}
//class StartMotorTestLowerMotorCommand: AbstractMotorTestInstantCommand()
//{
//
//    override fun executeImpl()
//    {
//        logger.info { "Calling startLowerMotor()" }
//        MotorTestSubsystem.startLowerMotor()
//    }
//
//}
//
//class StopMotorTestMotorsCommand: AbstractMotorTestInstantCommand()
//{
//    override fun executeImpl()
//    {
//        logger.info { "Calling stopMotors()" }
//        MotorTestSubsystem.stopMotors()
//    }
//
//}


