package frc.team3838.game.subsystems;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import frc.team3838.core.RobotProperties;
import frc.team3838.core.hardware.CANSparkMaxPair;
import frc.team3838.core.hardware.EncoderConfig;
import frc.team3838.core.hardware.NoOpMotorController;
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem;
import frc.team3838.core.subsystems.drive.DriveControlMode;
import frc.team3838.game.RobotMap;
import frc.team3838.game.RobotMap.CANs;
import frc.team3838.game.RobotMap.UI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class DriveTrainSubsystem extends Abstract3838DriveTrainSubsystem
{
    private static final Logger logger = LoggerFactory.getLogger(DriveTrainSubsystem.class);

    private final double encoderDistancePerPulse = RobotProperties.getEncoderDistancePerPulse();


    // TODO - need to configure
    // STALL SPEED is 0.17 in both directions


    @SuppressWarnings("FieldCanBeLocal")
    private final ScheduledExecutorService scheduledExecutorService;
    
    
    /**
     * Creates a new instance of this DriveTrainSubsystem.
     * This constructor is private since this class is a Singleton. External classes
     * should use the {@link #getInstance()} method to get the instance.
     * For example, instead of doing this:
     * <pre>
     *     DriveTrainSubsystem driveTrainSubsystem = new DriveTrainSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     DriveTrainSubsystem driveTrainSubsystem = DriveTrainSubsystem.getInstance();
     * </pre>
     */
    private DriveTrainSubsystem()
    {
        // Leaders flash blue
        // Followers flash magenta
        super(RobotMap.isDriveTrainEnabled
              ? new CANSparkMaxPair(CANs.DRIVE_LEFT_SIDE_REAR, CANs.DRIVE_LEFT_SIDE_FRONT, true, false, MotorType.kBrushless, IdleMode.kBrake)
              : NoOpMotorController.getInstance(),
              RobotMap.isDriveTrainEnabled
              ? new CANSparkMaxPair(CANs.DRIVE_RIGHT_SIDE_REAR, CANs.DRIVE_RIGHT_SIDE_FRONT, true, false, MotorType.kBrushless, IdleMode.kBrake)
              : NoOpMotorController.getInstance(),
              UI.getDriveTrainControlConfiguration()
        );
//        try
//        {
//            this.leftMotorOps = new MotorOps(getLeftSpeedController(), false);
//            this.rightMotorOps = new MotorOps(getRightSpeedController(), false);
//        }
//        catch (Exception e)
//        {
//            logger.error("An exception occurred when initialing MotorOps fields in DriveTrainSubsystem constructor. "
//                         + "DriveTrainSubsystem will be disabled. Cause Summary: {}", e.toString(), e);
//            disableSubsystem();
//        }
        scheduledExecutorService = Executors.newScheduledThreadPool(1);
        scheduledExecutorService.scheduleAtFixedRate(() -> {
            final DriverControlConfig2022 latestConfig = UI.getDriveTrainControlConfiguration();
            if (!getDriveTrainControlConfig().equals(latestConfig))
            {
                setDriveTrainControlConfig(latestConfig);
                logger.info("Switching Control Mode to {}", latestConfig);
            }

        }, 0, 20, TimeUnit.MILLISECONDS);

    }



    @SuppressWarnings("RedundantThrows")
    @Override
    protected void initSubsystem() throws Exception
    {
        logger.debug("DriveTrainSubsystem.initSubsystem() running");
        setCurveFactor(2);
        setShouldCurveInput(true);
        initEncoders();
        initDashboard();
        logger.debug("DriveTrainSubsystem.initSubsystem() completed");
    }


    @Nullable
    @Override
    protected EncoderConfig getLeftEncoderConfig()
    {
        logger.error("getLeftEncoderConfig() unexpectedly called for 2022 robot. Returning null");
        return null;
    }


    @Nullable
    @Override
    protected EncoderConfig getRightEncoderConfig()
    {
        logger.error("getRightEncoderConfig() unexpectedly called for 2022 robot. Returning null");
        return null;
    }


    @Override
    public double getMinSpeedForAutonomous() { return 0.10; }


    @Override
    public double getMaxSpeedForAutonomous() { return 0.50; }


    protected double getFineControlMaxAbsoluteSpeed()
    {
        return (getDriveTrainControlConfig().getDriveControlMode() == DriveControlMode.Arcade) ? 0.5 : 0.57;
    }
    
    
    /** The Singleton instance of this DriveTrainSubsystem. External classes should use the {@link #getInstance()} method to get the instance. */
    private static DriveTrainSubsystem INSTANCE;
    private static AtomicBoolean initFailed = new AtomicBoolean(false);
    
    
    private static final ReentrantLock lock = new ReentrantLock();
    
    /**
     * Returns the Singleton instance of this DriveTrainSubsystem. This static method
     * should be used by external classes, rather than the constructor
     * to get the instance of this class.
     * <pre>
     *     DriveTrainSubsystem driveTrainSubsystem = new DriveTrainSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     DriveTrainSubsystem driveTrainSubsystem = DriveTrainSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    @Nonnull
    public static DriveTrainSubsystem getInstance()
    {
        // "Double Checked Locking" implementation that provides quick access but with thread safe initialization,
        // and eliminates potential subtle initialization sequence bugs Eager initialization may cause
        // See Method 4 at https://www.geeksforgeeks.org/singleton-design-pattern/
        
        // Fast (non-synchronized) check to reduce overhead of acquiring a lock when it's not needed
        if (INSTANCE == null)
        {
            // Make thread safe
            synchronized (DriveTrainSubsystem.class)
            {
                lock.lock();
                try
                {
                    // Check nullness again as multiple threads can reach above null check
                    if (INSTANCE == null)
                    {
                        try
                        {
                            if (!initFailed.get())
                            {
                                INSTANCE = new DriveTrainSubsystem();
                            }
                        }
                        catch (Throwable t)
                        {
                            initFailed.set(true);
                            //noinspection UnnecessaryToStringCall
                            logger.error(">>>>>FATAL ERROR: An exception occurred when initializing the DriveTrainSubsystem. Cause summary: {}", t.toString(), t);
                            throw t;
                        }
                        
                    }
                }
                finally
                {
                   lock.unlock();
                }
            }
        }
        return INSTANCE;
    }
    
}
