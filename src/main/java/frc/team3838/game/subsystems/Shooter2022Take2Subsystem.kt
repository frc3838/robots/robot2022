package frc.team3838.game.subsystems

import com.ctre.phoenix.ErrorCode
import com.ctre.phoenix.motorcontrol.ControlMode
import com.ctre.phoenix.motorcontrol.FeedbackDevice
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX
import com.google.common.collect.ImmutableSet
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard
import frc.team3838.core.commands.AbstractInstant3838Command
import frc.team3838.core.dashboard.addIntSettingTextBox
import frc.team3838.core.meta.API
import frc.team3838.core.subsystems.Abstract3838Subsystem
import frc.team3838.core.subsystems.I3838Subsystem
import frc.team3838.game.RobotMap.CANs
import kotlin.math.pow

object Shooter2022Take2Subsystem : Abstract3838Subsystem()
{
    private var upperTalon: WPI_TalonSRX? = null
    private var lowerTalon: WPI_TalonSRX? = null
    private var dbSpeedSetting = 0
    val tab = Shuffleboard.getTab("Shooter2020_B")

    var isShooterRunning: Boolean = false
        private set

    @Throws(Exception::class)
    override fun initDefaultCommandImpl()
    {
    }

    @API
    fun calcNeededRpmForDistance(distancesToGoal: Double): Double = (0.0264 * distancesToGoal.pow(2)) - (0.1691 * distancesToGoal) + 1588.7

    @JvmOverloads
    fun runShooterAtSpeed(speed: Double = 775.0)
    {
        logger.debug { "Running Shooter at: $speed" }
        upperTalon!![ControlMode.Velocity] = speed * 1.00
        // MITCH: Change shooter upper/lower ratio here
        lowerTalon!![ControlMode.Velocity] = speed * 0.85
        isShooterRunning = true
    }

    fun stopShooter()
    {
        isShooterRunning = false
        upperTalon?.stopMotor()
        lowerTalon?.stopMotor()
    }

    override fun initSubsystem()
    {
        upperTalon = WPI_TalonSRX(CANs.SHOOTER_2022_TOP_MOTOR)
        lowerTalon = WPI_TalonSRX(CANs.SHOOTER_2022_BOT_MOTOR)

        val upperSfErrorCode = upperTalon!!.clearStickyFaults(Constants.kTimeoutMs)
        val lowerSfErrorCode = lowerTalon!!.clearStickyFaults(Constants.kTimeoutMs)
        
        if (upperSfErrorCode != ErrorCode.OK || lowerSfErrorCode != ErrorCode.OK)
        {
            logger.warn { "When clearing sticky faults, received non OK code. upperErrorCode: $upperSfErrorCode  lowerErrorCode: $lowerSfErrorCode" }
        }

        /* Factory Default all hardware to prevent unexpected behaviour */
        val upperResetErrorCode = upperTalon!!.configFactoryDefault()
        val lowerResetErrorCode = lowerTalon!!.configFactoryDefault()

        if (upperResetErrorCode != ErrorCode.OK || lowerResetErrorCode != ErrorCode.OK)
        {
            logger.warn { "When setting talons to Factory Default, received non OK code. upperErrorCode: $upperResetErrorCode  lowerErrorCode: $lowerResetErrorCode" }
        }
        
        /* Config sensor used for Primary PID [Velocity] */
        upperTalon!!.configSelectedFeedbackSensor(FeedbackDevice.Tachometer,
                                                  Constants.kPIDLoopIdx,
                                                  Constants.kTimeoutMs)
        lowerTalon!!.configSelectedFeedbackSensor(FeedbackDevice.Tachometer,
                                                  Constants.kPIDLoopIdx,
                                                  Constants.kTimeoutMs)

        /* 1 Edge per cycle (WHITE-black) */
        val edgesPerCycle = 1
        //tachTalonHigh.configPulseWidthPeriod_EdgesPerRot(edgesPerCycle, kTimeoutMs);
        //tachTalonLow.configPulseWidthPeriod_EdgesPerRot(edgesPerCycle, kTimeoutMs);
        upperTalon!!.configPulseWidthPeriod_EdgesPerRot(edgesPerCycle, Constants.kTimeoutMs)
        lowerTalon!!.configPulseWidthPeriod_EdgesPerRot(edgesPerCycle, Constants.kTimeoutMs)
        val feedbackCoef = 1.0
        //tachTalonHigh.configSelectedFeedbackCoefficient(feedbackCoef,0,kTimeoutMs);
        //tachTalonLow.configSelectedFeedbackCoefficient(feedbackCoef,0,kTimeoutMs);
        upperTalon!!.configSelectedFeedbackCoefficient(feedbackCoef, 0, Constants.kTimeoutMs)
        lowerTalon!!.configSelectedFeedbackCoefficient(feedbackCoef, 0, Constants.kTimeoutMs)


        /* Additional filtering if need be */
        val filterWindowSize = 1
        //tachTalonHigh.configPulseWidthPeriod_FilterWindowSz(filterWindowSize, kTimeoutMs);
        //tachTalonLow.configPulseWidthPeriod_FilterWindowSz(filterWindowSize, kTimeoutMs);
        upperTalon!!.configPulseWidthPeriod_FilterWindowSz(filterWindowSize, Constants.kTimeoutMs)
        lowerTalon!!.configPulseWidthPeriod_FilterWindowSz(filterWindowSize, Constants.kTimeoutMs)
        /**
         * Phase sensor accordingly.
         * Positive Sensor Reading should match Green (blinking) Leds on Talon
         */
        upperTalon!!.setSensorPhase(true)
        lowerTalon!!.setSensorPhase(true)

        /* Config the peak and nominal outputs */upperTalon!!.configNominalOutputForward(0.0, Constants.kTimeoutMs)
        upperTalon!!.configNominalOutputReverse(0.0, Constants.kTimeoutMs)
        upperTalon!!.configPeakOutputForward(1.0, Constants.kTimeoutMs)
        upperTalon!!.configPeakOutputReverse(-1.0, Constants.kTimeoutMs)
        lowerTalon!!.configNominalOutputForward(0.0, Constants.kTimeoutMs)
        lowerTalon!!.configNominalOutputReverse(0.0, Constants.kTimeoutMs)
        lowerTalon!!.configPeakOutputForward(1.0, Constants.kTimeoutMs)
        lowerTalon!!.configPeakOutputReverse(-1.0, Constants.kTimeoutMs)

        /* Config the Velocity closed loop gains in slot0 */upperTalon!!.config_kF(Constants.kPIDLoopIdx, Constants.kGains_Velocit.kF, Constants.kTimeoutMs)
        upperTalon!!.config_kP(Constants.kPIDLoopIdx, Constants.kGains_Velocit.kP, Constants.kTimeoutMs)
        upperTalon!!.config_kI(Constants.kPIDLoopIdx, Constants.kGains_Velocit.kI, Constants.kTimeoutMs)
        upperTalon!!.config_kD(Constants.kPIDLoopIdx, Constants.kGains_Velocit.kD, Constants.kTimeoutMs)
        lowerTalon!!.config_kF(Constants.kPIDLoopIdx, Constants.kGains_Velocit.kF, Constants.kTimeoutMs)
        lowerTalon!!.config_kP(Constants.kPIDLoopIdx, Constants.kGains_Velocit.kP, Constants.kTimeoutMs)
        lowerTalon!!.config_kI(Constants.kPIDLoopIdx, Constants.kGains_Velocit.kI, Constants.kTimeoutMs)
        lowerTalon!!.config_kD(Constants.kPIDLoopIdx, Constants.kGains_Velocit.kD, Constants.kTimeoutMs)

        tab.addIntSettingTextBox("Desired Speed", dbSpeedSetting) { dbSpeedSetting: Int -> this.dbSpeedSetting = dbSpeedSetting }

        tab.add(object : AbstractInstant3838Command("Start @ DB Speed")
                {
                    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem>
                    {
                        return ImmutableSet.of(Shooter2022Take2Subsystem)
                    }

                    override fun initializeImpl()
                    {
                        upperTalon!![ControlMode.Velocity] = dbSpeedSetting.toDouble()
                        lowerTalon!![ControlMode.Velocity] = dbSpeedSetting.toDouble()
                    }

                    @Throws(Exception::class)
                    override fun executeImpl()
                    {
                    }
                })


        runShooterAtSpeed(0.0)
    }

    internal class Gains(val kP: Double, val kI: Double, val kD: Double, val kF: Double, val kIzone: Int, val kPeakOutput: Double)
    internal object Constants
    {
        /**
         * Which PID slot to pull gains from. Starting 2018, you can choose from
         * 0,1,2 or 3. Only the first two (0,1) are visible in web-based
         * configuration.
         */
        const val kSlotIdx = 0

        /**
         * Talon SRX/ Victor SPX will supported multiple (cascaded) PID loops. For
         * now we just want the primary one.
         */
        const val kPIDLoopIdx = 0

        /**
         * Set to zero to skip waiting for confirmation, set to nonzero to wait and
         * report to DS if action fails.
         */
        const val kTimeoutMs = 30

        /**
         * PID Gains may have to be adjusted based on the responsiveness of control loop.
         * kF: 1023 represents output value to Talon at 100%, 7200 represents Velocity units at 100% output
         *
         * kP   kI   kD   kF          Iz    PeakOut  */
        //public final static Gains kGains_Velocit = new Gains( 0.25, 0.001, 20, 1023.0/7200.0,  300,  1.00);
        val kGains_Velocit = Gains(0.25, 0.0, 0.0, 1.0, 300, 1.00)
    }


}