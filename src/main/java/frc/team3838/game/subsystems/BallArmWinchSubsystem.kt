package frc.team3838.game.subsystems

import com.ctre.phoenix.motorcontrol.NeutralMode
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX
import com.google.common.collect.ImmutableSet
import frc.team3838.core.commands.AbstractPollingCommand
import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.dashboard.addBooleanBox
import frc.team3838.core.hardware.LimitSwitch
import frc.team3838.core.hardware.createWpiTalonSRX
import frc.team3838.core.logging.LaconicLogger
import frc.team3838.core.meta.API
import frc.team3838.core.subsystems.Abstract3838Subsystem
import frc.team3838.core.subsystems.I3838Subsystem
import frc.team3838.game.RobotMap


object BallArmWinchSubsystem : Abstract3838Subsystem()
{
    private var armMotor: WPI_TalonSRX? = null
    private var armUpLimitSwitch: LimitSwitch = LimitSwitch.createNoOp(true) // we default to a no-op  and init if the subsystem is enabled
    private var armDownLimitSwitch: LimitSwitch = LimitSwitch.createNoOp(true) // we default to a no-op  and init if the subsystem is enabled

    private val lowerSafetyLaconicLogger = LaconicLogger(logger, TimeDuration.ofSeconds(1))
    private val raiseSafetyLaconicLogger = LaconicLogger(logger, TimeDuration.ofSeconds(1))
    val requiredSubsystem: ImmutableSet<I3838Subsystem> by lazy { ImmutableSet.of(BallArmWinchSubsystem) }

    override fun initDefaultCommandImpl()
    {
        defaultCommand = object : AbstractPollingCommand()
        {
            override fun doAction()
            {
                // lower is a negative speed. Raise is a positive speed
                if (armIsDown() && ((armMotor?.get() ?: 0.0) < -0.01))
                {
                    val speed = armMotor?.get()
                    armMotor?.stopMotor()
                    lowerSafetyLaconicLogger.debug { "Motor monitoring command stopped motor from lowering. Speed was: $speed" }
                }
                else if (armIsUp() && ((armMotor?.get() ?: 0.0) > 0.01))
                {
                    val speed = armMotor?.get()
                    armMotor?.stopMotor()
                    raiseSafetyLaconicLogger.debug { "Motor monitoring command stopped motor from raising. Speed was: $speed" }
                }

            }

            // Because of init/construction sequence, we can NOT make this a field of the inner class and return the field
            // For now, we are returning an empty set so this is not interrupted
            override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> =  requiredSubsystem

        }
    }

    override fun initSubsystem()
    {
        armMotor = createWpiTalonSRX(RobotMap.CANs.COLLECTOR_ARM_WINCH_MOTOR, invert = true, NeutralMode.Brake)
        armUpLimitSwitch = LimitSwitch.create(RobotMap.DIOs.COLLECTOR_ARM_UP_SENSOR, invert = true)
        armDownLimitSwitch = LimitSwitch.create(RobotMap.DIOs.COLLECTOR_ARM_DOWN_SENSOR, invert = true)

        BallSystems.tab.apply{
            addBooleanBox("Is Down") { armIsDown() }
            addBooleanBox("Is Up") { armIsUp() }
            addNumber("Arm Speed") { armMotor?.get() ?: 0.0 }
        }

    }

    fun startArmMotorForLowering()
    {
        logger.debug { "startArmMotorForLowering: called. Checking limit switch" }
        if (armDownLimitSwitch.isNotActivated)
        {
            logger.debug { "startArmMotorForLowering: armDownLimitSwitch is NOT activated. Starting lower motor" }
            armMotor?.set(-0.75)
        }
        else
        {
            logger.debug { "startArmMotorForLowering: armDownLimitSwitch IS activated. Taking no action." }
        }
    }

    fun startArmMotorForRaising()
    {
        logger.debug { "startArmMotorForRaising: called. Checking limit switch" }
        if (armUpLimitSwitch.isNotActivated)
        {
            logger.debug { "startArmMotorForRaising: armUpLimitSwitch is NOT activated. Starting lift motor" }
            armMotor?.set(0.75)
        }
        else
        {
            logger.debug { "startArmMotorForRaising: armUpLimitSwitch IS activated. Taking no action." }
        }
    }

    fun stopArmMotor()
    {
        armMotor?.stopMotor()
        armMotor?.set(0.0) // stopping does not reset the speed, and we need it to be 0, so we can test for it above
        logger.debug { "stopArmMotor called." }
    }

    @Suppress("MemberVisibilityCanBePrivate")
    fun armIsDown(): Boolean = armDownLimitSwitch.isActivated
    @Suppress("MemberVisibilityCanBePrivate")
    fun armIsUp(): Boolean = armUpLimitSwitch.isActivated
    @API
    fun armIsInMiddle(): Boolean = !armIsUp() && !armIsDown()
}