package frc.team3838.game.subsystems

import com.ctre.phoenix.motorcontrol.NeutralMode
import com.google.common.collect.ImmutableSet
import edu.wpi.first.wpilibj.motorcontrol.MotorController
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab
import frc.team3838.core.commands.AbstractPollingCommand
import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.dashboard.addDoubleSettingTextBox
import frc.team3838.core.logging.LaconicLogger
import frc.team3838.core.subsystems.Abstract3838Subsystem
import frc.team3838.core.subsystems.I3838Subsystem
import frc.team3838.core.utils.constrainBetweenNegOneAndOne
import frc.team3838.core.utils.format
import frc.team3838.game.RobotMap
import mu.KotlinLogging

object MotorTestSubsystem: Abstract3838Subsystem()
{


    private val ourLogger = KotlinLogging.logger {}
    private val motorSpeedLaconicLogger = LaconicLogger(ourLogger, TimeDuration.ofSeconds(5))
    val requiredSubsystem: ImmutableSet<I3838Subsystem> by lazy { ImmutableSet.of(MotorTestSubsystem) }
    val tab: ShuffleboardTab = Shuffleboard.getTab("Motor Test")

    private var speed: Double = 0.0
    private var motor: MotorController? = null


    override fun initDefaultCommandImpl()
    {
        defaultCommand = object : AbstractPollingCommand()
        {
            override fun doAction()
            {
                motorSpeedLaconicLogger.info { "speed: set to: ${speed.format(3)} actual: ${motor?.get()?.format(3)} isInverted: ${motor?.inverted}  " }
            }

            // Because of init/construction sequence, we can NOT make this a field of the inner class and return the field
            override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = requiredSubsystem

        }
    }

    override fun initSubsystem()
    {
        val canId = RobotMap.CANs.SHOOTER_2022_BOT_MOTOR

        motor = frc.team3838.core.hardware.createWpiTalonSRX(canId, invert = false, NeutralMode.Brake)
//        motor = frc.team3838.core.hardware.createWpiVictorSPX(canId, invert = false, NeutralMode.Brake)
//        motor = frc.team3838.core.hardware.createCanSparkMax(canId, CANSparkMaxLowLevel.MotorType.kBrushless, invert = false, CANSparkMax.IdleMode.kBrake)

        tab.addDoubleSettingTextBox("Speed", 0.1) {
            speed = it.constrainBetweenNegOneAndOne()
        }
    }

    fun startMotor()
    {
        logger.info { "STARTING MOTOR with a speed of $speed" }
        motor?.set(speed)
    }
    fun stopMotor()
    {
        motor?.stopMotor()
    }
}