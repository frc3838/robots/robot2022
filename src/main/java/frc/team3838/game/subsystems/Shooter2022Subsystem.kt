package frc.team3838.game.subsystems
//
//import com.ctre.phoenix.motorcontrol.ControlMode
//import com.ctre.phoenix.motorcontrol.FeedbackDevice
//import com.ctre.phoenix.motorcontrol.NeutralMode
//import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX
//import com.google.common.collect.ImmutableSet
//import edu.wpi.first.wpilibj.shuffleboard.BuiltInWidgets
//import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard
//import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab
//import frc.team3838.core.commands.AbstractPollingCommand
//import frc.team3838.core.config.time.TimeDuration
//import frc.team3838.core.dashboard.addBooleanBox
//import frc.team3838.core.dashboard.addDoubleSettingTextBox
//import frc.team3838.core.dashboard.addIntSettingTextBox
//import frc.team3838.core.dashboard.addToggleSwitch
//import frc.team3838.core.hardware.createWpiTalonSRX
//import frc.team3838.core.logging.LaconicLeveledLogger
//import frc.team3838.core.meta.API
//import frc.team3838.core.pid.DConsumer
//import frc.team3838.core.pid.FConsumer
//import frc.team3838.core.pid.IConsumer
//import frc.team3838.core.pid.PConsumer
//import frc.team3838.core.pid.PIDTuner
//import frc.team3838.core.subsystems.Abstract3838Subsystem
//import frc.team3838.core.subsystems.I3838Subsystem
//import frc.team3838.core.utils.MathUtils
//import frc.team3838.core.utils.PIDSetting
//import frc.team3838.core.utils.format
//import frc.team3838.game.RobotMap
//import org.slf4j.event.Level
//import kotlin.math.abs
//import kotlin.math.max
//import kotlin.math.pow
//
//
//object Shooter2022Subsystem : Abstract3838Subsystem()
//{
//    @Suppress("unused")
//    private const val DEFAULT_F = 1023.0 / 7200.0
//
//    val requiredSubsystem: ImmutableSet<I3838Subsystem> by lazy { ImmutableSet.of(Shooter2022Subsystem) }
//
//    @API
//    const val tabName = "Shooter2022"
//    @JvmField
//    val tab: ShuffleboardTab = Shuffleboard.getTab(tabName)
//
//
//    private const val ENCODER_UNITS_PER_REV = 1 // For the tachometers, it's once per rev (encoders were 42 unites per rev).
//
//    /*
//     * The following factors are to convert between RPM and the sensor velocity, which is represented in "Units per 100ms".
//     * When setting speed on the MotorController in Velocity mode, the value is set in "position change / 100ms".
//     *
//     * The 600 is derived frm the fact that 1 sec is 1000ms so there are 600 "100 ms" units in a second. See the
//     * https://github.com/CrossTheRoadElec/Phoenix-Examples-Languages/tree/master/Java%20General/VelocityClosedLoop
//     * example code, specifically the "teleopPeriodic" method in the Robot class.
//     *
//     * For a very basic example of using the Tachometer (just reads the tachometer value), see
//     * https://github.com/CrossTheRoadElec/Phoenix-Examples-Languages/blob/master/Java%20General/Tachometer
//     */
//    /**
//     * Multiply the `getSelectedSensorVelocity()` value by this factor to get RPM.
//     */
//    private const val SensorVelocity_READING_TO_RPM_FACTOR = 600.0 / ENCODER_UNITS_PER_REV // 600 is the number of "100ms units" in a minute
//
//    /**
//     * Multiply the target value by this factor to get RPM.
//     */
//    private const val TARGET_RPM_TO_SensorVelocity_SETTING_FACTOR = 1.0 / SensorVelocity_READING_TO_RPM_FACTOR
//
//    private const val setRpmToVelocitySettingFactor = 2.16
//
//    // Using the encoders:
//    //    https://www.chiefdelphi.com/t/talonsrx-encoder-values-to-smart-dashboard-shuffleboard/343651/2
//    private const val SDB_SHARED_SPEED = "SHARED Speed Setting"
//
//
//    //    public static final String SDB_TOP_ACTUAL_SPEED_SETTING = "Top Actual Speed Setting";
//    //    public static final String SDB_BOT_ACTUAL_SPEED_SETTING = "Bottom Actual Speed Setting";
//    private const val SDB_TARGET_RPM = "Target Setting"
//    private const val SDB_UPPER_CALC_SPEED = "Upper Calc Setting"
//    private const val SDB_LOWER_CALC_SPEED = "Lower Calc Setting"
//    private const val SDB_UPPER_MAX_CALC_SPEED = "Upper Max Calc Setting"
//    private const val SDB_LOWER_MAX_CALC_SPEED = "Lower Max Calc Setting"
//    private const val SDB_UPPER_SPEED_VARIANCE = "Upper %"
//    private const val SDB_LOWER_SPEED_VARIANCE = "Lower %"
//    private const val SDB_UPPER_SPEED_SETTING = "Upper Speed Setting"
//    private const val SDB_LOWER_SPEED_SETTING = "Lower Speed Setting"
//    private const val SDB_UPPER_OUTPUT_PERCENTAGE = "Upper Output %"
//    private const val SDB_LOWER_OUTPUT_PERCENTAGE = "Lower Output %"
//    private const val SDB_UPPER_OUTPUT_VOLTAGE = "Upper Output Voltage"
//    private const val SDB_LOWER_OUTPUT_VOLTAGE = "Lower Output Voltage"
//    private const val SDB_UPPER_SPEED_VELOCITY = "UpperVelocity"
//    private const val SDB_LOWER_SPEED_VELOCITY = "LowerVelocity"
//
//    private const val SDB_SHOOTER_IS_RUNNING = "ShooterRunning"
//    private const val SDB_BALL_IN_SHOOTER = "Ball In"
//    private const val SDB_BREECH_HOME = "Breech Home"
//
//    private const val enablePidTuning = false
//    private var isUsePidLoop = true
//
//    /** Whether to use the PID Control for setting the motor speed (true), or use percentage setting for speed (false).   */
//    private var upperUsePidMode = true
//
//    /** Whether to use the PID Control for setting the motor speed (true), or use percentage setting for speed (false).   */
//    private var botUsePidMode = true
//    private val upperMotorPidSetting = PIDSetting("Upper", 0.25, 0.01, 0.0, /*DEFAULT_F*/ 1.0)
//    private val lowerMotorPidSetting = PIDSetting("Lower", 0.25, 0.01, 0.0, /*DEFAULT_F*/ 1.0)
//    private var upperPidTuner: PIDTuner? = null
//    private var lowerPidTuner: PIDTuner? = null
//
//
//    private var upperMotor: WPI_TalonSRX? = null
//    private var lowerMotor: WPI_TalonSRX? = null
//    private var upperVelocityReading = 0.0
//    private var lowerVelocityReading = 0.0
//
//    var isShooterRunning: Boolean = false
//        private set
//
//    /** If true, we use a velocity setting. If false we use RPMs. */
//    private const val useVelocity = false
//
//    private var sharedSpeedSetting = 0.5
//    private var targetRpms = 600
//    var upperCalcRpm = 0.0
//        private set
//    var lowerCalcRpm = 0.0
//        private set
//
//    // We want bottom spin, so we set lower motor to spin a bit faster than target RPM and the upper motor a bit slower
//    private var upperPercentVariance = 1.0 // 0.95
//    private var lowerPercentVariance = 1.0 // 1.20
//    var maxUpperCalcRpm = 0.0
//        private set
//    var maxLowerCalcRpm = 0.0
//        private set
//    @Throws(Exception::class)
//    override fun initSubsystem()
//    {
//        upperMotor = createTalonForShooter(RobotMap.CANs.SHOOTER_2022_TOP_MOTOR, upperMotorPidSetting, invert = false)
//        lowerMotor = createTalonForShooter(RobotMap.CANs.SHOOTER_2022_BOT_MOTOR, lowerMotorPidSetting, invert = false)
//        isShooterRunning = false
//
//        if (enablePidTuning)
//        {
//            upperPidTuner = PIDTuner(upperMotorPidSetting,
//                                     { value: Boolean -> upperUsePidMode = value },
//                                     PConsumer(upperMotor),
//                                     IConsumer(upperMotor),
//                                     DConsumer(upperMotor),
//                                     FConsumer(upperMotor))
//            lowerPidTuner = PIDTuner(lowerMotorPidSetting,
//                                     { value: Boolean -> botUsePidMode = value },
//                                     PConsumer(lowerMotor),
//                                     IConsumer(lowerMotor),
//                                     DConsumer(lowerMotor),
//                                     FConsumer(lowerMotor))
//            tab.addNumber("Upper Graph") { upperCalcRpm }
//                .withWidget(BuiltInWidgets.kGraph)
//                .withProperties(mapOf<String, Any>("Visible time" to 90))
//            tab.addNumber("Lower Graph") { lowerCalcRpm }
//                .withWidget(BuiltInWidgets.kGraph)
//                .withProperties(mapOf<String, Any>("Visible time" to 90))
//        }
//
//        tab.addBooleanBox("isShooting") { BreechSubsystem.isShooting() }
//        tab.addBooleanBox("Upper Xport") { BallTransporterSubsystem.upperSensorIsTriggered() }
//        tab.addBooleanBox("Lower Xport") { BallTransporterSubsystem.lowerSensorIsTriggered() }
//        tab.addBooleanBox(SDB_SHOOTER_IS_RUNNING) { isShooterRunning }
//        tab.addBooleanBox(SDB_BALL_IN_SHOOTER) { BreechSubsystem.haveBall() }
//        tab.addBooleanBox(SDB_BREECH_HOME) { BreechSubsystem.breechIsHome() }
//        tab.addNumber(SDB_UPPER_CALC_SPEED) { upperCalcRpm }
//        tab.addNumber(SDB_UPPER_MAX_CALC_SPEED) { maxUpperCalcRpm }
//        tab.addNumber(SDB_LOWER_CALC_SPEED) { lowerCalcRpm }
//        tab.addNumber(SDB_LOWER_MAX_CALC_SPEED) { maxLowerCalcRpm }
//        tab.addString(SDB_UPPER_SPEED_SETTING) { (upperMotor?.get() ?: -999.0).format(3) }
//        tab.addString(SDB_LOWER_SPEED_SETTING) { (lowerMotor?.get() ?: -999.0).format(3) }
//        tab.addString(SDB_UPPER_OUTPUT_PERCENTAGE) { (upperMotor?.motorOutputPercent ?: -999.0).format(3) }
//        tab.addString(SDB_LOWER_OUTPUT_PERCENTAGE) { (lowerMotor?.motorOutputPercent ?: -999.0).format(3) }
//        tab.addString(SDB_UPPER_OUTPUT_VOLTAGE) { (upperMotor?.motorOutputVoltage ?: -999.0).format(3) }
//        tab.addString(SDB_LOWER_OUTPUT_VOLTAGE) { (lowerMotor?.motorOutputVoltage ?: -999.0).format(3) }
//        tab.addString(SDB_UPPER_SPEED_VELOCITY) { (upperMotor?.getSelectedSensorVelocity(0) ?: -999.0).format(3) }
//        tab.addString(SDB_LOWER_SPEED_VELOCITY) { (lowerMotor?.getSelectedSensorVelocity(0) ?: -999.0).format(3) }
//
//
//        logger.debug("Adding Shooter Widgets to Shooter Tab")
//
//        @Suppress("UNUSED_VARIABLE")
//        val shardedSpeedWidget = tab.addDoubleSettingTextBox(SDB_SHARED_SPEED, sharedSpeedSetting) { value: Double ->
//            sharedSpeedSetting = MathUtils.constrainBetweenNegOneAndOne(value)
//        }
//
//        @Suppress("UNUSED_VARIABLE")
//        val targetRpmWidget = tab.addIntSettingTextBox(SDB_TARGET_RPM, targetRpms) { targetRpms: Int -> this.targetRpms = targetRpms }
//
//        @Suppress("UNUSED_VARIABLE")
//        val lowerVarianceWidget = tab.addDoubleSettingTextBox(SDB_LOWER_SPEED_VARIANCE, lowerPercentVariance
//                                                             ) { lowerPercentVariance: Double -> setLowerPercentVariance(lowerPercentVariance) }
//
//        @Suppress("UNUSED_VARIABLE")
//        val upperVarianceWidget = tab.addDoubleSettingTextBox(SDB_UPPER_SPEED_VARIANCE, upperPercentVariance
//                                                             ) { upperPercentVariance: Double -> setUpperPercentVariance(upperPercentVariance) }
//        tab.addToggleSwitch("Use PID", isUsePidLoop) { usePidLoop: Boolean -> isUsePidLoop = usePidLoop }
//    }
//
//    private fun createTalonForShooter(deviceNumber: Int, pidSetting: PIDSetting, @Suppress("SameParameterValue") invert: Boolean): WPI_TalonSRX
//    {
//        val talon = createWpiTalonSRX(deviceNumber, invert, NeutralMode.Brake)
//
//        /*
//         /* 2 Edges per cycle (WHITE-black-WHITE-black) */
//         int edgesPerCycle = 2;
//         _tachTalon.configPulseWidthPeriod_EdgesPerRot(edgesPerCycle, kTimeoutMs);
//         /* Additional filtering if need be */
//         int filterWindowSize = 1;
//         _tachTalon.configPulseWidthPeriod_FilterWindowSz(filterWindowSize, kTimeoutMs);
//         */
//
//        // Config sensor used for Primary PID [Velocity]  [From VelocityClosedLoop Example]
//        talon.configSelectedFeedbackSensor(FeedbackDevice.Tachometer,
//                                           RocConstants.PID_LOOP_INDEX,
//                                           RocConstants.TIMEOUT_MS)
//        // 1 edge per cycle white/black
//        val edgesPerCycle = 1
//        talon.configPulseWidthPeriod_EdgesPerRot(edgesPerCycle, RocConstants.TIMEOUT_MS)
//        talon.configSelectedFeedbackCoefficient(1.0, 0, RocConstants.TIMEOUT_MS)
//        // Additional filtering if needed
//        val filterWindowSize = 1
//        talon.configPulseWidthPeriod_FilterWindowSz(filterWindowSize, RocConstants.TIMEOUT_MS)
//
//        // Phase sensor accordingly. Positive Sensor Reading should match Green (blinking) LEDs on Talon  [From VelocityClosedLoop Example]
//        talon.setSensorPhase(true)
//
//        // Config the peak and nominal outputs  [From VelocityClosedLoop Example]
//        talon.configNominalOutputForward(0.0, RocConstants.TIMEOUT_MS)
//        talon.configNominalOutputReverse(0.0, RocConstants.TIMEOUT_MS)
//        talon.configPeakOutputForward(1.0, RocConstants.TIMEOUT_MS)
//        talon.configPeakOutputReverse(-1.0, RocConstants.TIMEOUT_MS)
//        talon.inverted = invert
//        configurePidValues(talon, pidSetting)
//        return talon
//    }
//
//    private fun configurePidValues(talon: WPI_TalonSRX, pidSetting: PIDSetting)
//    {
//        // Config the Velocity closed loop gains in slot0
//        talon.config_kF(RocConstants.PID_LOOP_INDEX, pidSetting.f, RocConstants.TIMEOUT_MS)
//        talon.config_kP(RocConstants.PID_LOOP_INDEX, pidSetting.p, RocConstants.TIMEOUT_MS)
//        talon.config_kI(RocConstants.PID_LOOP_INDEX, pidSetting.i, RocConstants.TIMEOUT_MS)
//        talon.config_kD(RocConstants.PID_LOOP_INDEX, pidSetting.d, RocConstants.TIMEOUT_MS)
//    }
//
//    private val speedLaconicLogger = LaconicLeveledLogger(logger, TimeDuration.ofSeconds(2), Level.INFO)
//
//    override fun initDefaultCommandImpl()
//    {
//        defaultCommand = object : AbstractPollingCommand()
//        {
//            override fun doAction()
//            {
//
//                updateDashBoardAndLogging()
//
//            }
//
//            private fun updateDashBoardAndLogging()
//            {
//                upperVelocityReading = upperMotor?.getSelectedSensorVelocity(0) ?: 0.0
//                // upperCalcRpm =  upperVelocityReading * SensorVelocity_READING_TO_RPM_FACTOR
//                // targetVelocityInUnitsPer100ms = targetRpms / setRpmToVelocitySettingFactor
//                // v = r / F
//                // v * F = r
//                upperCalcRpm =  upperVelocityReading * setRpmToVelocitySettingFactor
//                maxUpperCalcRpm = max(upperCalcRpm, maxUpperCalcRpm)
//                lowerVelocityReading = lowerMotor?.getSelectedSensorVelocity(0) ?: 0.0
//                // lowerVelocityReading * SensorVelocity_READING_TO_RPM_FACTOR
//                lowerCalcRpm = lowerVelocityReading * setRpmToVelocitySettingFactor
//                maxLowerCalcRpm = max(lowerCalcRpm, maxLowerCalcRpm)
//
//                speedLaconicLogger.debug { "upperVel = $upperVelocityReading  lowerVel = $lowerVelocityReading  upper RPM: $upperCalcRpm   RPM: $lowerCalcRpm  upper.get: ${upperMotor?.get()?.format(2)}  lower.get: ${lowerMotor?.get()?.format(2)}" }
//            }
//
//            // Because of init/construction sequence, we can NOT make this a field of the inner class and return the field
//            override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = requiredSubsystem
//
//        }
//    }
//
//
//
//    /** Starts the shooter to run at the specified RPMs. */
//    fun startShooter(targetSetting: Int)
//    {
//        if (upperMotor == null || lowerMotor == null) return
//
//        val targetVelocityInUnitsPer100ms = if (useVelocity) targetSetting.toDouble() else (targetSetting.toDouble() * TARGET_RPM_TO_SensorVelocity_SETTING_FACTOR)
//        val upperTargetVelocityInUnitsPer100ms = targetVelocityInUnitsPer100ms * upperPercentVariance
//        val lowerTargetVelocityInUnitsPer100ms = targetVelocityInUnitsPer100ms * lowerPercentVariance
//        isShooterRunning = true
//        upperMotor!![ControlMode.Velocity] = upperTargetVelocityInUnitsPer100ms
//        lowerMotor!![ControlMode.Velocity] = lowerTargetVelocityInUnitsPer100ms
//    }
//
//    /** Used for tuning. It reads the values from the dashboard widgets, makes config changes, and then sets the motor's target velocity.  */
//    fun setMotorSpeed()
//    {
//        if (upperMotor == null || lowerMotor == null) return
//
//        if (isUsePidLoop)
//        {
//            logger.debug("setMotorSpeed Running via closed loop using Velocity (i.e. PID Controlled). targetSetting:  $targetRpms")
//            if (enablePidTuning)
//            {
//                configurePidValues(upperMotor!!, upperMotorPidSetting)
//                configurePidValues(lowerMotor!!, lowerMotorPidSetting)
//            }
//            val targetVelocityInUnitsPer100ms = targetRpms // / setRpmToVelocitySettingFactor / 6 // 6 is the magic mitch factor  // targetRpms * TARGET_RPM_TO_SensorVelocity_SETTING_FACTOR
//            val upperTargetVelocityInUnitsPer100ms = targetVelocityInUnitsPer100ms * upperPercentVariance
//            val lowerTargetVelocityInUnitsPer100ms = targetVelocityInUnitsPer100ms * lowerPercentVariance
//            logger.debug("setMotorSpeed setting upper to ${upperTargetVelocityInUnitsPer100ms.format()} setting lower to: ${lowerTargetVelocityInUnitsPer100ms.format()} for targetSetting:  $targetRpms")
//            isShooterRunning = true
////            upperMotor!![ControlMode.Velocity] = upperTargetVelocityInUnitsPer100ms
////            lowerMotor!![ControlMode.Velocity] = lowerTargetVelocityInUnitsPer100ms
//            upperMotor?.set(ControlMode.Velocity, 775.0/*upperTargetVelocityInUnitsPer100ms*/)
//            lowerMotor?.set(ControlMode.Velocity, 775.0/*lowerTargetVelocityInUnitsPer100ms*/)
//        }
//        else
//        {
//            logger.debug("setMotorSpeed running using PercentOutput")
////            val upperSpeedSetting = sharedSpeedSetting * upperPercentVariance
////            val lowerSpeedSetting = sharedSpeedSetting * lowerPercentVariance
////            logger.debug("setMotorSpeed setting upper to ${upperSpeedSetting.format()} setting lower to: ${lowerSpeedSetting.format()} for targetSetting:  $targetRpms")
////            isShooterRunning = true
//////            upperMotor!![ControlMode.PercentOutput] = upperSpeedSetting
//////            lowerMotor!![ControlMode.PercentOutput] = lowerSpeedSetting
////            upperMotor?.set(ControlMode.PercentOutput, upperSpeedSetting)
////            lowerMotor?.set(ControlMode.PercentOutput, lowerSpeedSetting)
//        }
//    }
//
//    @API
//    fun calcNeededRpmForDistance(distancesToGoal: Double): Double = (0.0264 * distancesToGoal.pow(2)) - (0.1691 * distancesToGoal) + 1588.7
//
////    fun startTopMotor()
////    {
////        isShooterRunning = true
////        upperMotor?.set(1.0)
////    }
////
////    fun startBotMotor()
////    {
////        isShooterRunning = true
////        lowerMotor?.set(1.0)
////    }
//
////    fun stopMotors()
////    {
////        isShooterRunning = false
////        upperMotor?.set(0.0)
////        lowerMotor?.set(0.0)
////    }
//
//
//    @API
//    fun getUpperVelocityReading(): Double
//    {
//        return upperVelocityReading
//    }
//
//    @API
//    fun getLowerVelocityReading(): Double
//    {
//        return lowerVelocityReading
//    }
//
//    @API
//    fun setUpperPercentVariance(upperPercentVariance: Double)
//    {
//        this.upperPercentVariance = abs(upperPercentVariance)
//    }
//
//    @Suppress("unused")
//    @API
//    fun getUpperPercentVariance(): Double
//    {
//        return upperPercentVariance
//    }
//
//    @API
//    fun setLowerPercentVariance(lowerPercentVariance: Double)
//    {
//        this.lowerPercentVariance = abs(lowerPercentVariance)
//    }
//
//    @Suppress("unused")
//    @API
//    fun getLowerPercentVariance(): Double
//    {
//        return lowerPercentVariance
//    }
//
//    @Suppress("unused")
//    @API
//    val isLowerMotorInverted: Boolean
//        get() = lowerMotor != null && lowerMotor!!.inverted
//
//    @Suppress("unused")
//    @API
//    val isUpperMotorInverted: Boolean
//        get() = upperMotor != null && upperMotor!!.inverted
//
//}
//
//internal object RocConstants
//{
//    // Based on https://github.com/CrossTheRoadElec/Phoenix-Examples-Languages/blob/master/Java%20General/VelocityClosedLoop/src/main/java/frc/robot/Constants.java
//    /**
//     * Which PID slot to pull gains from. Starting 2018, you can choose from
//     * 0,1,2 or 3. Only the first two (0,1) are visible in web-based configuration.
//     */
//    @Suppress("unused")
//    const val SLOT_INDEX = 0
//
//    /**
//     * Talon SRX/ Victor SPX will support multiple (cascaded) PID loops. For
//     * now, we just want the primary one.
//     */
//    const val PID_LOOP_INDEX = 0
//
//    /**
//     * Set to zero to skip waiting for confirmation.
//     * Set to nonzero to wait and report to DS if action fails.
//     */
//    const val TIMEOUT_MS = 30
//
//    /**
//     * PID Gains may have to be adjusted based on the responsiveness of control loop.
//     * kF: 1023 represents output value to Talon at 100%, 7200 represents Velocity units at 100% output
//     *
//     * kP   kI   kD   kF          Iz    PeakOut
//     */
//    @Suppress("unused")
//    val GAINS_VELOCITY = Gains("",0.25, 0.001, 20.0, 1023.0 / 7200.0, 300, 1.00)
//}
//
//@Suppress("unused")
//class Gains(
//    name: String,
//    p: Double = 0.0,
//    i: Double = 0.0,
//    d: Double = 0.0,
//    f: Double = 0.0,
//    val iZone: Int = 300,
//    val peakOutput: Double = 1.00,
//    enableTuning: Boolean = false) : PIDSetting(name, p, i, d, f, enableTuning)