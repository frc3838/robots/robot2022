package frc.team3838.game.subsystems

import com.ctre.phoenix.motorcontrol.NeutralMode
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX
import com.google.common.collect.ImmutableSet
import frc.team3838.core.commands.AbstractPollingCommand
import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.dashboard.addBooleanBox
import frc.team3838.core.hardware.LimitSwitch
import frc.team3838.core.hardware.createWpiTalonSRX
import frc.team3838.core.logging.LaconicLogger
import frc.team3838.core.subsystems.Abstract3838Subsystem
import frc.team3838.core.subsystems.I3838Subsystem
import frc.team3838.game.RobotMap


object BallTransporterSubsystem: Abstract3838Subsystem()
{
    // MITCH: Set transport speed here
    private const val TRANSPORT_MOTOR_SPEED: Double = 0.40

    val requiredSubsystem: ImmutableSet<I3838Subsystem> by lazy { ImmutableSet.of(BallTransporterSubsystem) }

    private var transportMotor: WPI_TalonSRX? = null

    /** 'TI Transporter Ball In' Sensor that indicates the ball has been transferred from the ball intake to the ball transporter. */
    private var transportLowerSensor: LimitSwitch? = null

    /** 'TU Transporter Ball Up' Sensor that indicates the ball is at the top of the ball transporter, before falling into the shooter. */
    private var transportUpperSensor: LimitSwitch? = null

    /** Indicates fot the transport should be active - i.e. it is desired to be on; the operator has turned it on.
     *  However, the motor may not be running if a ball is att he top of the transport and there is a ball in the breech. */
    private var transportActive = false

    override fun initSubsystem()
    {
        transportMotor = createWpiTalonSRX(RobotMap.CANs.TRANSPORT_MOTOR, invert = false, NeutralMode.Brake)
        transportLowerSensor = LimitSwitch.create(RobotMap.DIOs.BALL_IN_TRANSPORT_IR_SENSOR, invert = true)
        transportUpperSensor = LimitSwitch.create(RobotMap.DIOs.BALL_TOP_TRANSPORT_IR_SENSOR, invert = true)

        BallSystems.tab.apply {
            addBooleanBox("Lower Xport") { upperSensorIsTriggered() }
            addBooleanBox("Upper Xport") { lowerSensorIsTriggered() }
        }
    }

    override fun initDefaultCommandImpl()
    {
        val laconicLogger = LaconicLogger(logger, TimeDuration.ofSeconds(2))
        defaultCommand = object : AbstractPollingCommand()
        {
            override fun doAction()
            {
                if (BreechSubsystem.haveBall() && upperSensorIsTriggered())
                {
                    stopTransportMotorInternal()
                    laconicLogger.debug("Stopped Transport: haveBall: ${BreechSubsystem.haveBall()}  upperXport: ${upperSensorIsTriggered()}")
                }
                else
                {
                    if (transportActive && BreechSubsystem.completedLastCycle())
                    {
                        startTransportMotorInternal()
                        laconicLogger.trace("Starting Transport: haveBall: ${BreechSubsystem.haveBall()}  upperXport: ${upperSensorIsTriggered()} transportActive: $transportActive completedLastCycle ${BreechSubsystem.completedLastCycle()}")
                    }
                    else
                    {
                        laconicLogger.trace("NOT Starting Transport: haveBall: ${BreechSubsystem.haveBall()}  upperXport: ${upperSensorIsTriggered()} transportActive: $transportActive completedLastCycle ${BreechSubsystem.completedLastCycle()}")
                    }
                }
            }

            // Because of init/construction sequence, we can NOT make this a field of the inner class and return the field
            override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = requiredSubsystem

        }
    }

    fun startTransportMotor()
    {
        transportActive = true
        transportMotor?.set(TRANSPORT_MOTOR_SPEED)
    }

    private fun startTransportMotorInternal()
    {
        transportMotor?.set(TRANSPORT_MOTOR_SPEED)
    }

    fun reverseTransportMotor()
    {
        if (transportLowerSensor?.isNotActivated == true)
        {
            transportActive = false
            transportMotor?.set(-TRANSPORT_MOTOR_SPEED)
        }
    }
    fun stopTransportMotor()
    {
        transportMotor?.stopMotor()
        transportActive = false
    }

    private fun stopTransportMotorInternal()
    {
        transportMotor?.stopMotor()
    }

    fun upperSensorIsTriggered(): Boolean = transportUpperSensor?.isActivated == true
    fun lowerSensorIsTriggered(): Boolean = transportLowerSensor?.isActivated == true
}