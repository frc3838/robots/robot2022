package frc.team3838.game.commands

import com.google.common.collect.ImmutableSet
import frc.team3838.core.commands.Abstract3838Command
import frc.team3838.core.commands.AbstractInstant3838Command
import frc.team3838.core.meta.API
import frc.team3838.core.subsystems.I3838Subsystem
import frc.team3838.core.utils.NO_OP
import frc.team3838.game.subsystems.BreechSubsystem
import frc.team3838.game.subsystems.Shooter2022Take2Subsystem


fun addAllShooterCommands()
{
//    Shooter2022Subsystem.tab.apply {
//        add(Shooter2022CallSetMotorSpeedCommand)
//        add(StopShooter2022MotorsCommand)
//        add(CycleBreechCommand)
//        add(StopBreechCommand)
//    }

    Shooter2022Take2Subsystem.tab.apply {
        add(StopShooterCommand())
        add(StartShooterAtSpeed(775))
        add(StartShooterAtSpeed(975))
        add(StartShooterAtSpeed(1175))
        add(StartShooterAtSpeed(1375))
        add(StartShooterAtSpeed(1575))
        add(StartShooterAtSpeed(1775))
    }

    BreechSubsystem.tab.apply {
        add(CycleBreechCommand())
        add(StopBreechCommand())
        add(ShootForceNewPositionCommand())
    }
}

// region Abstract Classes Region
abstract class AbstractShooter2022TakeBInstantCommand(name: String? = null) : AbstractInstant3838Command(name)
{
    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of(Shooter2022Take2Subsystem)
}

abstract class AbstractBreechInstantCommand(name: String? = null) : AbstractInstant3838Command(name)
{
    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of(BreechSubsystem)
}

@API
abstract class AbstractFullShootInstantCommand(name: String? = null): AbstractInstant3838Command(name)
{
    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of(Shooter2022Take2Subsystem, BreechSubsystem)
}

@API
abstract class AbstractFullShootCommand(name: String? = null): Abstract3838Command(name)
{
    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of(Shooter2022Take2Subsystem, BreechSubsystem)
}

// endregion Abstract Classes Region


class CycleBreechCommand: AbstractBreechInstantCommand("Cycle Breech")
{
    override fun executeImpl() = BreechSubsystem.cycleBreechToShoot()
}

class StopBreechCommand: AbstractBreechInstantCommand("Stop Breech")
{
    override fun executeImpl() = BreechSubsystem.stopMotor()
}

class ShootForceNewPositionCommand: AbstractBreechInstantCommand("Cycle Breech - ForceNewPosition")
{
    override fun executeImpl() = BreechSubsystem.cycleBreechToShoot(forceNewTargetPosition = true)
}

class StopShooterCommand: AbstractShooter2022TakeBInstantCommand("Stop Shooter B")
{
    override fun executeImpl() = Shooter2022Take2Subsystem.stopShooter()
}

class StartShooterAtSpeed(private val targetSpeed: Int): AbstractShooter2022TakeBInstantCommand("Start@$targetSpeed")
{
    override fun initializeImpl() = Shooter2022Take2Subsystem.runShooterAtSpeed(targetSpeed.toDouble())

    override fun executeImpl() = NO_OP()
}


class ShootSequenceCommand: AbstractBreechInstantCommand("Shoot")//  AbstractFullShootInstantCommand("Shoot") <== For now, we don't require ShooterSubsystem since we only check a boolean
{
    override fun executeImpl() = NO_OP()

    override fun initializeImpl()
    {
        if (BreechSubsystem.haveBall() && Shooter2022Take2Subsystem.isShooterRunning)
        {
            logger.debug { "Shooting" }
            BreechSubsystem.cycleBreechToShoot()
        }
        else
        {
            logger.info { "NOT Shooting. haveBall: ${BreechSubsystem.haveBall()}  shooterIsRunning: ${Shooter2022Take2Subsystem.isShooterRunning}" }
        }
    }

}

//class StartShooter2022TopMotorCommand: AbstractShooter2022InstantCommand()
//{
//    override fun executeImpl() = NO_OP() // Shooter2022Subsystem.startTopMotor ()
//}
//
//class StartShooter2022BotMotorCommand: AbstractShooter2022InstantCommand()
//{
//    override fun executeImpl() = NO_OP() //Shooter2022Subsystem.startBotMotor()
//}
//
//class StopShooter2022MotorsCommand: AbstractShooter2022InstantCommand("STOP Shooter")
//{
//    override fun executeImpl() = NO_OP() //Shooter2022Subsystem.stopMotors()
//}
//
//class StartShooter2022MotorsCommand(private val targetValue: Double = 775.0): AbstractShooter2022InstantCommand()
//{
//    override fun executeImpl() = Shooter2022Take2Subsystem.runShooterAtSpeed(targetValue)
//}

///** Used to set the motor speed of the shooter. It is designed to be called from the dashboard after modifying the target RPMs.  */
//class Shooter2022CallSetMotorSpeedCommand : AbstractShooter2022InstantCommand("Set Motor Speed")
//{
//    override fun executeImpl() = Shooter2022Subsystem.setMotorSpeed()
//}
