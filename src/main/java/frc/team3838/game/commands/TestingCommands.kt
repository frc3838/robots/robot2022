package frc.team3838.game.commands

import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard
import frc.team3838.core.commands.drive.SimpleDriveFromDashboardCommand
import frc.team3838.core.dashboard.addDoubleSettingTextBox
import java.util.function.DoubleConsumer


private var seconds: Double = 0.25
private var speed: Double = 0.20

fun addAllTestingCommands()
{
    Shuffleboard.getTab("Testing").apply {
        addDoubleSettingTextBox("seconds (ex: 1.5)", 0.25) { DoubleConsumer { secondsSetting: Double -> seconds = secondsSetting} }
        addDoubleSettingTextBox("Speed (-0.5 to 0.5)", 0.2) { DoubleConsumer { speedSetting: Double -> speed = speedSetting } }
        add(SimpleDriveFromDashboardCommand({ speed }, { seconds }))
    }
}