//@file:Suppress("DEPRECATION")
//
package frc.team3838.game.commands
//
//import com.google.common.collect.ImmutableSet
//import frc.team3838.core.commands.AbstractInstant3838Command
//import frc.team3838.core.subsystems.I3838Subsystem
//import frc.team3838.core.utils.NO_OP
//import frc.team3838.game.subsystems.ShooterDEPRECATEDSubsystem
//
///*
// _____                                _           _
//|  __ \                              | |         | |
//| |  | | ___ _ __  _ __ ___  ___ __ _| |_ ___  __| |
//| |  | |/ _ \ '_ \| '__/ _ \/ __/ _` | __/ _ \/ _` |
//| |__| |  __/ |_) | | |  __/ (_| (_| | ||  __/ (_| |
//|_____/ \___| .__/|_|  \___|\___\__,_|\__\___|\__,_|
//            | |
//            |_|
//
// */
//@Deprecated("Last year's robot")
//abstract class AbstractShooterInstantCommand : AbstractInstant3838Command()
//{
//    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of(ShooterDEPRECATEDSubsystem)
//    override fun initializeImpl() = NO_OP()
//}
//
//@Deprecated("Last year's robot")
//abstract class AbstractBallInstantCommand : AbstractInstant3838Command()
//{
//    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of()
//    override fun initializeImpl() = NO_OP()
//}
//
//@Deprecated("Last year's robot")
//abstract class AbstractAcquisitionArmInstantCommand : AbstractInstant3838Command()
//{
//    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of()
//    override fun initializeImpl() = NO_OP()
//}
//
//
//@Deprecated("Last year's robot")
//class StopShooterMotorsCommand: AbstractShooterInstantCommand()
//{
//    override fun executeImpl() = ShooterDEPRECATEDSubsystem.stopMotors()
//}
//
//@Deprecated("Last year's robot")
//class ShooterCallSetMotorSpeedCommand: AbstractShooterInstantCommand()
//{
//    override fun executeImpl() = ShooterDEPRECATEDSubsystem.setMotorSpeed()
//}
//
//@Deprecated("Last year's robot")
//class ShooterStartHighSpeed(): AbstractShooterInstantCommand()
//{
//    override fun executeImpl() = ShooterDEPRECATEDSubsystem.startShooter(6000)
//}
//
//@Deprecated("Last year's robot")
//class ShooterStartMediumSpeed(): AbstractShooterInstantCommand()
//{
//    override fun executeImpl() = ShooterDEPRECATEDSubsystem.startShooter(4000)
//}
//
//@Deprecated("Last year's robot")
//class ShooterStartLowSpeed(): AbstractShooterInstantCommand()
//{
//    override fun executeImpl() = ShooterDEPRECATEDSubsystem.startShooter(2000)
//}
//
//@Deprecated("Last year's robot")
//class RingLightToggleCommand(): AbstractShooterInstantCommand()
//{
//    override fun executeImpl() = ShooterDEPRECATEDSubsystem.toggleRingLight()
//}
//
//
//
