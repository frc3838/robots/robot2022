package frc.team3838.game.commands;

import javax.annotation.Nonnull;
import java.util.Map;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInLayouts;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardLayout;
import frc.team3838.core.dashboard.DashboardManager;
import frc.team3838.game.subsystems.MotorTestCommandsKt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class CommandDashboardButtons
{
    private static final Logger logger = LoggerFactory.getLogger(CommandDashboardButtons.class);

    /*
        EXAMPLE FROM https://wpilib.screenstepslive.com/s/currentCS/m/shuffleboard/l/1021980-organizing-widgets
        ShuffleboardLayout elevatorCommands = Shuffleboard.getTab("Commands")
          .getLayout("Elevator", BuiltInLayouts.kList)
          .withSize(2, 2)
          .withProperties(Map.of("Label position", "HIDDEN")); // hide labels for commands
        elevatorCommands.add(new ElevatorDownCommand());
        elevatorCommands.add(new ElevatorUpCommand());

     */


    public static void addAllCommands()
    {
        BallCommandsKt.addAllBallCommands();
        MotorTestCommandsKt.addAllMotorTestCommands();
        Shooter2022CommandsKt.addAllShooterCommands();
        TestingCommandsKt.addAllTestingCommands();
    }
    
    


    @Nonnull
    private static ShuffleboardLayout createLayout(String title, int width, int height)
    {
        return DashboardManager.getCommandsTab().getLayout(title, BuiltInLayouts.kList)
                               .withSize(width, height)
                               .withProperties(Map.of("Label position", "HIDDEN"));  // hide labels for commands
    }
}
