package frc.team3838.game.commands

import com.google.common.collect.ImmutableSet
import frc.team3838.core.commands.Abstract3838Command
import frc.team3838.core.commands.Abstract3838CommandGroup
import frc.team3838.core.commands.AbstractInstant3838Command
import frc.team3838.core.commands.common.SleepCommand
import frc.team3838.core.config.time.PartialSeconds
import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.subsystems.I3838Subsystem
import frc.team3838.core.utils.NO_OP
import frc.team3838.game.subsystems.BallArmWinchSubsystem
import frc.team3838.game.subsystems.BallCollectorSubsystem
import frc.team3838.game.subsystems.BallSystems
import frc.team3838.game.subsystems.BallTransporterSubsystem
import frc.team3838.game.subsystems.Shooter2022Take2Subsystem


fun addAllBallCommands()
{
    BallSystems.tab.apply {
        add(LowerCollectorArm())
        add(RaiseCollectorArm())
        add(StopCollectorArm())
        add(StartIntakeMotorCommand())
        add(StopIntakeMotorCommand())
        add(ReverseIntakeMotorCommand())
        add(StartIntakeAndTransportMotorsCommand())
        add(StopIntakeAndTransportMotorsCommand())
        add(StartTransportMotorCommand())
        add(StopTransportMotorCommand())
        add(ReverseTransportMotorCommand())
    }
}

// region Abstract Classes Region
abstract class AbstractCollectorCommand(name: String? = null) : Abstract3838Command(name)
{
    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of(BallCollectorSubsystem)
}

abstract class AbstractCollectorInstantCommand(name: String? = null) : AbstractInstant3838Command(name)
{
    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of(BallCollectorSubsystem)
}

abstract class AbstractArmWinchCommand(name: String? = null) : Abstract3838Command(name)
{
    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of(BallArmWinchSubsystem)
}

abstract class AbstractArmWinchInstantCommand(name: String? = null) : AbstractInstant3838Command(name)
{
    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of(BallArmWinchSubsystem)
}

abstract class AbstractTransporterCommand(name: String? = null) : Abstract3838Command(name)
{
    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of(BallTransporterSubsystem)
}

abstract class AbstractTransporterInstantCommand(name: String? = null) : AbstractInstant3838Command(name)
{
    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of(BallTransporterSubsystem)
}

abstract class AbstractBallSystemCommand(name: String? = null) : Abstract3838Command(name)
{
    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of(BallCollectorSubsystem, BallTransporterSubsystem)
}

abstract class AbstractBallSystemInstantCommand(name: String? = null) : AbstractInstant3838Command(name)
{
    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of(BallCollectorSubsystem, BallTransporterSubsystem)
}

// endregion Abstract classes

class LowerCollectorArmOLD: AbstractArmWinchCommand("Lower")
{
    override fun initializeImpl() = BallArmWinchSubsystem.startArmMotorForLowering()

    override fun executeImpl() = NO_OP()

    override fun isFinishedImpl(): Boolean = BallArmWinchSubsystem.armIsDown()

    override fun endImpl() = BallArmWinchSubsystem.stopArmMotor()

    override fun interruptedImpl()
    {
        super.interruptedImpl()
        logger.info { "interrupted called for LowerCollectorArm" }
    }
}
class LowerCollectorArm(delay:TimeDuration = TimeDuration.ofSeconds(0.125)): AbstractArmWinchCommand("Lower w delay")
{
    private var delayStart = 0L
    private val delayTimeMs = delay.toMillis()
    override fun initializeImpl() = BallArmWinchSubsystem.startArmMotorForLowering()

    override fun executeImpl() = NO_OP()

    override fun isFinishedImpl(): Boolean
    {
        if (BallArmWinchSubsystem.armIsDown())
        {
            if(delayStart == 0L)
            {
                delayStart = System.currentTimeMillis()
                return false
            }
            else
            {
                return (System.currentTimeMillis() - delayStart) >= delayTimeMs
            }
        }
        else
        {
            return false
        }
    }

    override fun endImpl() = BallArmWinchSubsystem.stopArmMotor()

    override fun interruptedImpl()
    {
        super.interruptedImpl()
        logger.info { "interrupted called for LowerCollectorArm" }
    }
}

class RaiseCollectorArm: AbstractArmWinchCommand("Raise")
{
    override fun initializeImpl() = BallArmWinchSubsystem.startArmMotorForRaising()

    override fun executeImpl() = NO_OP()

    override fun isFinishedImpl(): Boolean = BallArmWinchSubsystem.armIsUp()

    override fun endImpl() = BallArmWinchSubsystem.stopArmMotor()

    override fun interruptedImpl()
    {
        super.interruptedImpl()
        logger.info { "interrupted called for RaiseCollectorArm" }
    }
}

class StopCollectorArm : AbstractArmWinchInstantCommand("Stop Arm")
{
    override fun executeImpl() = BallArmWinchSubsystem.stopArmMotor()
}

class StartIntakeMotorCommand(private val speedToUse: Double = BallCollectorSubsystem.TELEOP_DEFAULT_INTAKE_MOTOR_SPEED): AbstractCollectorInstantCommand("Start Intake")
{
    override fun executeImpl() = BallCollectorSubsystem.startIntakeMotor(speedToUse)
}

class ReverseIntakeMotorCommand: AbstractCollectorInstantCommand("Reverse Intake")
{
    override fun executeImpl() = BallCollectorSubsystem.reverseIntakeMotor()
}
class ReverseIntakeMotorWithDelayCommand: Abstract3838CommandGroup()
{
    init
    {
        addSequential(SleepCommand(TimeDuration.ofSeconds(0, PartialSeconds.HalfSecond)))
        addSequential(ReverseIntakeMotorCommand())
    }
}

class StopIntakeMotorCommand: AbstractCollectorInstantCommand("Stop Intake")
{
    override fun executeImpl() = BallCollectorSubsystem.stopIntakeMotor()
}


class StartTransportMotorCommand: AbstractTransporterInstantCommand("Start Transport")
{
    override fun executeImpl() = BallTransporterSubsystem.startTransportMotor()
}

class ReverseTransportMotorCommand: AbstractTransporterInstantCommand("Reverse Transport")
{
    override fun executeImpl() = BallTransporterSubsystem.reverseTransportMotor()
}

class StopTransportMotorCommand: AbstractTransporterInstantCommand("Stop Transport")
{
    override fun executeImpl() = BallTransporterSubsystem.stopTransportMotor()
}


class StartIntakeAndTransportMotorsCommand(private val intakeSpeed: Double = BallCollectorSubsystem.TELEOP_DEFAULT_INTAKE_MOTOR_SPEED) : AbstractBallSystemInstantCommand("Start Intake & Transport")
{
    override fun executeImpl()
    {
        BallTransporterSubsystem.startTransportMotor()
        BallCollectorSubsystem.startIntakeMotor(intakeSpeed)
    }
}

class StopIntakeAndTransportMotorsCommand : AbstractBallSystemInstantCommand("Stop Intake & Transport")
{
    override fun executeImpl()
    {
        BallTransporterSubsystem.stopTransportMotor()
        BallCollectorSubsystem.stopIntakeMotor()
    }
}


class StopAllMotorsCommand : AbstractInstant3838Command("Stop All Motors")
{
    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> =
        ImmutableSet.of(Shooter2022Take2Subsystem, BallCollectorSubsystem, BallTransporterSubsystem)

    override fun executeImpl()
    {
        BallCollectorSubsystem.stopIntakeMotor()
        BallTransporterSubsystem.stopTransportMotor()
        Shooter2022Take2Subsystem.stopShooter()
    }
}


