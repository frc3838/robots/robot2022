package frc.team3838.game;

import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.google.common.collect.ImmutableMap;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.XboxController.Axis;
import edu.wpi.first.wpilibj.buttons.Button;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.core.config.AbstractIOAssignments;
import frc.team3838.core.controls.AxisAsButton;
import frc.team3838.core.controls.AxisPair;
import frc.team3838.core.controls.ButtonAction;
import frc.team3838.core.controls.HidBasedAxisPair;
import frc.team3838.core.controls.LimitingAxisPair;
import frc.team3838.core.controls.POVDirection;
import frc.team3838.core.dashboard.SortedSendableChooser;
import frc.team3838.core.dashboard.SortedSendableChooser.Order;
import frc.team3838.core.meta.API;
import frc.team3838.core.subsystems.I3838Subsystem;
import frc.team3838.core.subsystems.NavxSubsystem;
import frc.team3838.core.subsystems.Subsystems;
import frc.team3838.core.subsystems.drive.DriveControlMode;
import frc.team3838.game.commands.LowerCollectorArm;
import frc.team3838.game.commands.RaiseCollectorArm;
import frc.team3838.game.commands.ReverseIntakeMotorWithDelayCommand;
import frc.team3838.game.commands.ShootSequenceCommand;
import frc.team3838.game.commands.StartIntakeAndTransportMotorsCommand;
import frc.team3838.game.commands.StartShooterAtSpeed;
import frc.team3838.game.commands.StopAllMotorsCommand;
import frc.team3838.game.commands.StopCollectorArm;
import frc.team3838.game.commands.StopIntakeAndTransportMotorsCommand;
import frc.team3838.game.commands.StopIntakeMotorCommand;
import frc.team3838.game.commands.StopShooterCommand;
import frc.team3838.game.subsystems.BallArmWinchSubsystem;
import frc.team3838.game.subsystems.BallCollectorSubsystem;
import frc.team3838.game.subsystems.BallTransporterSubsystem;
import frc.team3838.game.subsystems.BreechSubsystem;
import frc.team3838.game.subsystems.DriveTrainSubsystem;
import frc.team3838.game.subsystems.DriverControlConfig2022;
import frc.team3838.game.subsystems.MotorTestSubsystem;
import frc.team3838.game.subsystems.Shooter2022Take2Subsystem;
import frc.team3838.game.subsystems.UsbDualCamera2022Subsystem;
import frc.team3838.game.subsystems.UsbSingleCamera2022CameraOneSubsystem;
import frc.team3838.game.subsystems.UsbSingleCamera2022CameraZeroSubsystem;

            

/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap
{
    /**
     * Factor added to AIO & DIO port and PWM Channel numbers on the MXP (myRIO Expansion Port) 'More Board'.
     * Thus, DIO port 2 on the MXP More board is DIO 12 (2 + ADD_FACTOR) to the roboRIO.
     * And  AIO port 5 on the MXP More board is DIO 15 (5 + ADD_FACTOR) to the roboRIO.
     * <br/>
     * See <a href='www.revrobotics.com/product/more-board/'>www.revrobotics.com/product/more-board/</a> <br/>
     * See <a href='http://wpilib.screenstepslive.com/s/4485/m/13809/l/145309-java-conventions-for-objects-methods-and-variables#MXPIONumbering'>http://wpilib.screenstepslive.com/s/4485/m/13809/l/145309-java-conventions-for-objects-methods-and-variables#MXPIONumbering</a><br/>
     */
    @SuppressWarnings("unused")
    public static final int MXP_ADD_FACTOR = 10;

    @SuppressWarnings("WeakerAccess")
    @Nonnull
    public static final ImmutableMap<Class<? extends I3838Subsystem>, Boolean> enabledSubsystemsMap = initSubsystemsMap();

    public static final boolean isDriveTrainEnabled = true;
    private static final boolean isShooterEnabled = true;
    private static final boolean isBallCollectionEnabled = true;

    private static ImmutableMap<Class<? extends I3838Subsystem>, Boolean> initSubsystemsMap()
    {
        final ImmutableMap.Builder<Class<? extends I3838Subsystem>, Boolean> map = ImmutableMap.builder();
        
        map.put(DriveTrainSubsystem.class, isDriveTrainEnabled);
        map.put(NavxSubsystem.class, true);
        map.put(BallArmWinchSubsystem.class, isBallCollectionEnabled);
        map.put(BallCollectorSubsystem.class, isBallCollectionEnabled);
        map.put(BallTransporterSubsystem.class, isBallCollectionEnabled);
        map.put(Shooter2022Take2Subsystem.class, isShooterEnabled);
        map.put(BreechSubsystem.class, isShooterEnabled);
        
        // MITCH: Enable/Disable USB Cameras here false means disabled, true means enabled
        //        If the subsystem has errors, it may cause the robot to not boot up properly
        //        So if the usb camera are not working, you may need to disable and go without
        //        DO NOT enable the dual camera subsystem and the two individual ones at the same time
        map.put(UsbDualCamera2022Subsystem.class, false);
        map.put(UsbSingleCamera2022CameraZeroSubsystem.class, true);
        map.put(UsbSingleCamera2022CameraOneSubsystem.class, true);
        
    
        // Do NOT enable the following during a competition.
        map.put(MotorTestSubsystem.class, false);
        //map.put(ShooterDEPRECATEDSubsystem.class, false);
        return map.build();
    }
    
    
    public static class CANs extends AbstractIOAssignments
    {
        // CAN IDs are assigned via the "Phoenix Tuner"
        // By default (out of the box) devices always have a device ID of 0 by default.
        // So we keep that ID unused.
        
        // Keep assignments in numerical order please
        // NOTE: DUPLICATE ASSIGNMENTS WILL RESULT IN THE ROBOT FAILING TO STARTUP (i.e. ERROR'S OUT)
        //       EVEN IF THE VARIABLE IS NOT USED ANYWHERE. ENSURE THERE ARE NO DUPLICATES :)
        //       See AbstractIOAssignments class level Javadoc for more details
        
        
        @SuppressWarnings("unused")
        public static final int DEFAULT_AND_THEREFORE_RESERVED_ID = 0;
        public static final int COLLECTOR_ARM_WINCH_MOTOR = 10; //TalonSRX
        public static final int COLLECTOR_INTAKE_MOTOR = 11;    //TalonSRX
        public static final int TRANSPORT_MOTOR = 12;           //TalonSRX
        
        /**
         * The CAN Bus ID for the Power Distribution Panel (PDP). We usually do not need to access
         * this at all. But we do document the CAN ID for it here to prevent conflicts.
         */
        @SuppressWarnings("unused")
        public static final int PDP = 9;
        
        public static final int DRIVE_LEFT_SIDE_REAR = 20;   // Rear/intake-end
        public static final int DRIVE_LEFT_SIDE_FRONT = 21;  // Front/shooter-end      // a.k.a. Driver's side
        public static final int DRIVE_RIGHT_SIDE_REAR = 22;  // Rear/intake-end
        public static final int DRIVE_RIGHT_SIDE_FRONT = 23; // Front/shooter-end     // a.k.a. Passenger's side
        
        public static final int BREECH_LIFT_MOTOR = 30;
        public static final int SHOOTER_2022_TOP_MOTOR = 31;
        public static final int SHOOTER_2022_BOT_MOTOR = 32;
        
        
        private CANs() {/* Static use only. */}
    }
    
    @API
    public static class DIOs extends AbstractIOAssignments
    {
        // Keep assignments in numerical order please
        // NOTE: DUPLICATE ASSIGNMENTS WILL RESULT IN THE ROBOT FAILING TO STARTUP (i.e. ERROR'S OUT) 
        //       EVEN IF THE VARIABLE IS NOT USED ANYWHERE. ENSURE THERE ARE NO DUPLICATES :)
        //       See AbstractIOAssignments class level Javadoc for more details
    
        public static final int COLLECTOR_ARM_UP_SENSOR = 0;
        public static final int COLLECTOR_ARM_DOWN_SENSOR = 1;
        
        /** TU / Transporter Ball Up Sensor that indicates the ball is at the top of the ball transporter, before falling into the shooter. */
        public static final int BALL_TOP_TRANSPORT_IR_SENSOR = 3;
    
        /** TI / Transporter Ball In Sensor that indicates the ball has been transferred from the ball intake to the ball transporter. */
        public static final int BALL_IN_TRANSPORT_IR_SENSOR = 2;
        
        /** SI / Shooter Ball In Sensor that indicates the ball is in the chamber/breech and is ready to be shot/fired. */
        public static final int BALL_IN_SHOOTER_IR_SENSOR = 8;
        
        public static final int BREECH_ARM_HOME_POSITION_SENSOR = 9;
        
        /*
            == NAVX DIO ADDRESSING
            navX-MXP  MXP Pin   RoboRIO 
            Port      Number    Channel Address
            0         DIO0      10
            1         DIO1      11
            2         DIO2      12
            3         DIO3      13
            -         -         14-17 reserved for internal navX use
            4         DIO8      18
            5         DIO9      19
            6         DIO10     20
            7         DIO11     21
            8         DIO12     22
            9         DIO13     23   
         */
        
        
        @SuppressWarnings("unused")
        public static final int RESERVED_FOR_INTERNAL_USE_BY_THE_NAVx_BOARD_14 = 14;
        @SuppressWarnings("unused")
        public static final int RESERVED_FOR_INTERNAL_USE_BY_THE_NAVx_BOARD_15 = 15;
        @SuppressWarnings("unused")
        public static final int RESERVED_FOR_INTERNAL_USE_BY_THE_NAVx_BOARD_16 = 16;
        @SuppressWarnings("unused")
        public static final int RESERVED_FOR_INTERNAL_USE_BY_THE_NAVx_BOARD_17 = 17;

        // Keep assignments in numerical order please
    
        
        private DIOs() {/* Static use only. */}
    }
    
    @API
    public static class AIOs extends AbstractIOAssignments
    {
        // Keep assignments in numerical order please
        // NOTE: DUPLICATE ASSIGNMENTS WILL RESULT IN THE ROBOT FAILING TO STARTUP (i.e. ERROR'S OUT) 
        //       EVEN IF THE VARIABLE IS NOT USED ANYWHERE. ENSURE THERE ARE NO DUPLICATES :)
        //       See AbstractIOAssignments class level Javadoc for more details
    
    
        private AIOs() {/* Static use only. */}
    }

    @API
    public static class PWMs extends AbstractIOAssignments
    {
        // Keep assignments in numerical order please
        // NOTE: DUPLICATE ASSIGNMENTS WILL RESULT IN THE ROBOT FAILING TO STARTUP (i.e. ERROR'S OUT) 
        //       EVEN IF THE VARIABLE IS NOT USED ANYWHERE. ENSURE THERE ARE NO DUPLICATES :)
        //       See AbstractIOAssignments class level Javadoc for more details
    
        /*
            === PWM Output Addressing ===   
            navX-MXP        MXP Pin         RoboRIO
            Port            Number          Channel Address
            0               PWM0            10
            1               PWM1            11
            2               PWM2            12
            3               PWM3            13
            4               PWM4            14
            5               PWM5            15
            6               PWM6            16
            7               PWM7            17
            8               PWM8            18
            9               PWM9            19
         */
    
        private PWMs() {/* Static use only. */}
    }

    /**
     * Port assignments for the Pneumatic Control Module (PCM).
     * [Not to be confused with the PWMs (Pulse Width Modulation) channel assignment class.]
     */
    @API
    public static class PCM extends AbstractIOAssignments
    {
        // Keep assignments in numerical order please 
        // NOTE: DUPLICATE ASSIGNMENTS WILL RESULT IN THE ROBOT FAILING TO STARTUP (i.e. ERROR'S OUT) 
        //       EVEN IF THE VARIABLE IS NOT USED ANYWHERE. ENSURE THERE ARE NO DUPLICATES :)
        //       See AbstractIOAssignments class level Javadoc for more details
    
        private PCM() {/* Static use only. */}
    }
    
    @API
    public static class Relays extends AbstractIOAssignments
    {
        // Keep assignments in numerical order please
    
        private Relays() {/* Static use only. */}
    }
    
    @SuppressWarnings("UseOfConcreteClass")
    @API
    public static class UI
    {
        @SuppressWarnings("unused")
        private static final Logger logger = LoggerFactory.getLogger(UI.class);
        private static final boolean USE_MULTIPLE_CONFIGS = false;
        private static final SortedSendableChooser<DriverControlConfig2022> configChooser = new SortedSendableChooser<>(Order.Insertion);
        private static final DriverControlConfig2022 defaultDriverControlConfig;
    
        public static DriverControlConfig2022 getDriveTrainControlConfiguration()
        {
            return (USE_MULTIPLE_CONFIGS) ? configChooser.getSelected() : defaultDriverControlConfig;
        }
    
        static
        {
            int loopCount = 0;
            while (!Subsystems.haveSubsystemsBeenInitialized() && (loopCount < 20))
            {
                loopCount++;
                try {TimeUnit.MILLISECONDS.sleep(100);} catch (InterruptedException ignore) {}
            }

            final Builder<DriverControlConfig2022> configsListBuilder = ImmutableList.builder();
            final Logger logger = LoggerFactory.getLogger(UI.class);

            /*
                     _                 _   _      _    
                    | | ___  _   _ ___| |_(_) ___| | __
                 _  | |/ _ \| | | / __| __| |/ __| |/ /
                | |_| | (_) | |_| \__ \ |_| | (__|   < 
                 \___/ \___/ \__, |___/\__|_|\___|_|\_\
                             |___/                                       
             */
            //~~Joystick Configuration~~
            final Joystick primaryStick = new Joystick(0);
    
            //trigger.whenPressed(CycleBreechCommand.INSTANCE);
            final Button fineControlButton = new JoystickButton(primaryStick, 1);
            final Button reverseModeButton = new JoystickButton(primaryStick, 2);
            
         
            final Button stopAllMotorsButton = new JoystickButton(primaryStick, 10);
            stopAllMotorsButton.whenPressed(new StopAllMotorsCommand());
            
            final AxisPair joystickAxisPair = new HidBasedAxisPair(primaryStick);
            //final AxisPair rateLimitedAxisPair = new SlewRateLimitedAxisPair(joystickAxisPair, 1.0, 5.0);
            // MITCH: Change joystick values here. Set to maximum desired absolute value. First number is x/rotate, second is y/speed
            final AxisPair rateLimitedAxisPair = new LimitingAxisPair(joystickAxisPair, 0.5, 1.0);

            defaultDriverControlConfig = new DriverControlConfig2022("Joystick Arcade Control on port 0",
                                                                     DriveControlMode.Arcade,
                                                                     rateLimitedAxisPair,
                                                                     reverseModeButton,
                                                                     fineControlButton
            );
            
            /*
                  ____                                      _ 
                 / ___| __ _ _ __ ___   ___ _ __   __ _  __| |
                | |  _ / _` | '_ ` _ \ / _ \ '_ \ / _` |/ _` |
                | |_| | (_| | | | | | |  __/ |_) | (_| | (_| |
                 \____|\__,_|_| |_| |_|\___| .__/ \__,_|\__,_|
                                           |_|                
               */
            
            //~~Gamepad/Xbox controller configurations~~
            final XboxController coDriverGamePad = new XboxController(1);
            final JoystickButton xBlueButton = new JoystickButton(coDriverGamePad, XboxController.Button.kX.value);
            final JoystickButton yYellowButton = new JoystickButton(coDriverGamePad, XboxController.Button.kY.value);
            final JoystickButton bRedButton = new JoystickButton(coDriverGamePad, XboxController.Button.kB.value);
            final JoystickButton aGreenButton = new JoystickButton(coDriverGamePad, XboxController.Button.kA.value);
            
            // MITCH: you can change the three predefined Shooter speeds here.
            xBlueButton.whenPressed(new StartShooterAtSpeed(700));
            yYellowButton.whenPressed(new StartShooterAtSpeed(1000));
            bRedButton.whenPressed(new StartShooterAtSpeed(1100));
            aGreenButton.whenPressed(new StopShooterCommand());
            final Button rightThrottleAsButton = new AxisAsButton(coDriverGamePad, Axis.kRightTrigger);
            rightThrottleAsButton.whenPressed(new ShootSequenceCommand());
            final JoystickButton leftTrigger = new JoystickButton(coDriverGamePad, XboxController.Button.kLeftBumper.value);
            leftTrigger.whenPressed(new StopIntakeAndTransportMotorsCommand());
            final JoystickButton reverseIntake = new JoystickButton(coDriverGamePad, XboxController.Button.kBack.value);
            reverseIntake.whenPressed(new StopIntakeMotorCommand());
            //reverseIntake.whileHeld(new ReverseIntakeMotorCommand());
            reverseIntake.whileHeld(new ReverseIntakeMotorWithDelayCommand());
            reverseIntake.whenReleased(new StopIntakeMotorCommand());
    
            final Button leftThrottleAsButton = new AxisAsButton(coDriverGamePad, Axis.kLeftTrigger);
            leftThrottleAsButton.whenPressed(new StartIntakeAndTransportMotorsCommand());
    
            final Button lowerCollectorButton = POVDirection.S_DOWN_180.asButton(coDriverGamePad);
            lowerCollectorButton.whenPressed(new LowerCollectorArm());
            lowerCollectorButton.whenReleased(new StopCollectorArm());
            final Button raiseCollectorButton = POVDirection.N_UP_0.asButton(coDriverGamePad);
            raiseCollectorButton.whenPressed(new RaiseCollectorArm());
            raiseCollectorButton.whenReleased(new StopCollectorArm());
    
    
    
            configsListBuilder.add(defaultDriverControlConfig);
            
            
            // ** If we ever use multiple configs again, create next config here, and add to configs list builder **
            
            
            
            if (USE_MULTIPLE_CONFIGS)
            {
                final ImmutableList<DriverControlConfig2022> configs = configsListBuilder.build();

                if (configs.isEmpty())
                {
                    logger.error("No joystick or gamepad is connected. Cannot create a UI configuration.");
                    final DriverControlConfig2022 noOpConfig = DriverControlConfig2022.getDriverControlConfigNoOpInstance();
                    configChooser.setDefaultOption("No operation - no joystick/gamepad is connected.", noOpConfig);
                }
                else if (configs.size() == 1)
                {
                    final DriverControlConfig2022 config = configs.get(0);
                    configChooser.setDefaultOption(config.getConfigurationName(), config);
                }
                else
                {
                    for (int i = 0; i < configs.size(); i++)
                    {
                        final DriverControlConfig2022 config = configs.get(i);
                        final String prefix = "# " + (i + 1) + ") ";
                        if (config.equals(defaultDriverControlConfig))
                        {
                            configChooser.setDefaultOption(prefix + config.getConfigurationName(), config);
                        }
                        else
                        {
                            configChooser.addOption(prefix + config.getConfigurationName(), config);
                        }
                    }
                }

                SmartDashboard.putData("==UI Configuration Chooser==", configChooser);
            }
            
        } // end static initializer block


        /*
            NOTE: Look into the newer Button API:
            
            final Trigger intakeTrigger = new JoystickButton(m_operatorController, XboxController.Button.kBumperRight.value).or(new JoystickButton(m_driverController, XboxController.Button.kBumperRight.value));
            intakeTrigger
                .whenActive(new InstantCommand(() -> m_intake.toggleIntakePosition(true))
                .andThen(new WaitUntilCommand(() -> !intakeTrigger.get())
                .andThen(new InstantCommand(() -> m_intake.toggleIntakeWheels(true))))
                .withTimeout(0.5));                
         */
        
        
        @API
        public static Button createAndAssignButton(final GenericHID hid,
                                                   final int buttonNumber,
                                                   final ButtonAction action,
                                                   final Command command) throws IllegalArgumentException
        {
            final JoystickButton button = new JoystickButton(hid, buttonNumber);
            return assignButton(button, action, command);
        }


        @API
        public static Button assignButton(Button button, ButtonAction action, Command command)
        {
            action.assign(button, command);
            return button;
        }
    
    
        private UI() {/* Static use only. */}
    }
}
