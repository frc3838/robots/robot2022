package frc.team3838.core.hardware;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.motorcontrol.MotorController;
import frc.team3838.core.meta.API;
import frc.team3838.core.utils.MathUtils;



/**
 * A SpeedController to wrap another speed controller that scales the set value to a 'usable range'.
 * For example, say there is a motor that stalls at ±0.25 or lower, and we do not want to go faster than
 * ±0.85. It's (absolute) usable range then is 0.25 to 0.85, a range of 0.6. If we get Joystick input of 0.5
 * (to indicate 50%), we do not want to set the motor to 0.5, but rather half of its usable range.
 * Half of the 0.6 range is 0.3, added to the min speed of 0.253 tells us that 50% of the usable range is
 * 0.55 (0.25 + 0.3). Thus, when configured with a min speed of 0.25 and a max speed of 0.85,  a call to
 * this class' <tt>set(0.5)</tt> will call the wrapped SpeedController with <tt>set(0.55)</tt>. Similarly,
 * a value of -0.5 (i.e. 50% in reverse) would set -0.55 on the wrapped controller.
 * <p>
 * <strong>Important:</strong> This class can only wrap speed controllers that have a speed range of -1.0 to 1.0.
 * It cannot wrap PWM based controllers that use a range of 0 to 255.
 */
@API
public class ScalingMotorController implements MotorController
{
    private static final Logger logger = LoggerFactory.getLogger(ScalingMotorController.class);

    @Nonnull
    private final MotorController wrappedMotorController;
    private final double minOrStallSpeed;
    private final double maxSpeed;


    @API
    public ScalingMotorController(@Nonnull MotorController wrappedMotorController)
    {
        this(wrappedMotorController, 0, 1.0);
    }


    @API
    public ScalingMotorController(@Nonnull MotorController wrappedMotorController, double minOrStallSpeed)
    {
        this(wrappedMotorController, minOrStallSpeed, 1.0);
    }


    @API
    public ScalingMotorController(@Nonnull MotorController wrappedMotorController, double minOrStallSpeed, double maxSpeed)
    {
        this.wrappedMotorController = wrappedMotorController;
        this.minOrStallSpeed = minOrStallSpeed;
        this.maxSpeed = maxSpeed;
    }


    @Override
    public double get() {return wrappedMotorController.get();}


    @Override
    public void set(double speed)
    {
        double scaledSpeed = MathUtils.scaleRange(Math.abs(speed), 0, 1.0, minOrStallSpeed, maxSpeed);
        scaledSpeed = MathUtils.isNegative(speed) ? -scaledSpeed : scaledSpeed;
        logger.trace("Scaling speed of {} to {} (within the range of {} to {})", speed, scaledSpeed, minOrStallSpeed, maxSpeed);
        wrappedMotorController.set(scaledSpeed);
    }


    @Override
    public void setInverted(boolean isInverted) {wrappedMotorController.setInverted(isInverted);}


    @Override
    public boolean getInverted() {return wrappedMotorController.getInverted();}


    @Override
    public void disable() {wrappedMotorController.disable();}


    @Override
    public void stopMotor() {wrappedMotorController.stopMotor();}



    @Nullable
    public static final String SMART_DASHBOARD_TYPE = "ScalingMotorController";





}
