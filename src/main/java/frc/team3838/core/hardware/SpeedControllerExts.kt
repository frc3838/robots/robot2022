package frc.team3838.core.hardware

import com.ctre.phoenix.ErrorCode
import com.ctre.phoenix.motorcontrol.ControlMode
import com.ctre.phoenix.motorcontrol.IMotorController
import com.ctre.phoenix.motorcontrol.InvertType
import com.ctre.phoenix.motorcontrol.NeutralMode
import com.ctre.phoenix.motorcontrol.can.BaseMotorController
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX
import com.google.common.collect.ImmutableList
import com.revrobotics.CANSparkMax
import com.revrobotics.CANSparkMaxLowLevel
import com.revrobotics.REVLibError
import frc.team3838.core.meta.API
import org.slf4j.Logger

private val logger = mu.KotlinLogging.logger {}


@JvmOverloads
fun createWpiTalonSRX(canDeviceNumber: Int, invert: Boolean = false, neutralMode: NeutralMode = NeutralMode.Brake): WPI_TalonSRX
{
    val controller = WPI_TalonSRX(canDeviceNumber)
    controller.initCtreMotorController(invert, neutralMode)
    return controller
}

@JvmOverloads
fun createWpiVictorSPX(canDeviceNumber: Int, invert: Boolean = false, neutralMode: NeutralMode = NeutralMode.Brake): WPI_VictorSPX
{
    val controller = WPI_VictorSPX(canDeviceNumber)
    controller.initCtreMotorController(invert, neutralMode)
    return controller
}

@JvmOverloads
fun createCanSparkMax(canDeviceNumber: Int, motorType:CANSparkMaxLowLevel.MotorType,
                      invert: Boolean = false, idleMode: CANSparkMax.IdleMode = CANSparkMax.IdleMode.kBrake): CANSparkMax
{
    val controller = CANSparkMax(canDeviceNumber, motorType)
    controller.restoreFactoryDefaults()
    val revLibError = controller.clearFaults()
    if (revLibError != REVLibError.kOk)
    {
        val msg = String.format(
            "An error occurred when setting CAN Controller on channel %d to factory default configuration. Error Code: %s",
            canDeviceNumber,
            revLibError
                               )
        logger.error(msg)
        throw IllegalStateException(msg)
    }
    controller.inverted = invert
    val revLibErrorIdeMode = controller.setIdleMode(idleMode)
    checkErrorResponse(revLibErrorIdeMode, controller, "Setting IdleMode", logger)
    return controller
}

fun checkErrorResponse(revLibError: REVLibError, sparkMax: CANSparkMax, activity: String, logger: Logger)
{
    if (revLibError != REVLibError.kOk)
    {
        val msg = String.format(
            "An error occurred when configuring CAN Controller with ID %d. Activity: %s. Error Code: %s",
            sparkMax.getDeviceId(),
            activity,
            revLibError
                               )
        logger.error(msg)
        throw IllegalStateException(msg)
    }
}

@JvmOverloads
fun BaseMotorController.initCtreMotorController(invert: Boolean = false, neutralMode: NeutralMode = NeutralMode.Brake)
{
    val errorCode: ErrorCode = this.configFactoryDefault(50 /*milliseconds*/)
    if (errorCode != ErrorCode.OK)
    {
        val msg = "An error occurred when setting Motor Controller, device ID ${this.deviceID}, to factory default configuration. Error Code: $errorCode"
        logger.error(msg)
        throw IllegalStateException(msg)
    }
    this.set(ControlMode.PercentOutput, 0.0)
    this.setNeutralMode(neutralMode)
    this.inverted = invert // Internally this calls either setInverted(InvertType.InvertMotorOutput) or setInverted(InvertType.None)
}


/**
 * Creates and configures a new WPI_TalonSRX motor controller.
 *
 * @param deviceNumber the CAN BUS device number
 * @param invertType Whether to invert the motor (`InvertType.InvertMotorOutput`, i.e. `true`)
 *                    or not (`InvertType.None`, i.e. `false`). The follower based values
 *                    of `InvertType.FollowMaster` or `InvertType.OpposeMaster` should
 *                    generally not be used here. Instead use the `newWpiTalonSrxFollower`
 *                    function. Defaults to `InvertType.None`.
 *  @param neutralMode  the mode of operation during neutral throttle output. Defaults to `NeutralMode.Brake`
 */
@API
@JvmOverloads
fun newWpiTalonSrx(deviceNumber: Int,
                   invertType: InvertType = InvertType.None,
                   neutralMode: NeutralMode = NeutralMode.Brake): WPI_TalonSRX
{
    val controller = WPI_TalonSRX(deviceNumber)
    warnIfFollowerInvertType(invertType, "newWpiTalonSrx")
    configureController(controller, invertType, neutralMode)
    return controller
}

@API
@JvmOverloads
fun newWpiTalonSrxFollower(deviceNumber: Int,
                           leaderToFollow: IMotorController,
                           invertType: InvertType = InvertType.FollowMaster,
                           neutralMode: NeutralMode = NeutralMode.Brake): WPI_TalonSRX
{
    val controller = WPI_TalonSRX(deviceNumber)
    warnIfNotFollowerInvertType(invertType, "newWpiTalonSrxFollower")
    configureControllerAsFollower(controller, leaderToFollow, invertType, neutralMode)
    return controller
}

@API
@JvmOverloads
fun newWpiTalonSrxFollowers(deviceNumbers: Collection<Int>,
                            leaderToFollow: IMotorController,
                            invertType: InvertType = InvertType.FollowMaster,
                            neutralMode: NeutralMode = NeutralMode.Brake): ImmutableList<WPI_TalonSRX>
{
    val builder = ImmutableList.builder<WPI_TalonSRX>()
    for (deviceNumber in deviceNumbers)
    {
        builder.add(newWpiTalonSrxFollower(deviceNumber, leaderToFollow, invertType, neutralMode))
    }
    return builder.build()
}

/**
 * Creates and configures a new WPI_VictorSPX motor controller.
 *
 * @param deviceNumber the CAN BUS device number
 * @param invertType Whether to invert the motor (`InvertType.InvertMotorOutput`, i.e. `true`)
 *                    or not (`InvertType.None`, i.e. `false`). The follower based values
 *                    of `InvertType.FollowMaster` or `InvertType.OpposeMaster` should
 *                    generally not be used here. Instead use the `newWpiVictorSpxFollower`
 *                    function. Defaults to `InvertType.None`.
 *  @param neutralMode  the mode of operation during neutral throttle output. Defaults to `NeutralMode.Brake`
 */
@API
@JvmOverloads
fun newWpiVictorSpx(deviceNumber: Int,
                    invertType: InvertType = InvertType.None,
                    neutralMode: NeutralMode = NeutralMode.Brake): WPI_VictorSPX
{
    val controller = WPI_VictorSPX(deviceNumber)
    warnIfFollowerInvertType(invertType, "newWpiVictorSpx")
    configureController(controller, invertType, neutralMode)
    return controller
}

@API
@JvmOverloads
fun newWpiVictorSpxFollower(deviceNumber: Int,
                            leaderToFollow: IMotorController,
                            invertType: InvertType = InvertType.FollowMaster,
                            neutralMode: NeutralMode = NeutralMode.Brake): WPI_VictorSPX
{
    val controller = WPI_VictorSPX(deviceNumber)
    warnIfNotFollowerInvertType(invertType, "newWpiVictorSpxFollower")
    configureControllerAsFollower(controller, leaderToFollow, invertType, neutralMode)
    return controller
}

@API
@JvmOverloads
fun newWpiVictorSpxFollowers(deviceNumbers: Collection<Int>,
                             leaderToFollow: IMotorController,
                             invertType: InvertType = InvertType.FollowMaster,
                             neutralMode: NeutralMode = NeutralMode.Brake): ImmutableList<WPI_VictorSPX>
{
    val builder = ImmutableList.builder<WPI_VictorSPX>()
    for (deviceNumber in deviceNumbers)
    {
        builder.add(newWpiVictorSpxFollower(deviceNumber, leaderToFollow, invertType, neutralMode))
    }
    return builder.build()
}

private fun configureControllerAsFollower(controller: BaseMotorController,
                                          leaderToFollow: IMotorController,
                                          invertType: InvertType = InvertType.None,
                                          neutralMode: NeutralMode = NeutralMode.Brake)
{
    configureController(controller, invertType, neutralMode)
    controller.follow(leaderToFollow)
}


private fun configureController(controller: BaseMotorController,
                                invertType: InvertType = InvertType.None,
                                neutralMode: NeutralMode = NeutralMode.Brake)
{
    controller.setInverted(invertType)
    controller.setNeutralMode(neutralMode)
}

private fun warnIfFollowerInvertType(invertType: InvertType, functionName: String)
{
    if (invertType == InvertType.FollowMaster || invertType == InvertType.OpposeMaster)
    {
        logger.warn {
            "In the '$functionName' function, a follower based invertType of '$invertType' is being set " +
            "on a motor controller NOT being designated as a follower. Consider " +
            "using the '${functionName}Follower' function instead of the '$functionName' function " +
            "if you need to create a motor controller acting as a follower."
        }
    }
}

private fun warnIfNotFollowerInvertType(invertType: InvertType, functionName: String)
{
    if (invertType != InvertType.FollowMaster && invertType != InvertType.OpposeMaster)
    {
        logger.warn {
            "In the '$functionName' function, a non-follower based invertType of '$invertType' is being set " +
            "on a motor controller being designated as a follower. When creating a follower, either the " +
            "'InvertType.FollowMaster' or 'InvertType.OpposeMaster' InvertType should be used."
        }
    }
}