package frc.team3838.core.hardware

import edu.wpi.first.wpilibj.DigitalInput
import frc.team3838.core.meta.API
import frc.team3838.core.utils.time.getFpgaTimeInMilliseconds
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

/**
 * Class to provide Limit Switch operations with meaningful method names.
 * Use the factory methods to create. Use the [.isActivated] to detect trigger.
 * Example Uses:
 * ```
 * LimitSwitch bottomLimitSwitch = LimitSwitch.createNormal(DIOs.ELEVATOR_BOTTOM_LIMIT_SWITCH);
 * LimitSwitch topLimitSwitch = LimitSwitch.createNormal(DIOs.ELEVATOR_TOP_LIMIT_SWITCH);
 *
 * LimitSwitch climberLimitSwitch = LimitSwitch.createInverted(DIOs.CLIMBER_LIMIT_SWITCH);
 * if (climberLimitSwitch.isActivated())
 * {
 *     stopClimberMotor();
 * }
 *```
 */
interface LimitSwitch
{
    val invert: Boolean

    /**
     * Property to indicate if the switch is activated (`true`) or not ('false).
     *
     * @see isNotActivated
     */
    @get:API
    val isActivated: Boolean

    /**
     * Property to indicate if the switch is *not* activated (`true`) or is ('false).
     * @see isActivated
     */
    @get:API
    val isNotActivated: Boolean
        get() = !isActivated

    /**
     * An alternative to using the [isActivated] and/or [isNotActivated] properties.
     * This will return the same value as the [isActivated] property.
     *
     * @see isActivated
     * @see isNotActivated
     */
    @API
    fun get(): Boolean = isActivated

    companion object
    {
        /**
         * Creates a [LimitSwitch] for the supplied DIO.
         *
         * @param dio the DIO port the limit switch is on/uses.
         * @param invert whether to invert the limit switch; default is `false`
         */
        @API
        @JvmOverloads
        fun create(dio: Int, invert: Boolean = false): LimitSwitch = SimpleLimitSwitch(dio, invert)

        /**
         * Creates a [LimitSwitch] for the supplied [DigitalInput].
         *
         * @param digitalInput the DigitalInput the limit switch is on/uses.
         * @param invert whether to invert the limit switch; default is `false`
         */
        @API
        @JvmOverloads
        fun create(digitalInput: DigitalInput, invert: Boolean = false): LimitSwitch = SimpleLimitSwitch(digitalInput, invert)

        @API
        @JvmOverloads
        fun createNoOp(dummyValue: Boolean = false): LimitSwitch = NoOpLimitSwitch(dummyValue)

        @API
        @JvmOverloads
        fun createDebounced(dio: Int, debounceDelayMs: Int = 50, invert: Boolean = false): LimitSwitch =
            DebouncedAltSteadySateStrategyLimitSwitch(DigitalInput(dio), debounceDelayMs, invert)

        @API
        @JvmOverloads
        fun createDebounced(digitalInput: DigitalInput, debounceDelayMs: Int = 50, invert: Boolean = false): LimitSwitch =
            DebouncedAltSteadySateStrategyLimitSwitch(digitalInput, debounceDelayMs, invert)

        private class NoOpLimitSwitch(private val dummyValue: Boolean) : LimitSwitch
        {
            override val invert: Boolean
                get() = false
            override val isActivated: Boolean
                get() = dummyValue
        }
        @API
        private class SimpleLimitSwitch(digitalInput: DigitalInput, invert: Boolean = false) : LimitSwitch
        {
            override val invert: Boolean
            private val digitalInput: DigitalInput?

            constructor(dio: Int, invert: Boolean = false): this(DigitalInput(dio), invert)

            init
            {
                require(digitalInput.channel >= 0) { "DigitalInput specified is invalid as its DIO channel is less than zero. It was: ${digitalInput.channel}" }
                this.digitalInput = digitalInput
                this.invert = invert
            }

            /**
             * Property to indicate if the switch is activated (`true`) or not ('false).
             * @see isNotActivated
             */
            @get:API
            override val isActivated: Boolean
                get() = (invert != digitalInput!!.get())
        }
    }

    private abstract class AbstractDebouncedLimitSwitch(digitalInput: DigitalInput, debounceDelayMs: Int = 50, invert: Boolean = false) : LimitSwitch
    {
        protected val debounceDelayMs: Int
        final override val invert: Boolean
        protected val digitalInput: DigitalInput?
        protected var currentReportedState: Boolean
        protected var lastDebounceTime = 0L

        abstract val stateReader: Runnable

        protected val executor = Executors.newScheduledThreadPool(1).apply {
            scheduleAtFixedRate(stateReader, 10, 5, TimeUnit.MILLISECONDS)
        }


        init
        {
            require(digitalInput.channel >= 0) { "DigitalInput specified is invalid as its DIO channel is less than zero. It was: ${digitalInput.channel}" }
            this.digitalInput = digitalInput
            this.invert = invert
            this.debounceDelayMs = debounceDelayMs
            this.currentReportedState = getState()

            Runtime.getRuntime().addShutdownHook(object : Thread()
                                                 {
                                                     override fun run()
                                                     {
                                                         try
                                                         {
                                                             executor.shutdownNow()
                                                         }
                                                         catch (ignore: Throwable) { }
                                                     }
                                                 })
        }

        protected fun getState(): Boolean = (invert != digitalInput!!.get())

        /**
         * Property to indicate if the switch is activated (`true`) or not ('false).
         * @see isNotActivated
         */
        @get:API
        override val isActivated: Boolean
            get() = currentReportedState
    }

    private class DebouncedSteadySateStrategyLimitSwitch(digitalInput: DigitalInput, debounceDelayMs: Int = 50, invert: Boolean = false): AbstractDebouncedLimitSwitch(digitalInput, debounceDelayMs, invert)
    {
        override val stateReader: Runnable
            get() = Runnable {
                // https://digilent.com/reference/learn/microprocessor/tutorials/debouncing-via-software/start
                val presentState = getState()
                val timeNow = getFpgaTimeInMilliseconds()
                if (presentState != currentReportedState)
                {
                    // every time the switch state changes, get the time of that change
                    lastDebounceTime = timeNow
                }
                if (timeNow - lastDebounceTime > debounceDelayMs)
                {
                    // if the difference between the last time the state changed is greater than the delay period
                    // it is safe to say the limit switch is in the final steady state, so set the current state
                    currentReportedState = presentState
                }
            }
    }

    private class DebouncedAltSteadySateStrategyLimitSwitch(digitalInput: DigitalInput, debounceDelayMs: Int = 10, invert: Boolean = false): AbstractDebouncedLimitSwitch(digitalInput, debounceDelayMs, invert)
    {
        override val stateReader: Runnable
            get() = Runnable {
                // https://www.thegeekpub.com/246471/debouncing-a-switch-in-hardware-or-software/
                // We check how long it has been since the switch was last pressed.
                // If it was less than debounceDelayMs, we ignore the “press” as it would be an erroneous bounce.
                // sample the state of the switch - is it pressed or not?
                val presentState = getState()
                val timeNow = getFpgaTimeInMilliseconds()
                // filter out any noise by setting a time buffer
                if (timeNow - lastDebounceTime > debounceDelayMs)
                {
                    // if the switch has been pressed, lets toggle the currentReportedState
                    currentReportedState = presentState
                    lastDebounceTime = timeNow
                }
            }
    }
}