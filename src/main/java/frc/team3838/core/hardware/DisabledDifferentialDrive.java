package frc.team3838.core.hardware;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.motorcontrol.MotorController;
import frc.team3838.core.meta.API;



/**
 * A {@link DifferentialDrive} implementation that does nothing. It can be used so {@code DifferentialDrive}
 * or {@code RobotDriveBase}variables/fields can be guaranteed to not be null. Furthermore, it prevents
 * "... Output not updated often enough" warnings by managing the the motor safety watchdog.
 */
@API
public class DisabledDifferentialDrive extends DifferentialDrive
{
    @SuppressWarnings("WeakerAccess")
    public static final MotorController noOpMotorController = new NoOpMotorController();

    @SuppressWarnings("FieldCanBeLocal")
    private final ScheduledExecutorService scheduledExecutorService;

    public DisabledDifferentialDrive()
    {
        super(noOpMotorController, noOpMotorController);

        // m_safetyHelper was removed in 2019. New Motor Safety was reworked in 2019
        // It is enabled on the speed controllers
        // http://wpilib.screenstepslive.com/s/currentCS/m/java/l/599705-using-the-motor-safety-feature

        setSafetyEnabled(true);
        scheduledExecutorService = Executors.newScheduledThreadPool(1);
        scheduledExecutorService.scheduleAtFixedRate(this::feed, 0, 20, TimeUnit.MILLISECONDS);
    }

    public MotorController getMotorController() { return DisabledDifferentialDrive.noOpMotorController; }

}
