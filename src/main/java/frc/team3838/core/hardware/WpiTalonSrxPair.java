package frc.team3838.core.hardware;

import javax.annotation.Nonnull;

import com.ctre.phoenix.motorcontrol.InvertType;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.motorcontrol.MotorController;
import frc.team3838.core.meta.API;



@API
public class WpiTalonSrxPair extends AbstractCanBusMotorPair<WPI_TalonSRX> implements MotorController
{
    public WpiTalonSrxPair(int leaderDeviceNumber,
                           int followerDeviceNumber,
                           boolean invertLeader,
                           @Nonnull InvertType followerInvertType)
    {
        super(leaderDeviceNumber, followerDeviceNumber, invertLeader, followerInvertType);
    }
    
    @Nonnull
    @Override
    protected WPI_TalonSRX createCanController(int deviceNumber)
    {
        return new WPI_TalonSRX(deviceNumber);
    }
}
