package frc.team3838.core.hardware;

import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ctre.phoenix.motorcontrol.InvertType;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import com.revrobotics.REVLibError;

import edu.wpi.first.wpilibj.motorcontrol.MotorController;
import frc.team3838.core.logging.LaconicLogger;
import frc.team3838.core.meta.API;


// https://codedocs.revrobotics.com/java/com/revrobotics/cansparkmax
public class CANSparkMaxPair implements MotorController,
                                                 MotorControllerPair
{
    protected final Logger logger = LoggerFactory.getLogger(CANSparkMaxPair.class + "-" + getClass());
    private final LaconicLogger setInvertedLaconicLogger = new LaconicLogger(logger, 30, TimeUnit.SECONDS);

    // Example See https://github.com/CrossTheRoadElec/Phoenix-Examples-Languages/blob/master/Java/SixTalonArcadeDrive/src/main/java/frc/robot/Robot.java
    private final CANSparkMax leader;
    private final CANSparkMax follower;
    
    /**
     *  @param leaderDeviceNumber the leader's CAN device Number
     * @param followerDeviceNumber the follower's CAN device Number
     * @param invertLeader whether to invert the leader
     * @param invertFollowerFromLeader sets if the follower is inverted <b>as compared to the leader</b>.
*                                 When false, the follower's output will be the same as the leader.
     * @param motorType the motorYpe (i.e. brushed or brushless)
     */
    public CANSparkMaxPair(int leaderDeviceNumber,
                           int followerDeviceNumber,
                           boolean invertLeader,
                           boolean invertFollowerFromLeader,
                           MotorType motorType,
                           IdleMode idleMode)
    {
        leader = createAndInitCanController(leaderDeviceNumber, motorType);
        follower = createAndInitCanController(followerDeviceNumber, motorType);
        leader.setInverted(invertLeader);
        checkErrorResponse(leader.setIdleMode(idleMode), leader, "setIdleMode to " + idleMode);
        checkErrorResponse(follower.follow(leader, invertFollowerFromLeader), follower, "setting as follower");
        leader.stopMotor();
    }


    @Nonnull
    private CANSparkMax createAndInitCanController(int channel, MotorType motorType)
    {
        CANSparkMax controller = new CANSparkMax(channel, motorType);
        final REVLibError revLibError = controller.clearFaults();
        if (revLibError != REVLibError.kOk)
        {
            final String msg = String.format("An error occurred when setting CAN Controller on channel %d to factory default configuration. Error Code: %s",
                                             channel,
                                             revLibError);
            logger.error(msg);
            throw new IllegalStateException(msg);
        }
        return controller;
    }
    
    private void checkErrorResponse(@NotNull REVLibError revLibError, @NotNull CANSparkMax sparkMax, @NotNull String activity)
    {
        if (revLibError != REVLibError.kOk)
        {
            final String msg = String.format("An error occurred when configuring CAN Controller with ID %d. Activity: %s. Error Code: %s",
                                             sparkMax.getDeviceId(),
                                             activity,
                                             revLibError);
            logger.error(msg);
            throw new IllegalStateException(msg);
        }
    }
    

    @API
    public CANSparkMax getLeader()
    {
        return leader;
    }


    @API
    public CANSparkMax getFollower()
    {
        return follower;
    }


    @Override
    public double get()
    {
        return leader.get();
    }


    @Override
    public void set(double speed)
    {
        leader.set(speed);
    }
    
    
    @Override
    public void setVoltage(double outputVolts)
    {
       leader.setVoltage(outputVolts);
    }
    
    
    /**
     * <b>This is an unsupported operations the Rev Robotics controllers. Do the motor inversion via the constructor.</b>
     */
    @API
    @Override
    public void setInverted(boolean invertLeader, @Nonnull InvertType followerInvertType)
    {
        
        throw new UnsupportedOperationException("The 'setInverted' method is unsupported for the Rev Robotics controllers. Do the motor inversion via the constructor");
    }


    /**
     * <p>
     * <strong style="color:red">
     * For the Motor Pairs classes, this {@code setInverted} method should typically
     * not be used. Instead use the {@link #setInverted(boolean, InvertType)}
     * </strong>
     * </p>
     * <p>
     * {@inheritDoc}
     * </p>
     */
    @Override
    public void setInverted(boolean isInverted)
    {
        setInvertedLaconicLogger.warn("The setInverted(boolean) method was called on a motor pair. In general, it is preferable to use the setInverted(boolean, InvertType) for a motor pair.");
        setInverted(isInverted, InvertType.FollowMaster);
    }


    @Override
    public boolean getInverted() {return leader.getInverted();}


    @Override
    public void disable()
    {
        stopMotor();
        leader.disable();
    }


    @Override
    public void stopMotor()
    {
        leader.stopMotor();
    }
    
}
