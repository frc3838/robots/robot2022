package frc.team3838.core.hardware;

import javax.annotation.Nonnull;

import com.ctre.phoenix.motorcontrol.InvertType;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;

import edu.wpi.first.wpilibj.motorcontrol.MotorController;
import frc.team3838.core.meta.API;



/**
 * See, and generally use, the {@link WpiVictorSpxPair} rather than this class.
 */
@API
public class VictorSpxPair extends AbstractCanBusMotorPair<VictorSPX> implements MotorController
{
    public VictorSpxPair(int leaderDeviceNumber,
                         int followerDeviceNumber,
                         boolean invertLeader,
                         @Nonnull InvertType followerInvertType)
    {
        super(leaderDeviceNumber, followerDeviceNumber, invertLeader, followerInvertType);
    }
    
    
    @Override
    @Nonnull
    protected VictorSPX createCanController(int deviceNumber)
    {
        return new VictorSPX(deviceNumber);
    }
}
