package frc.team3838.core.hardware;

import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

import edu.wpi.first.wpilibj.motorcontrol.MotorController;
import frc.team3838.core.meta.API;



/**
 * A {@link edu.wpi.first.wpilibj.motorcontrol.MotorController} implementation that wraps two or more {@code MotorControllers} in parallel such that
 * an operation on an instance of this {@code ParallelMotorController} is called on all the wrapped controllers.
 * For example, is a drivetrain has two motors driving a single wheel, the two speed controllers for those two
 * motors can be wrapped in this class so that operations can be called a single time.
 */
public class ParallelMotorController implements MotorController
{

    private final ImmutableList<MotorController> motorControllers;
    private MotorController refMotorController;


    @API
    public ParallelMotorController(@Nonnull MotorController motorController, @Nonnull MotorController motorController2)
    {
        //noinspection ZeroLengthArrayAllocation
        this(motorController, motorController2, new MotorController[0]);
    }


    @API
    public ParallelMotorController(Iterable<MotorController> motorControllers)
    {
        this.motorControllers = ImmutableList.copyOf(motorControllers);
        construct();
    }

    @SuppressWarnings("WeakerAccess")
    @API
    public ParallelMotorController(@Nonnull MotorController motorController, @Nonnull MotorController motorController2, MotorController... additionalMotorControllers)
    {
        final Builder<MotorController> collectionBuilder = ImmutableList.builder();
        collectionBuilder.add(motorController);
        collectionBuilder.add(motorController2);
        for (MotorController controller : additionalMotorControllers)
        {
            collectionBuilder.add(controller);
        }
        this.motorControllers = collectionBuilder.build();
        construct();
    }



    private void construct()
    {
        this.refMotorController = this.motorControllers.get(0);
        inversionCheck();
    }

    private void inversionCheck()
    {
        boolean allHaveSameInversion = true;

        boolean invertState = refMotorController.getInverted();
        for (MotorController motorController : motorControllers)
        {
            if (motorController.getInverted() != invertState)
            {
                allHaveSameInversion = false;
                break;
            }
        }
        if (!allHaveSameInversion)
        {
            throw new IllegalStateException("Not all speed controllers added to the ParallelMotorController have the same invert state. This "
                                            + "class cannot handle such a scenario at this time.");
        }
    }

    @Override
    public double get()
    {
        return refMotorController.get();
    }


    @Override
    public void set(double speed)
    {
        for (MotorController motorController : motorControllers)
        {
            motorController.set(speed);
        }
    }


    @Override
    public void setInverted(boolean isInverted)
    {
        for (MotorController motorController : motorControllers)
        {
            motorController.setInverted(isInverted);
        }
    }


    @Override
    public boolean getInverted()
    {
        return refMotorController.getInverted();
    }


    @Override
    public void disable()
    {
        for (MotorController motorController : motorControllers)
        {
            motorController.disable();
        }
    }


    @Override
    public void stopMotor()
    {
        for (MotorController motorController : motorControllers)
        {
            motorController.stopMotor();
        }
    }

}
