package frc.team3838.core.hardware;

import edu.wpi.first.wpilibj.motorcontrol.MotorController;



public class NoOpMotorController implements MotorController
{
    private static final NoOpMotorController INSTANCE = new NoOpMotorController();

    public static NoOpMotorController getInstance()
    {
        return INSTANCE;
    }

    @Override
    public double get() { return 0; }


    @Override
    public void set(double speed) { }


    @Override
    public void setInverted(boolean isInverted) { }


    @Override
    public boolean getInverted() { return false; }


    @Override
    public void disable() { }


    @Override
    public void stopMotor() { }
}
