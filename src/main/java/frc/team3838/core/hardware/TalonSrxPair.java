package frc.team3838.core.hardware;

import javax.annotation.Nonnull;

import com.ctre.phoenix.motorcontrol.InvertType;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;

import edu.wpi.first.wpilibj.motorcontrol.MotorController;
import frc.team3838.core.meta.API;



/**
 * See, and generally use, the {@link WpiTalonSrxPair} rather than this class.
 */
@API
public class TalonSrxPair extends AbstractCanBusMotorPair<TalonSRX> implements MotorController
{
    public TalonSrxPair(int leaderDeviceNumber,
                        int followerDeviceNumber,
                        boolean invertLeader,
                        @Nonnull InvertType followerInvertType)
    {
        super(leaderDeviceNumber, followerDeviceNumber, invertLeader, followerInvertType);
    }
    
    @Nonnull
    @Override
    protected TalonSRX createCanController(int deviceNumber)
    {
        return new TalonSRX(deviceNumber);
    }
}
