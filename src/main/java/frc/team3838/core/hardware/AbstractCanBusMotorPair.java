package frc.team3838.core.hardware;

import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ctre.phoenix.ErrorCode;
import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.InvertType;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.BaseMotorController;

import edu.wpi.first.wpilibj.motorcontrol.MotorController;
import frc.team3838.core.logging.LaconicLogger;
import frc.team3838.core.meta.API;



public abstract class AbstractCanBusMotorPair<T extends BaseMotorController> implements MotorController,
                                                                                        MotorControllerPair
{
    protected final Logger logger = LoggerFactory.getLogger(AbstractCanBusMotorPair.class + "-" + getClass());
    private final LaconicLogger setInvertedLaconicLogger = new LaconicLogger(logger, 30, TimeUnit.SECONDS);

    // Example See https://github.com/CrossTheRoadElec/Phoenix-Examples-Languages/blob/master/Java/SixTalonArcadeDrive/src/main/java/frc/robot/Robot.java
    private final T leader;
    private final T follower;
    private final ControlMode controlMode;


    AbstractCanBusMotorPair(int leaderDeviceNumber,
                            int followerDeviceNumber,
                            boolean invertLeader,
                            @Nonnull InvertType followerInvertType)
    {
        controlMode = ControlMode.PercentOutput; // TODO add as constructor arg (After converting to Kotlin)
        leader = createAndInitCanController(leaderDeviceNumber);
        follower = createAndInitCanController(followerDeviceNumber);
        follower.follow(leader);
        leader.set(controlMode, 0.0);
        setInverted(invertLeader, followerInvertType);
    }


    @Nonnull
    private T createAndInitCanController(int channel)
    {
        T controller = createCanController(channel);
        final ErrorCode errorCode = controller.configFactoryDefault(50 /*milliseconds*/);
        if (errorCode != ErrorCode.OK)
        {
            final String msg = String.format("An error occurred when setting CAN Controller on channel %d to factory default configuration. Error Code: %s",
                                             channel,
                                             errorCode);
            logger.error(msg);
            throw new IllegalStateException(msg);
        }
        controller.setNeutralMode(NeutralMode.Brake); // Enable Brake mode. Previous API was canTalon.enableBrakeMode(true);


        // controller.enableControl(); obsolete call? Not finding an equivalent in the new API
        return controller;
    }


    @Nonnull
    protected abstract T createCanController(int deviceNumber);


    @API
    public T getLeader()
    {
        return leader;
    }


    @API
    public T getFollower()
    {
        return follower;
    }


    @Override
    public double get()
    {
        // TODO Need to determine range of this result. May need to change this code
        return leader.getMotorOutputPercent();
    }


    @Override
    public void set(double speed)
    {
        leader.set(controlMode, speed);
    }

    @API
    @Override
    public void setInverted(boolean invertLeader, @Nonnull InvertType followerInvertType)
    {
        // For example, see:
        //    https://github.com/CrossTheRoadElec/Phoenix-Examples-Languages/blob/master/Java/SixTalonArcadeDrive/src/main/java/frc/robot/Robot.java#L88

        // Note: true == InvertType.InvertMotorOutput
        //       false == InvertType.None
        leader.setInverted(invertLeader);
        follower.setInverted(followerInvertType);
    }


    /**
     * <p>
     * <strong style="color:red">
     * For the Motor Pairs classes, this {@code setInverted} method should typically
     * not be used. Instead use the {@link #setInverted(boolean, InvertType)}
     * </strong>
     * </p>
     * <p>
     * {@inheritDoc}
     * </p>
     */
    @Override
    public void setInverted(boolean isInverted)
    {
        setInvertedLaconicLogger.warn("The setInverted(boolean) method was called on a motor pair. In general, it is preferable to use the setInverted(boolean, InvertType) for a motor pair.");
        setInverted(isInverted, InvertType.FollowMaster);
    }


    @Override
    public boolean getInverted() {return leader.getInverted();}


    @Override
    public void disable()
    {
        stopMotor();
        leader.set(ControlMode.Disabled, 0.0);
    }


    @Override
    public void stopMotor()
    {
        leader.set(controlMode, 0.0);
    }
    
}
