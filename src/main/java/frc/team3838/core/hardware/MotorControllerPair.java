package frc.team3838.core.hardware;

import javax.annotation.Nonnull;

import com.ctre.phoenix.motorcontrol.InvertType;

import edu.wpi.first.wpilibj.motorcontrol.MotorController;



public interface MotorControllerPair extends MotorController
{
    void setInverted(boolean invertLeader, @Nonnull InvertType followerInvertType);
}
