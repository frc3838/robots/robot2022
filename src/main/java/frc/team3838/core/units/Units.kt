package frc.team3838.core.units

import frc.team3838.core.meta.API
import frc.team3838.core.utils.format
import kotlin.math.absoluteValue


@JvmInline
@API
value class Inches(val value: Double)
{
    @API fun toFeet(): Feet = Feet(value / 12)
    @API fun absolute(): Inches = Inches(value.absoluteValue)
    @API fun absoluteValue(): Double = value.absoluteValue
    @API fun toString(decimalPlaces: Int): String = "${value.format(decimalPlaces)} inches"
    override fun toString(): String = toString(2)
}

fun Inches.and(feet: Feet): Inches = this.plus(feet)
operator fun Inches.unaryMinus(): Inches = Inches(-this.value)
operator fun Inches.inc(): Inches = Inches(this.value + 1)
operator fun Inches.dec(): Inches = Inches(this.value - 1)
operator fun Inches.plus(other: Inches): Inches = Inches(this.value + other.value)
operator fun Inches.plus(other: Double): Inches = Inches(this.value + other)
operator fun Inches.plus(other: Int): Inches = Inches(this.value + other)
operator fun Double.plus(other: Inches): Inches = Inches(this + other.value)
operator fun Int.plus(other: Inches): Inches = Inches(this + other.value)
operator fun Inches.plus(feet: Feet): Inches = Inches(this.value + feet.toInches().value)
operator fun Inches.minus(other: Inches): Inches = Inches(this.value - other.value)
operator fun Inches.minus(other: Double): Inches = Inches(this.value - other)
operator fun Inches.minus(other: Int): Inches = Inches(this.value - other)
operator fun Double.minus(other: Inches): Inches = Inches(this - other.value)
operator fun Int.minus(other: Inches): Inches = Inches(this - other.value)
operator fun Inches.minus(feet: Feet): Inches = Inches(this.value - feet.toInches().value)
operator fun Inches.times(other: Inches): Inches = Inches(this.value * other.value)
operator fun Inches.times(other: Double): Inches = Inches(this.value * other)
operator fun Inches.times(other: Int): Inches = Inches(this.value * other)
operator fun Double.times(other: Inches): Inches = Inches(this * other.value)
operator fun Int.times(other: Inches): Inches = Inches(this * other.value)
operator fun Inches.times(feet: Feet): Inches = Inches(this.value * feet.toInches().value)
operator fun Inches.div(denominator: Inches): Inches = Inches(this.value / denominator.value)
operator fun Inches.div(denominator: Double): Inches = Inches(this.value / denominator)
operator fun Inches.div(denominator: Int): Inches = Inches(this.value / denominator)
operator fun Double.div(denominator: Inches): Inches = Inches(this / denominator.value)
operator fun Int.div(denominator: Inches): Inches = Inches(this / denominator.value)
operator fun Inches.div(denominator: Feet): Inches = Inches(this.value / denominator.toInches().value)
operator fun Inches.compareTo(other: Inches) = this.value.compareTo(other.value)
operator fun Inches.compareTo(other: Double) = this.value.compareTo(other)
operator fun Inches.compareTo(other: Int) = this.value.compareTo(other)
operator fun Double.compareTo(other: Inches) = this.compareTo(other.value)
operator fun Int.compareTo(other: Inches) = this.compareTo(other.value)
operator fun Inches.compareTo(feet: Feet) = this.compareTo(feet.toInches().value)

@JvmInline
@API
value class Feet(val value: Double)
{
    @API fun toInches(): Inches = Inches(value * 12)
    @API fun absolute(): Feet = Feet(value.absoluteValue)
    @API fun absoluteValue(): Double = value.absoluteValue
    @API fun toString(decimalPlaces: Int): String = "${value.format(decimalPlaces)} feet"
    override fun toString(): String = toString(2)
}

fun Feet.and(inches: Inches): Feet = this.plus(inches)
operator fun Feet.unaryMinus(): Feet = Feet(-this.value)
operator fun Feet.inc(): Feet = Feet(this.value + 1)
operator fun Feet.dec(): Feet = Feet(this.value - 1)
operator fun Feet.plus(other: Feet): Feet = Feet(this.value + other.value)
operator fun Feet.plus(other: Double): Feet = Feet(this.value + other)
operator fun Feet.plus(other: Int): Feet = Feet(this.value + other)
operator fun Double.plus(other: Feet): Feet = Feet(this + other.value)
operator fun Int.plus(other: Feet): Feet = Feet(this + other.value)
operator fun Feet.plus(inches: Inches) = Feet(this.value + inches.toFeet().value)
operator fun Feet.minus(other: Feet): Feet = Feet(this.value - other.value)
operator fun Feet.minus(other: Double): Feet = Feet(this.value - other)
operator fun Feet.minus(other: Int): Feet = Feet(this.value - other)
operator fun Double.minus(other: Feet): Feet = Feet(this - other.value)
operator fun Int.minus(other: Feet): Feet = Feet(this - other.value)
operator fun Feet.minus(inches: Inches): Feet = Feet(this.value - inches.toFeet().value)
operator fun Feet.times(other: Feet): Feet = Feet(this.value * other.value)
operator fun Feet.times(other: Double): Feet = Feet(this.value * other)
operator fun Feet.times(other: Int): Feet = Feet(this.value * other)
operator fun Double.times(other: Feet): Feet = Feet(this * other.value)
operator fun Int.times(other: Feet): Feet = Feet(this * other.value)
operator fun Feet.times(inches: Inches): Feet = Feet(this.value * inches.toFeet().value)
operator fun Feet.div(denominator: Feet): Feet = Feet(this.value / denominator.value)
operator fun Feet.div(denominator: Double): Feet = Feet(this.value / denominator)
operator fun Feet.div(denominator: Int): Feet = Feet(this.value / denominator)
operator fun Double.div(denominator: Feet): Feet = Feet(this / denominator.value)
operator fun Int.div(denominator: Feet): Feet = Feet(this / denominator.value)
operator fun Feet.div(denominator: Inches): Feet = Feet(this.value / denominator.toFeet().value)
operator fun Feet.compareTo(other: Feet) = this.value.compareTo(other.value)
operator fun Feet.compareTo(other: Double) = this.value.compareTo(other)
operator fun Feet.compareTo(other: Int) = this.value.compareTo(other)
operator fun Double.compareTo(other: Feet) = this.compareTo(other.value)
operator fun Int.compareTo(other: Feet) = this.compareTo(other.value)
operator fun Feet.compareTo(inches: Inches) = this.compareTo(inches.toFeet())

@JvmInline
value class Angle(val value: Double)
{
    override fun toString(): String = toString(2)
    fun toString(decimalPlaces: Int): String = "${value.format(decimalPlaces)}\u00B0"
}

operator fun Angle.unaryMinus(): Angle = Angle(-this.value)
operator fun Angle.inc(): Angle = Angle(this.value + 1)
operator fun Angle.dec(): Angle = Angle(this.value - 1)
operator fun Angle.plus(other: Angle): Angle = Angle(this.value + other.value)
operator fun Angle.plus(other: Double): Angle = Angle(this.value + other)
operator fun Angle.plus(other: Int): Angle = Angle(this.value + other)
operator fun Double.plus(other: Angle): Angle = Angle(this + other.value)
operator fun Int.plus(other: Angle): Angle = Angle(this + other.value)
operator fun Angle.minus(other: Angle): Angle = Angle(this.value - other.value)
operator fun Angle.minus(other: Double): Angle = Angle(this.value - other)
operator fun Angle.minus(other: Int): Angle = Angle(this.value - other)
operator fun Double.minus(other: Angle): Angle = Angle(this - other.value)
operator fun Int.minus(other: Angle): Angle = Angle(this - other.value)
operator fun Angle.times(other: Angle): Angle = Angle(this.value * other.value)
operator fun Angle.times(other: Double): Angle = Angle(this.value * other)
operator fun Angle.times(other: Int): Angle = Angle(this.value * other)
operator fun Double.times(other: Angle): Angle = Angle(this * other.value)
operator fun Int.times(other: Angle): Angle = Angle(this * other.value)
operator fun Angle.div(denominator: Angle): Angle = Angle(this.value / denominator.value)
operator fun Angle.div(denominator: Double): Angle = Angle(this.value / denominator)
operator fun Angle.div(denominator: Int): Angle = Angle(this.value / denominator)
operator fun Double.div(denominator: Angle): Angle = Angle(this / denominator.value)
operator fun Int.div(denominator: Angle): Angle = Angle(this / denominator.value)
operator fun Angle.compareTo(other: Angle) = this.value.compareTo(other.value)
operator fun Angle.compareTo(other: Double) = this.value.compareTo(other)
operator fun Angle.compareTo(other: Int) = this.value.compareTo(other)
operator fun Double.compareTo(other: Angle) = this.compareTo(other.value)
operator fun Int.compareTo(other: Angle) = this.compareTo(other.value)


@JvmInline
value class Speed(val value: Double)
{
    override fun toString(): String = toString(2)
    fun toString(decimalPlaces: Int): String = value.format(decimalPlaces)
}

operator fun Speed.unaryMinus(): Speed = Speed(-this.value)
operator fun Speed.inc(): Speed = Speed(this.value + 1)
operator fun Speed.dec(): Speed = Speed(this.value - 1)
operator fun Speed.plus(other: Speed): Speed = Speed(this.value + other.value)
operator fun Speed.plus(other: Double): Speed = Speed(this.value + other)
operator fun Speed.plus(other: Int): Speed = Speed(this.value + other)
operator fun Double.plus(other: Speed): Speed = Speed(this + other.value)
operator fun Int.plus(other: Speed): Speed = Speed(this + other.value)
operator fun Speed.minus(other: Speed): Speed = Speed(this.value - other.value)
operator fun Speed.minus(other: Double): Speed = Speed(this.value - other)
operator fun Speed.minus(other: Int): Speed = Speed(this.value - other)
operator fun Double.minus(other: Speed): Speed = Speed(this - other.value)
operator fun Int.minus(other: Speed): Speed = Speed(this - other.value)
operator fun Speed.times(other: Speed): Speed = Speed(this.value * other.value)
operator fun Speed.times(other: Double): Speed = Speed(this.value * other)
operator fun Speed.times(other: Int): Speed = Speed(this.value * other)
operator fun Double.times(other: Speed): Speed = Speed(this * other.value)
operator fun Int.times(other: Speed): Speed = Speed(this * other.value)
operator fun Speed.div(denominator: Speed): Speed = Speed(this.value / denominator.value)
operator fun Speed.div(denominator: Double): Speed = Speed(this.value / denominator)
operator fun Speed.div(denominator: Int): Speed = Speed(this.value / denominator)
operator fun Double.div(denominator: Speed): Speed = Speed(this / denominator.value)
operator fun Int.div(denominator: Speed): Speed = Speed(this / denominator.value)
operator fun Speed.compareTo(other: Speed) = this.value.compareTo(other.value)
operator fun Speed.compareTo(other: Double) = this.value.compareTo(other)
operator fun Speed.compareTo(other: Int) = this.value.compareTo(other)
operator fun Double.compareTo(other: Speed) = this.compareTo(other.value)
operator fun Int.compareTo(other: Speed) = this.compareTo(other.value)

