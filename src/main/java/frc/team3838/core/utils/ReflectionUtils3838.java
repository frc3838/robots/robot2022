package frc.team3838.core.utils;

import java.lang.reflect.Modifier;
import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public final class ReflectionUtils3838
{
    private static final Logger logger = LoggerFactory.getLogger(ReflectionUtils3838.class);


    private ReflectionUtils3838() { }


    
    public static <T> SortedSet<Class<? extends T>> findImplementations(@Nonnull Class<T> classType)
    {
        return findImplementations(classType, (Class<?>) null);
    }


    public static <T> SortedSet<Class<? extends T>> findImplementations(@Nonnull Class<T> classType, @Nullable Class<?> packageClass)
    {
        final String pkgName = determineBasePackageName(classType, packageClass);
        return findImplementations(classType, pkgName);
    }

    public static <T> SortedSet<Class<? extends T>> findImplementations(@Nonnull Class<T> classType, @Nullable String pkgName)
    {
        if (pkgName == null) {pkgName = "frc"; }
        
        final SubTypesScanner subTypesScanner = new SubTypesScanner();
        final ConfigurationBuilder configurationBuilder = new ConfigurationBuilder()
            .setUrls(ClasspathHelper.forPackage(pkgName))
            .setScanners(subTypesScanner)
            .filterInputsBy(new FilterBuilder().includePackage(pkgName));
        final Reflections reflections = new Reflections(configurationBuilder);
        final TreeSet<Class<? extends T>> implementations = new TreeSet<>(Comparator.comparing(Class::getName));
        implementations.addAll(reflections.getSubTypesOf(classType));
        return implementations;
    }


    public static <T> SortedSet<Class<? extends T>> findImplementationsFiltered(@Nonnull Class<T> classType)
    {
        return findImplementationsFiltered(classType, (Class<?>) null);
    }

    public static <T> SortedSet<Class<? extends T>> findImplementationsFiltered(@Nonnull Class<T> classType, @Nullable Class<?> packageClass)
    {
        final String pkgName = determineBasePackageName(classType, packageClass);
        return findImplementationsFiltered(classType, pkgName);
    }
    
    public static <T> SortedSet<Class<? extends T>> findImplementationsFiltered(@Nonnull Class<T> classType, @Nullable String pkgName)
    {
        final SortedSet<Class<? extends T>> unfilteredInstances = findImplementations(classType, pkgName);
        final SortedSet<Class<? extends T>> filteredInstances = new TreeSet<>(Comparator.comparing(Class::getName));
        for (Class<? extends T> instance : unfilteredInstances)
        {
            if (!instance.getName().toLowerCase().contains("example") &&
                !instance.getName().toLowerCase().contains("obsolete") &&
                !Modifier.isAbstract(instance.getModifiers()))
            {
                filteredInstances.add(instance);
            }
        }
        return filteredInstances;
    }


    @Nonnull
    private static <T> String determineBasePackageName(@Nonnull Class<T> classType, @Nullable Class<?> packageClass)
    {
        if (packageClass == null) {packageClass = classType; }

        final String fullPkgName = packageClass.getPackage().getName();
        //        final String pkgName = "frc.team3838";
        return fullPkgName.substring(0, fullPkgName.indexOf('.'));
    }
}
