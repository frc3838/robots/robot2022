package frc.team3838.core.utils

import mu.KotlinLogging
import kotlin.reflect.KClass
import kotlin.reflect.jvm.jvmName

private val logger = KotlinLogging.logger {}

/** Checks if the class is a Kotlin `object` or not. */
fun KClass<out Any>?.isObject(): Boolean
{
    try
    {
        return if (this?.jvmName?.contains("$") == true)
        {
            // It's an anonymous class
            false
        }
        else
        {
            this?.objectInstance != null
        }
    }
    catch (e: Throwable)
    {
        try
        {
            logger.info { "An exception occurred when determining if a KCLass is a Kotlin object. KCLass.qualifiedName = '${this?.qualifiedName}'  KCLass.jvmName = '${this?.jvmName}'. Cause: $e" }
        }
        catch (e2: Exception)
        {
            logger.info { "An exception occurred when logging exception that occurred when determining if KCLass is an object. Original exception: $e  | logging exception: $e2" }
        }
        return false
    }
}

/** Checks if the class is a Kotlin `object` or not. */
fun Class<*>?.isKotlinObject(): Boolean
{
    return try
    {
        this?.kotlin?.isObject() ?: false
    }
    catch (e: Exception)
    {
        logger.info { "An exception occurred when determining if CLass is an Kotlin object. KCLass.qualifiedName = '${this?.name}'. Cause: $e" }
        false
    }
}

/**
 * Gets the instance of a Kotlin `object`. It must be
 * verified -- via the [isKotlinObject] function -- prior
 * to calling this method. For a safer option that
 * does not require the pre-check, use
 * [getKotlinObjectInstanceSafe].
 */
fun <T> Class<T>.getKotlinObjectInstance(): T
{
    // Not seeing a cleaner way to do this
    // Some suggestions on SO, but they do not work 
    //      https://stackoverflow.com/questions/55369415/convert-java-class-to-kotlins-kclass
    val clazz = this as Class<*>
    @Suppress("UNCHECKED_CAST")
    return clazz.kotlin.objectInstance as T
}

/**
 * Gets the instance of a Kotlin `object`, or `null` if
 * the class is not a Kotlin `object`.
 */
fun <T> Class<T>?.getKotlinObjectInstanceSafe(): T?
{
    return if (this == null)
    {
        null
    }
    else
    {
        val clazz = this as Class<*>
        val kClass = clazz.kotlin
        val instance = kClass.objectInstance
        if (instance == null)
        {
            null
        }
        else
        {
            try
            {
                @Suppress("UNCHECKED_CAST")
                instance as T
            }
            catch (e: Throwable)
            {
                null
            }
        }
    }
}