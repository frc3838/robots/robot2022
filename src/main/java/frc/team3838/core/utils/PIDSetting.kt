package frc.team3838.core.utils

import edu.wpi.first.networktables.EntryListenerFlags
import edu.wpi.first.networktables.EntryNotification
import edu.wpi.first.util.function.BooleanConsumer
import edu.wpi.first.wpilibj.shuffleboard.BuiltInLayouts
import edu.wpi.first.wpilibj.shuffleboard.BuiltInWidgets
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab
import edu.wpi.first.wpilibj.shuffleboard.SimpleWidget
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard
import frc.team3838.core.meta.API
import java.util.function.DoubleConsumer

@API
open class PIDSetting @JvmOverloads constructor(val name:String,
                                                var p: Double = 0.0,
                                                var i: Double = 0.0,
                                                var d: Double = 0.0,
                                                var f: Double = 0.0,
                                                val enableTuning: Boolean = false) //: Sendable
{
    companion object
    {
        const val DEFAULT_TAB_TITLE = "PID Tuning"
    }
    
    //val pidDash: PIDDash? = addToTab(name, true, )

    override fun toString(): String
    {
        return String.format("P=%6.2f  I=%6.2f,  D=%6.2f  (F=%6.2f)", p, i, d, f)
    }

//    override fun initSendable(builder: SendableBuilder)
//    {
//        builder.setSmartDashboardType("PID_Setting")
//        builder.addDoubleProperty("P", { p }) { p: Double -> this.p = p }
//        builder.addDoubleProperty("I", { i }) { i: Double -> this.i = i }
//        builder.addDoubleProperty("D", { d }) { d: Double -> this.d = d }
//    }


    // TODO look at converting to a builder
    @JvmOverloads
    fun addToTab(layoutTitle: String,
                 includeF: Boolean = false,
                 pidTab: ShuffleboardTab = Shuffleboard.getTab(DEFAULT_TAB_TITLE),
                 enabledConsumer: BooleanConsumer? = null,
                 pConsumer: DoubleConsumer? = null,
                 iConsumer: DoubleConsumer? = null,
                 dConsumer: DoubleConsumer? = null,
                 fConsumer: DoubleConsumer? = null
                ): PIDDash
    {

        val height = if (includeF) 5 else 4

        val pidLayout = pidTab.getLayout(layoutTitle, BuiltInLayouts.kList)
            .withSize(2, height)
            // TODO move to parameters
            .withPosition(0, 0)
            .withProperties(mapOf<String, Any>("Label Position" to "LEFT")) // options include "HIDDEN"

        val p = pidLayout.add("P", 0.00)
            .withWidget(BuiltInWidgets.kTextView)
            .withSize(2, 1)
            .withPosition(0, 0)

        val i = pidLayout.add("I", 0.00)
            .withWidget(BuiltInWidgets.kTextView)
            .withSize(2, 1)
            .withPosition(0, 1)

        val d = pidLayout.add("D", 0.00)
            .withWidget(BuiltInWidgets.kTextView)
            .withSize(2, 1)
            .withPosition(0, 2)

        val f: SimpleWidget? = if (includeF)
        {
            pidLayout.add("D", 0.00)
                .withWidget(BuiltInWidgets.kTextView)
                .withSize(2, 1)
                .withPosition(0, 3)
        }
        else
        {
            null
        }

        val enabledWidget = pidLayout.add("Enabled", false)
            .withWidget(BuiltInWidgets.kToggleSwitch)
            .withPosition(0, height - 1)

        addConsumer(p, pConsumer)
        addConsumer(i, iConsumer)
        addConsumer(d, dConsumer)
        addConsumer(f, fConsumer)
        addConsumer(enabledWidget, enabledConsumer)

        return PIDDash(pidTab,pidLayout, p, i, d, f, enabledWidget)
    }

    private fun addConsumer(widget: SimpleWidget?, consumer: DoubleConsumer?)
    {
        @Suppress("DuplicatedCode")
        if (consumer != null && widget != null)
        {
            val pEntry = widget.entry
            pEntry.addListener({ event: EntryNotification ->
                                   if (event.value.isDouble)
                                   {
                                       SmartDashboard.postListenerTask {
                                           consumer.accept(event.value.double)
                                       }
                                   }
                               }, EntryListenerFlags.kImmediate or EntryListenerFlags.kNew or EntryListenerFlags.kUpdate)
        }
    }

    private fun addConsumer(widget: SimpleWidget?, consumer: BooleanConsumer?)
    {
        @Suppress("DuplicatedCode")
        if (consumer != null && widget != null)
        {
            val pEntry = widget.entry
            pEntry.addListener({ event: EntryNotification ->
                                   if (event.value.isBoolean)
                                   {
                                       SmartDashboard.postListenerTask {
                                           consumer.accept(event.value.boolean)
                                       }
                                   }
                               }, EntryListenerFlags.kImmediate or EntryListenerFlags.kNew or EntryListenerFlags.kUpdate)
        }
    } 
    
    /*
    == Sample copied from FRC code to work from as an example ==
   **
   * Add a double property.
   *
   * @param key    property name
   * @param getter getter function (returns current value)
   * @param setter setter function (sets new value)
   *
    @Override
    public void addDoubleProperty(String key, DoubleSupplier getter, DoubleConsumer setter)
    {
        Property property = new Property(m_table, key);
        if (getter != null)
        {
            property.m_update = entry -> entry.setDouble(getter.getAsDouble());
        }
        if (setter != null)
        {
            property.m_createListener = entry -> entry.addListener(event -> {
                if (event.value.isDouble())
                {
                    SmartDashboard.postListenerTask(() -> setter.accept(event.value.getDouble()));
                }
            }, EntryListenerFlags.kImmediate | EntryListenerFlags.kNew | EntryListenerFlags.kUpdate);
        }
        m_properties.add(property);
    }
     */


}