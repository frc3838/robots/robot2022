package frc.team3838.core.utils.time

import edu.wpi.first.wpilibj.RobotController
import frc.team3838.core.meta.API


/**
 * Return the system clock time in milliseconds. Return the time from the FPGA hardware clock in
 * milliseconds since the FPGA started.
 *
 * @return Robot running time in milliseconds.
 */
@API
fun getFpgaTimeInMilliseconds(): Long = RobotController.getFPGATime() / 1_000

/**
 * Return the system clock time in seconds. Return the time from the FPGA hardware clock in
 * seconds since the FPGA started.
 *
 * @return Robot running time in seconds.
 */
@API
fun getFpgaTimeInSeconds(): Double = RobotController.getFPGATime() / 1_000_000.0