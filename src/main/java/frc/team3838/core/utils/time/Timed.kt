package frc.team3838.core.utils.time


interface Timed
{
    fun isTimedOut(): Boolean
}