package frc.team3838.core.utils.time

import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.meta.API
import java.util.concurrent.TimeUnit

@API
fun createStartedTimer(timeDuration: TimeDuration): Timer = Timer(timeDuration).start()

@API
val NO_TIMEOUT = TimeDuration.of(Long.MAX_VALUE, TimeUnit.MILLISECONDS)

@Suppress("MemberVisibilityCanBePrivate")
class Timer(val timeDuration: TimeDuration) : Timed
{
    private var timeoutTime: Long = NO_TIMEOUT.duration
    private var hasTimedOut = false

    /**
     * Starts the timer. The function returns itself for use in a chained assignment such as:
     * `val timer = Timer(timeDuration).start()`
     */
    fun start(): Timer
    {
        hasTimedOut = false
        timeoutTime = if (timeDuration == NO_TIMEOUT) 
        {
            NO_TIMEOUT.duration 
        } 
        else 
        {
            getFpgaTimeInMilliseconds() + timeDuration.toMillis()
        }
        return this
    }

    @Synchronized
    override fun isTimedOut(): Boolean
    {
        if (hasTimedOut) {return hasTimedOut }
        hasTimedOut = timeDuration != NO_TIMEOUT && (getFpgaTimeInMilliseconds() >= timeoutTime)
        return hasTimedOut
    }
    
    @API
    fun millisRemaining(): Long = if (timeDuration == NO_TIMEOUT) { Long.MAX_VALUE } else { timeoutTime - getFpgaTimeInMilliseconds() }
    
    @API
    fun timeRemaining(): String = if (timeDuration == NO_TIMEOUT)
    {
        "No Timeout Set"
    }
    else
    {
        val timeLeft = timeoutTime - getFpgaTimeInMilliseconds()
        if (timeLeft < 0 )
        {
            "Has Timed Out"
        }
        else
        {
            "${timeLeft}ms"
        }
    }
}
