package frc.team3838.core.utils

import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardLayout
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab
import edu.wpi.first.wpilibj.shuffleboard.SimpleWidget


class PIDDash internal constructor(val pidTab: ShuffleboardTab,
                                   val pidLayout: ShuffleboardLayout,
                                   val pWidget: SimpleWidget,
                                   val iWidget: SimpleWidget,
                                   val dWidget: SimpleWidget,
                                   val fWidget: SimpleWidget?,
                                   val enabledWidget: SimpleWidget)
