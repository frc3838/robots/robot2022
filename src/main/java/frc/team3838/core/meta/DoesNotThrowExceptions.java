package frc.team3838.core.meta;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;



/**
 * An annotation to indicate that a method has been defensively coded to endure that
 * it will not throw an Exception.
 */
@Target({METHOD})
@Retention(RetentionPolicy.SOURCE)
public @interface DoesNotThrowExceptions
{

}
