package frc.team3838.core.meta;

public interface Enableable 
{
    boolean isEnabled();

    void disable();
}
