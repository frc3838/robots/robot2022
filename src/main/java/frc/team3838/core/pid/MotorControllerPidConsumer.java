package frc.team3838.core.pid;

import java.util.function.DoubleConsumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ctre.phoenix.motorcontrol.IMotorController;



public abstract class MotorControllerPidConsumer implements DoubleConsumer
{
    public static final int DEFAULT_TIMEOUT_MS = 30;
    
    /** Talon SRX/ Victor SPX will supported multiple (cascaded) PID loops. For now we just want the primary one. */
    public static final int DEFAULT_SLOT_INDEX = 0;
    protected final Logger logger = LoggerFactory.getLogger(getClass());
    
    protected final int slotIndex;
    protected final int timeoutMs;
    protected final IMotorController motorController;
    
    
    /**
     * Creates a MotorControllerPidConsumer that will use the provided settings when
     * consuming the appropriate PID value on the supplied controller.
     *
     * @param motorController The Cross TheRoad Electronics Phoenix MotorController for PID Control
     */
    protected MotorControllerPidConsumer(IMotorController motorController)
    {
        this(motorController, DEFAULT_SLOT_INDEX, DEFAULT_TIMEOUT_MS);
    }
    
    
    /**
     * Creates a MotorControllerPidConsumer that will use the provided settings when
     * consuming the appropriate PID value on the supplied controller.
     *  @param motorController The Cross TheRoad Electronics Phoenix MotorController for PID Control
     * @param timeoutMs       Timeout value in ms. If nonzero, function will wait for
     *                        config success and report an error if it times out.
     */
    protected MotorControllerPidConsumer(IMotorController motorController, int timeoutMs)
    {
        this(motorController,DEFAULT_SLOT_INDEX, timeoutMs);
    }
    
    
    /**
     * Creates a MotorControllerPidConsumer that will use the provided settings when
     * consuming the appropriate PID value on the supplied controller.
     * 
     * @param motorController The Cross TheRoad Electronics Phoenix MotorController for PID Control
     * @param slotIndex   Parameter slot for the constant.  Talon SRX/ Victor SPX will supported 
     *                    multiple (cascaded) PID loops. 0 is the primary one.
     * @param timeoutMs Timeout value in ms. If nonzero, function will wait for
     *                  config success and report an error if it times out.
     *                  If zero, no blocking or checking is performed.
     */
    protected MotorControllerPidConsumer(IMotorController motorController, int slotIndex, int timeoutMs)
    {
        this.slotIndex = slotIndex;
        this.timeoutMs = timeoutMs;
        this.motorController = motorController;
    }
    
    
    @Override
    public abstract void accept(double value);
}
