package frc.team3838.core.pid;


import com.ctre.phoenix.motorcontrol.IMotorController;

import frc.team3838.core.meta.API;



@API
public class FConsumer extends MotorControllerPidConsumer
{
    @API
    public FConsumer(IMotorController motorController) 
    {
        super(motorController);
    }
    
    
    @API
    public FConsumer(IMotorController motorController, int timeoutMs)
    {
        super(motorController, timeoutMs);
    }
    
    
    @API
    public FConsumer(IMotorController motorController, int slotIdx, int timeoutMs)
    {
        super(motorController, slotIdx, timeoutMs);
    }
    
    
    @Override
    public void accept(double value)
    {
        motorController.config_kF(slotIndex, value, timeoutMs);
    }
}
