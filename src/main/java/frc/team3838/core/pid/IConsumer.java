package frc.team3838.core.pid;


import com.ctre.phoenix.motorcontrol.IMotorController;

import frc.team3838.core.meta.API;



@API
public class IConsumer extends MotorControllerPidConsumer
{
    @API
    public IConsumer(IMotorController motorController) 
    {
        super(motorController);
    }
    
    
    @API
    public IConsumer(IMotorController motorController, int timeoutMs)
    {
        super(motorController, timeoutMs);
    }
    
    
    @API
    public IConsumer(IMotorController motorController, int slotIdx, int timeoutMs)
    {
        super(motorController, slotIdx, timeoutMs);
    }
    
    
    @Override
    public void accept(double value)
    {
        motorController.config_kI(slotIndex, value, timeoutMs);
    }
}
