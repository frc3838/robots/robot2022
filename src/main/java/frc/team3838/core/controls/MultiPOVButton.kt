package frc.team3838.core.controls

import com.google.common.collect.ImmutableList
import edu.wpi.first.wpilibj.GenericHID
import edu.wpi.first.wpilibj.buttons.Button
import frc.team3838.core.meta.API

/**
 * A class that creates a Button from multiple POV/DPad/Hat-Switch
 * directions. This can be useful to provide less sensitive control.
 * For example, say we just want to use left and right on the POV switch.
 * In the excitement of driving, the driver, when attempting to press the 
 * left (i.e. West, or 270 degrees) direction, may actually press the 
 * "Up and Left" (i.e. North West or 315 degrees) direction. 
 * 
 * To solve this, you can create a button such that "Left" is represented
 * by three buttons, SW_DOWN_AND_LEFT_225, W_LEFT_270, and NW_UP_AND_LEFT_315
 * directions, and "Right" by the NE_UP_AND_RIGHT_45, E_RIGHT_90. and 
 * SE_DOWN_AND_RIGHT_135 directions:
 * 
 * ```Java
 * // Java
 * Button leftAction = new MultiPOVButton(coDriverGamePad, 
 *                                        POVDirection.W_LEFT_270,
 *                                        POVDirection.NW_UP_AND_LEFT_315, 
 *                                        POVDirection.SW_DOWN_AND_LEFT_225);
 * Button rightAction = new MultiPOVButton(coDriverGamePad, 
 *                                         POVDirection.E_RIGHT_90,
 *                                         POVDirection.NE_UP_AND_RIGHT_45, 
 *                                         POVDirection.SE_DOWN_AND_RIGHT_135);
 * ```
 * ```Kotlin
 * // Kotlin
 * val leftAction: Button = MultiPOVButton(coDriverGamePad, 
 *                                         POVDirection.W_LEFT_270,
 *                                         POVDirection.NW_UP_AND_LEFT_315, 
 *                                         POVDirection.SW_DOWN_AND_LEFT_225)
 * val rightAction: Button = MultiPOVButton(coDriverGamePad, 
 *                                          POVDirection.E_RIGHT_90,
 *                                          POVDirection.NE_UP_AND_RIGHT_45, 
 *                                          POVDirection.SE_DOWN_AND_RIGHT_135)
 * ```
 * 
 * NOTE: In the strictest terms, it does not matter which direction is passed in as 
 * the "primary" direction. This is more for documentation purposes. But also, the 
 * "primary" direction is evaluated first, and so there can be a nanosecond or so
 * of improved performance given the primary button is the most likely one to be pushed.
 *
 */
@API
class MultiPOVButton : Button
{
    private val buttons: List<Button>
    
    @Suppress("unused")
    constructor(hid: GenericHID, primaryPovDirection: POVDirection, vararg otherPovDirections: POVDirection) : this(hid, 0, primaryPovDirection, *otherPovDirections)

    /**
     * @param povControlNumber on joysticks/gamepads with multiple POV/DPad/Hat-switch switches,
     *                         a number must be provided for the secondary switch(es)
     */
    constructor(hid: GenericHID, povControlNumber: Int, primaryPovDirection: POVDirection, vararg otherPovDirections: POVDirection)
    {
        val builder = ImmutableList.builder<Button>()
        builder.add(primaryPovDirection.asButton(hid, povControlNumber))
        otherPovDirections.forEach {
            builder.add(it.asButton(hid, povControlNumber))
        }
        
        buttons = builder.build()
    }

    override fun get(): Boolean = buttons.stream().anyMatch { it.get() }
}
