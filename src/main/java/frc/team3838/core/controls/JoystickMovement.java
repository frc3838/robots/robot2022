package frc.team3838.core.controls;

public enum JoystickMovement
{
    PUSH
        {
            @Override
            public JoystickMovement getOpposite()
            {
                return PULL;
            }
        },
    PULL
    {
        @Override
        public JoystickMovement getOpposite()
        {
            return PUSH;
        }
    },
    LEFT
    {
        @Override
        public JoystickMovement getOpposite()
        {
            return RIGHT;
        }
    },
    RIGHT
    {
        @Override
        public JoystickMovement getOpposite()
        {
            return LEFT;
        }
    };


    public abstract JoystickMovement getOpposite();

}
