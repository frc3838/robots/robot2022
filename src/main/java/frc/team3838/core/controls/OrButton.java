package frc.team3838.core.controls;

import java.util.Collection;
import javax.annotation.Nonnull;

import edu.wpi.first.wpilibj.buttons.Trigger;
import frc.team3838.core.meta.API;



@API
public class OrButton extends AbstractLogicButton
{
    @API
    public OrButton(@Nonnull Trigger triggerOrButton) { super(triggerOrButton); }


    @API
    public OrButton(@Nonnull Trigger triggerOrButton, Trigger... otherTriggersOrButtons) { super(triggerOrButton, otherTriggersOrButtons); }


    @API
    public OrButton(@Nonnull Collection<Trigger> triggersOrButtons) { super(triggersOrButtons); }


    @Override
    public boolean get()
    {
        return triggers.stream().anyMatch(Trigger::get);
    }
}
