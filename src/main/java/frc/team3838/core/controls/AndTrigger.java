package frc.team3838.core.controls;

import java.util.Collection;
import javax.annotation.Nonnull;

import edu.wpi.first.wpilibj.buttons.Trigger;
import frc.team3838.core.meta.API;



@API
public class AndTrigger extends AbstractLogicTrigger
{
    @API
    public AndTrigger(@Nonnull Trigger trigger) { super(trigger); }


    @API
    public AndTrigger(@Nonnull Trigger trigger, Trigger... otherTriggers) { super(trigger, otherTriggers); }


    @API
    public AndTrigger(@Nonnull Collection<Trigger> triggers) { super(triggers); }


    @Override
    public boolean get()
    {
        return !triggers.isEmpty() && triggers.stream().allMatch(Trigger::get);
    }
}
