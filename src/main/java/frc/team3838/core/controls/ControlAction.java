package frc.team3838.core.controls;

import edu.wpi.first.wpilibj.buttons.Trigger;
import edu.wpi.first.wpilibj.command.Command;



public interface ControlAction<T extends Trigger>
{
    void assign(T trigger, Command command);
}
