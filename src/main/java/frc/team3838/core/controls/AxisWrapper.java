package frc.team3838.core.controls;

import edu.wpi.first.wpilibj.GenericHID;
import frc.team3838.core.meta.API;

@API
public class AxisWrapper implements SimpleAxis
{
    private final GenericHID hid;
    private final int axisNumber;
    private final boolean invert;
    
    
    public AxisWrapper(GenericHID hid, int axisNumber, boolean invert)
    {
        this.hid = hid;
        this.axisNumber = axisNumber;
        this.invert = invert;
    }
    
    @Override
    public double get()
    {
        final double rawAxis = hid.getRawAxis(axisNumber);
        return invert ? -rawAxis : rawAxis;
    }
}
