package frc.team3838.core.controls;

import java.util.Collection;
import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

import edu.wpi.first.wpilibj.buttons.Button;
import edu.wpi.first.wpilibj.buttons.Trigger;



public abstract class AbstractLogicButton extends Button
{
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    protected final ImmutableList<Trigger> triggers;


    protected AbstractLogicButton(@Nonnull Trigger triggerOrButton)
    {
        this.triggers = ImmutableList.of(triggerOrButton);
    }


    protected AbstractLogicButton(@Nonnull Trigger triggerOrButton, Trigger... otherTriggersOrButtons)
    {
        final Builder<Trigger> listBuilder = ImmutableList.builder();

        listBuilder.add(triggerOrButton);
        listBuilder.add(otherTriggersOrButtons);

        this.triggers = listBuilder.build();
    }


    protected AbstractLogicButton(@Nonnull Collection<Trigger> triggersOrButtons)
    {
        if (triggersOrButtons.isEmpty())
        {
            logger.warn("Triggers collection is empty.");
        }
        this.triggers = ImmutableList.copyOf(triggersOrButtons);
    }

}
