package frc.team3838.core.controls;

import java.util.Collection;
import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

import edu.wpi.first.wpilibj.buttons.Trigger;



public abstract class AbstractLogicTrigger extends Trigger
{
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    protected final ImmutableList<Trigger> triggers;


    protected AbstractLogicTrigger(@Nonnull Trigger trigger)
    {
        this.triggers = ImmutableList.of(trigger);
    }


    protected AbstractLogicTrigger(@Nonnull Trigger trigger, Trigger... otherTriggers)
    {
        final Builder<Trigger> listBuilder = ImmutableList.builder();

        listBuilder.add(trigger);
        listBuilder.add(otherTriggers);

        this.triggers = listBuilder.build();
    }


    protected AbstractLogicTrigger(@Nonnull Collection<Trigger> triggers)
    {
        if (triggers.isEmpty())
        {
            logger.warn("Triggers collection is empty.");
        }
        this.triggers = ImmutableList.copyOf(triggers);
    }

}
