package frc.team3838.core.controls;

import edu.wpi.first.wpilibj.GenericHID;
import frc.team3838.core.meta.API;



@API
public class GamepadLeftThumbArcadeAxisPair extends AbstractGamePadAxisPair
{

    public GamepadLeftThumbArcadeAxisPair(GenericHID hid)
    {
        super(hid);
    }


    @Override
    public double getXorLeft()
    {
        return hid.getRawAxis(1);
    }


    @Override
    public double getYorRight()
    {
        return hid.getRawAxis(2);
    }
}
