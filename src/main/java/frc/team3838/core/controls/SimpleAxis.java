package frc.team3838.core.controls;

public interface SimpleAxis 
{
    double get();
}
