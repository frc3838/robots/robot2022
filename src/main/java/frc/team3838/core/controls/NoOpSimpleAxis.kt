package frc.team3838.core.controls


object NoOpSimpleAxis: SimpleAxis
{
    override fun get(): Double = 0.0

}