package frc.team3838.core.controls


// We retain for search & discovery purposes.
@Deprecated(message = "Use the POVDirection.asButton() function to create a WPI Lib POVButton. Or see MultiPOVButton")
object DPadButton
