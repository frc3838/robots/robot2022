package frc.team3838.core.controls

import edu.wpi.first.wpilibj.GenericHID
import edu.wpi.first.wpilibj.XboxController
import edu.wpi.first.wpilibj.buttons.Button
import frc.team3838.core.meta.API
import frc.team3838.core.utils.MathUtils

@API
class AxisAsButton @JvmOverloads constructor(private val genericHID: GenericHID, private val axisNumber: Int, private val triggerValue: Double = 0.2) : Button()
{

    val isNegative = MathUtils.isNegative(triggerValue)

    @API
    @JvmOverloads
    constructor(genericHID: GenericHID, axis: XboxController.Axis, triggerValue: Double = 0.2) : this(genericHID, axis.value, triggerValue)

    override fun get(): Boolean
    {
        val value = genericHID.getRawAxis(axisNumber)
        return if (isNegative)
        {
            value in -1.0 .. triggerValue
        }
        else
        {
            value in triggerValue .. 1.0
        }
    }
}