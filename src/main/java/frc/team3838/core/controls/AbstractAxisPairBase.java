package frc.team3838.core.controls;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.util.sendable.Sendable;
import edu.wpi.first.util.sendable.SendableBuilder;
import edu.wpi.first.util.sendable.SendableRegistry;



public abstract class AbstractAxisPairBase implements AxisPair,
                                                      Sendable
{
    private static final Logger logger = LoggerFactory.getLogger(AbstractAxisPairBase.class);

    /**
     * Creates an instance of the sensor base.
     */
    protected AbstractAxisPairBase()
    {
        super();
        SendableRegistry.setName(this, getClass().getSimpleName());
    }


    /**
     * Initializes this {@link Sendable} object.
     *
     * @param builder sendable builder
     */
    @Override
    public void initSendable(SendableBuilder builder)
    {


    }
}
