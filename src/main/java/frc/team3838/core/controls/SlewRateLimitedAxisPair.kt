package frc.team3838.core.controls

import edu.wpi.first.math.filter.SlewRateLimiter
import frc.team3838.core.meta.API

/**
 * An AxisPair where the AxisPair input is Rate Limited via a [SlewRateLimiter].
 * The provided rate limits define the rate-of-change limit, in units per second.
 * For example, a value of 3 results in making the increase from 0 to 1.0 take a 1/3 of a second.
 * (i.e. it'll take a 1/3 second to go from 0 to 1.0). This can prevent rapid
 * acceleration, and possibly tipping.
 *
 * @param axisPair the delegate AxisPair to wrap
 * @param xOrLeftOrSpeedLimiter the Slew rate limiter for the x/left/speed Axis
 * @param yOrRightOrRotateLimiter the Slew rate limiter for the y/right/rotate Axis
 */
class SlewRateLimitedAxisPair(private val axisPair: AxisPair,
                              private val xOrLeftOrSpeedLimiter: SlewRateLimiter,
                              private val yOrRightOrRotateLimiter: SlewRateLimiter) : AxisPair
{

    /**
     * Creates an AxisPair where the AxisPair input is Rate Limited via a [SlewRateLimiter].
     * The provided rate limits define the rate-of-change limit, in units per second.
     * For example, a value of 3 results in making the increase from 0 to 1.0 take a 1/3 of a second.
     * (i.e. it'll take a 1/3 second to go from 0 to 1.0). This can prevent rapid
     * acceleration, and possibly tipping.
     *
     *
     * @param xOrLeftOrSpeedRateLimit the Slew rate limit for the x/left/speed Axis
     * @param yOrRightOrRotateRateLimit the Slew rate limit for the y/right/rotate Axis
     */
    @API
    constructor(axisPair: AxisPair,
        xOrLeftOrSpeedRateLimit: Double,
        yOrRightOrRotateRateLimit: Double,
               ) : this(axisPair, SlewRateLimiter(xOrLeftOrSpeedRateLimit), SlewRateLimiter(yOrRightOrRotateRateLimit))



    override fun getXorLeft(): Double = xOrLeftOrSpeedLimiter.calculate(axisPair.xorLeft)

    override fun getYorRight(): Double = yOrRightOrRotateLimiter.calculate(axisPair.yorRight)
}