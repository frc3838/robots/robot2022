package frc.team3838.core.controls;

import edu.wpi.first.wpilibj.DigitalInput;



public class InvertibleDigitalInput extends DigitalInput
{

    private final boolean invert;


    public InvertibleDigitalInput(int channel, boolean invert)
    {
        super(channel);
        this.invert = invert;
    }


    @Override
    public boolean get()
    {
        return invert != super.get();
    }
}
