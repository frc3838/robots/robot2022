package frc.team3838.core.controls

import edu.wpi.first.wpilibj.GenericHID
import edu.wpi.first.wpilibj.Joystick
import edu.wpi.first.wpilibj.PS4Controller
import edu.wpi.first.wpilibj.XboxController


fun GenericHID.getXAxis(): Double
{
    return if (this is Joystick)
    {
        this.x
    }
    else
    {
        getXAxis(GamepadSide.Right)
    }
}

fun GenericHID.getXAxis(gamepadSide: GamepadSide): Double
{
    return when (this)
    {
        is XboxController -> 
        {
            when (gamepadSide) 
            {
                GamepadSide.Left -> this.leftX
                GamepadSide.Right -> this.rightX
            }
        }
        is PS4Controller ->
        {
            when (gamepadSide)
            {
                GamepadSide.Left  -> this.leftX
                GamepadSide.Right -> this.rightX
            }
        }
        is Joystick -> this.getXAxis()
        else -> getRawAxis(Joystick.AxisType.kX.value)
    }
}

fun GenericHID.getYAxis(): Double
{
    return if (this is Joystick)
    {
        this.y
    }
    else
    {
        getYAxis(GamepadSide.Right)
    }
}

fun GenericHID.getYAxis(gamepadSide: GamepadSide): Double
{
    return when (this)
    {
        is XboxController ->
        {
            when (gamepadSide)
            {
                GamepadSide.Left  -> this.leftY
                GamepadSide.Right -> this.rightY
            }
        }
        is PS4Controller  ->
        {
            when (gamepadSide)
            {
                GamepadSide.Left  -> this.leftY
                GamepadSide.Right -> this.rightY
            }
        }
        is Joystick       -> this.getYAxis()
        else              -> getRawAxis(Joystick.AxisType.kY.value)
    }
}