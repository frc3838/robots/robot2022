package frc.team3838.core.controls

import frc.team3838.core.utils.MathUtils

/**
 * An AxisPair where the AxisPair input is Rate Limited.
 *
 * @param delegateAxisPair the delegate AxisPair to wrap
 * @param xOrLeftOrRotateAbsoluteMax the absolute value of the max speed for the x-axis
 * @param yOrRightOrSpeedAbsoluteMax the absolute value of the max speed for the y-axis
 */
class LimitingAxisPair(private val delegateAxisPair: AxisPair,
                       private  val xOrLeftOrRotateAbsoluteMax: Double,
                       private  val yOrRightOrSpeedAbsoluteMax: Double,
                      ) : AxisPair
{


    override fun getXorLeft(): Double = MathUtils.scaleRange(
        /* inputValue =      */ delegateAxisPair.xorLeft,
        /* inputRangeMin =   */ -1.0,
        /* inputRangeMax =   */ 1.0,
        /* outputRangeMin =  */ -xOrLeftOrRotateAbsoluteMax,
        /* outputRangeMax =  */ xOrLeftOrRotateAbsoluteMax
                                                                     )

    override fun getYorRight(): Double = MathUtils.scaleRange(
        /* inputValue =      */ delegateAxisPair.yorRight,
        /* inputRangeMin =   */ -1.0,
        /* inputRangeMax =   */ 1.0,
        /* outputRangeMin =  */ -yOrRightOrSpeedAbsoluteMax,
        /* outputRangeMax =  */ yOrRightOrSpeedAbsoluteMax
                                                             )
}