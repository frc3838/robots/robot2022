package frc.team3838.core.controls

import edu.wpi.first.wpilibj.buttons.Button
import frc.team3838.core.meta.API

@API
object NoOpButton : Button()
{
    override fun get(): Boolean = false
}