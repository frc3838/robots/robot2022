package frc.team3838.core.controls

import edu.wpi.first.wpilibj.GenericHID
import edu.wpi.first.wpilibj.buttons.POVButton

/**
 * An enum to represent the 8 buttons on a POV/Dpad/Hat-Switch on a gamepad
 * or joystick. Each enum is named for a compass direction, a relative
 * direction, and its angle. This provides absolute clarity, and easy 
 * search and discovery.
 * 
 * To create a button, use the [asButton] function, or pass the [angle]
 * into the constructor for the WPI Lib [POVButton] class. For example:
 * 
 * ```Java
 * // Java
 * Button lowerLiftButton = POVDirection.W_LEFT_270.asButton(coDriverGamePad);
 * Button raiseLiftButton = new POVButton(coDriverGamePad, POVDirection.E_RIGHT_90.getAngle());
 * ```
 * ```Kotlin
 * // Kotlin
 * val lowerLiftButton = POVDirection.W_LEFT_270.asButton(coDriverGamePad)
 * val raiseLiftButton = POVButton(coDriverGamePad, POVDirection.W_LEFT_270.angle)
 * ```
 * 
 * Note, if the controller (i.e. GamePad or Joystick) has more than one 
 * POV/Dpad/Hat-Switch, you will need to pass in the number of the secondary 
 * ones as a final parameter.
 * 
 * @property angle The angle of the direction, as returned by the POV switch.
 */
@Suppress("unused")
enum class POVDirection(val angle: Int)
{
    N_UP_0(0), 
    NE_UP_AND_RIGHT_45(45), 
    E_RIGHT_90(90), 
    SE_DOWN_AND_RIGHT_135(135), 
    S_DOWN_180(180), 
    SW_DOWN_AND_LEFT_225(225), 
    W_LEFT_270(270), 
    NW_UP_AND_LEFT_315(315);
    
    
    /**
     * Creates a [POVButton] (from WPI Lib) for the direction.
     * This provides a more convenient way to get a `POVButton` rather 
     * than using the constructor, Below are examples creating a button
     * using this method, and using ghe `POVButton` constructor.
     * ```Java
     * // Java
     * Button lowerLiftButton = POVDirection.W_LEFT_270.asButton(coDriverGamePad);
     * Button raiseLiftButton = new POVButton(coDriverGamePad, POVDirection.E_RIGHT_90.getAngle());
     * ```
     * ```Kotlin
     * // Kotlin
     * val lowerLiftButton = POVDirection.W_LEFT_270.asButton(coDriverGamePad)
     * val raiseLiftButton = POVButton(coDriverGamePad, POVDirection.W_LEFT_270.angle)
     * ```
     * 
     * Note, if the controller (i.e. GamePad or Joystick) has more than one 
     * POV/Dpad/HiHat, you will need to pass in the number of the secondary 
     * ones as a final parameter.
     * 
     * Keywords for search discovery: PovButton DPadButton HiHatButton HatSwitchButton
     * 
     * @param hid the hid (i.e. Joystick or Gamepad) the POV (aka DPad) is on
     * @param povControlNumber  the POV (or DPad) to use on a hid/joystick/gamepad with multiple
     *                          POV controls (i.e. multiple DPads)
     */
    @JvmOverloads
    fun asButton(hid: GenericHID, povControlNumber: Int = 0): POVButton = POVButton(hid, angle, povControlNumber)
}

// We retain for discovery/search purposes, as many folks call the POV control a DPad
@Suppress("unused")
@Deprecated(message = "Use PovDirection", replaceWith = ReplaceWith("PovDirection"))
enum class DPadDirection(val angle: Int)
{
    N_UP(0), 
    NE_UP_AND_RIGHT(45), 
    E_RIGHT(90), 
    SE_DOWN_AND_RIGHT(135), 
    S_DOWN(180), 
    SW_DOWN_AND_LEFT(225), 
    W_LEFT(270), 
    NW_UP_AND_LEFT(315);
}
