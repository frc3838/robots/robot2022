package frc.team3838.core.controls;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.XboxController.Axis;



/**
 * Uses the two GamePad Throttles to create a -1 to 1 Axis. In the event
 * v=both buttons are presses, the positive value takes precedence. (As a future 
 * enhancement, it would be nice to make this configurable as either 
 * Positive precedence, Negative precedence, or No Value if both pressed)
 */
public class GamePadFullRangeAxisFromThrottles implements SimpleAxis
{
    private final GenericHID hid;
    private final boolean useLeftForPositive;
    
    public GamePadFullRangeAxisFromThrottles(GenericHID hid, GamepadSide positiveAxis)
    {
        this.hid = hid;
        useLeftForPositive = positiveAxis == GamepadSide.Left;
    }
    
    
    public double get()
    {
       
        double positive;
        double negative;
        if (useLeftForPositive )
        {
            positive = hid.getRawAxis(Axis.kLeftTrigger.value);
            negative = -hid.getRawAxis(Axis.kRightTrigger.value);
        }
        else
        {
            positive = hid.getRawAxis(Axis.kRightTrigger.value);
            negative = -hid.getRawAxis(Axis.kLeftTrigger.value);
        }
        
        // Give positive precedence, but with a slight dead zone
        if (positive > 0.01)
        {
            return positive;
        }
        else
        {
            return negative;
        }
    }
}
