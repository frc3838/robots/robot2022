package frc.team3838.core.commands

import frc.team3838.core.utils.NO_OP


/** An abstract base class for an Instant (i.e. one time call ot the execute method). 
 * Extend the and implement the [executeImpl] method and the [getRequiredSubsystems] method.
 */
abstract class AbstractInstant3838Command @JvmOverloads protected constructor (name: String? = null) :
        Abstract3838Command(name)
{
    override fun initializeImpl() = NO_OP()

    override fun isFinishedImpl(): Boolean = true

    override fun endImpl() = NO_OP()
}