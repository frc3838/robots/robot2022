package frc.team3838.core.commands

import com.google.common.collect.ImmutableSet
import frc.team3838.core.meta.API
import frc.team3838.core.subsystems.I3838Subsystem

/**
 * Runs the provided action one time.
 * @see Instant3838NoSubsystemCommand
 * @see Instant3838OneSubsystemCommand
 * @see Instant3838ThreeSubsystemCommand
 */
@API
class Instant3838TwoSubsystemCommand<S1 : I3838Subsystem, S2 : I3838Subsystem>
@JvmOverloads constructor(val subsystemOne: S1,
                          val subsystemTwo: S2,
                          name: String? = null,
                          private val action: (S1, S2) -> Unit) :
        AbstractInstant3838Command(name)
{
    override fun executeImpl() = action.invoke(subsystemOne, subsystemTwo)
    
    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of(subsystemOne, subsystemTwo)
}