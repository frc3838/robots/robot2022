package frc.team3838.core.commands

import com.google.common.collect.ImmutableSet
import edu.wpi.first.wpilibj.command.Command
import edu.wpi.first.wpilibj.command.Subsystem
import frc.team3838.core.commands.common.SleepCommand
import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.logging.LaconicLogger
import frc.team3838.core.meta.API
import frc.team3838.core.meta.DoesNotThrowExceptions
import frc.team3838.core.subsystems.I3838Subsystem
import frc.team3838.core.utils.time.Timed
import frc.team3838.core.utils.time.Timer
import mu.KLogger
import mu.KotlinLogging
import org.slf4j.event.Level
import java.util.concurrent.TimeUnit


val NO_TIMEOUT = TimeDuration.of(Long.MAX_VALUE, TimeUnit.MILLISECONDS)

@Suppress("unused")
val NO_SUBSYSTEMS: ImmutableSet<I3838Subsystem> = ImmutableSet.of()


/**
 * The base command that all Commands used in the 3838 Robot should extend (rather than extending the WPI Lib `Command` class).
 * This 3838 specific implementation adds a number of useful features to the WPI Lib `Command`.
 * 
 *  - **Robust exception handling** : All method implementations in subclasses can safely throw exceptions which are gracefully logged and handled 
 *  by this super class. This completely eliminates the need for boilerplate exception handling in all commands.
 *  - **Improved command timeout capability** : This class can handle timeouts with millisecond precision rather then second precision afford in the
 *  WPI Lib `Command` class. Second precision is sometime not fine grained enough for some commands that need to be timed to the 1/2, 1/4 or even 1/8 second.
 *  Additionally, when the command ends due to a timeout, that information is logged, and retrievable from the `isTimedOut()` method.
 *  - **Subsystem availability/enabled awareness** : A Command will not be initialized or run in the event one or more of the subsystems it depends on 
 *  is disables, or is not available due to an exception occurring when the subsystem initialized. This prevents a flood of secondary error messages as well
 *  as potential red herrings when someone tries to troubleshoot the command when the issue is the subsystem failed to initialize.
 *  
 *  @param name The name of the command. Defaults to the simple class name if not set or if set to `null`.
 *  @param timeoutDuration When the command should timeout. Default is no timeout. NOTE: setting this property after the command has started will
 *                         not change the timeout for the current run. The setting is read (and used) during command initialization.
 */
@API
abstract class Abstract3838Command @JvmOverloads constructor(
        name: String? = null,
        @API private val timeoutDuration: TimeDuration = NO_TIMEOUT) : Command(), Timed
{
    protected val logger: KLogger

    /** Access to the logger for Java subclasses. */
    fun getTheLogger(): KLogger = logger

    @API
    protected val timeoutTimer: Timer = Timer(timeoutDuration)

    @API
    protected val timeRemainingLaconicLogger: LaconicLogger

    @API
    protected val executeLaconicLogger: LaconicLogger
    @API
    protected val initializeLaconicLogger: LaconicLogger

    @API
    protected val executeExceptionLaconicLogger: LaconicLogger

    @API
    protected open var timeoutLogLevel: Level = Level.DEBUG

    /** Updates the interval of the timeout logging to the supplied interval. */
    @API
    protected fun setTimeoutLoggingInterval(interval: TimeDuration) = timeRemainingLaconicLogger.setInterval(interval)

    /** Updates the interval of the timeout logging to the supplied interval. */
    @API
    protected fun setTimeoutLoggingInterval(intervalDuration: Long, intervalTimeUnit: TimeUnit) = timeRemainingLaconicLogger.setInterval(intervalDuration, intervalTimeUnit)

    /** Updates the interval of the timeout logging to the supplied interval, such that the supplied string parses to a TIme Duration. */
    @API
    protected fun setTimeoutLoggingInterval(interval: String) = timeRemainingLaconicLogger.setInterval(interval)
    
    
    /**
     * Set to `true` in the [initializeImpl] method implementation to tell the parent
     * class to not check the [isTimedOut] method as part of its [isFinished] check.
     * It should be very rare that this flag needs to be set. If no timeout
     * is desired, then either do not set one, or set it to the [NO_TIMEOUT]
     * flag value. Setting this flag to true is primarily for the extreme
     * corner case where a command wants more control over when in the 
     * "is finished" logic the `isTimedOut` method is called. By default,
     * the `isFinished` method will first call `isFinishedImpl` and then 
     * `isTimedOut`.
     */
    @API
    protected var noParentIsTimedOutCheck = false
    

    @API
    var hasHadException = false
        private set
    
    @API
    var initializedSuccessfully = true
        private set
    
    private var disableMessageLogged = false
    
    private var disabledMessage = createDisabledMessage()


    @Suppress("MemberVisibilityCanBePrivate")
    var commandTimedOut = false
        private set

    init
    {
        @Suppress("DEPRECATION") // setName in Sendable is deprecated, but not in Command
        super.setName(name ?: this::class.java.simpleName)
        logger = KotlinLogging.logger(this::class.java.name)
        timeRemainingLaconicLogger = LaconicLogger(logger, TimeDuration.of("250 milliseconds"))
        executeLaconicLogger = LaconicLogger(logger, TimeDuration.ofSeconds(5))
        initializeLaconicLogger = LaconicLogger(logger, TimeDuration.ofSeconds(5))
        executeExceptionLaconicLogger = LaconicLogger(logger, TimeDuration.ofSeconds(3))
        registerRequiredSubsystems()
        if (timeoutDuration != NO_TIMEOUT)
        {
            // We add an extra 100 milliseconds so our internal timeout takes precedence, but the WPI timeout acts as a backup
            setTimeout(timeoutDuration.toFractionalSeconds() + 0.1)
        }
    }

    private val disabledPeriodicLogger = LaconicLogger(logger, TimeDuration.of(15, TimeUnit.SECONDS))


    private fun createDisabledMessage() = """COMMAND NOT AVAILABLE: Not all required subsystems for the command '$name' are 
            |enabled, or there was a problem when initializing the command. Therefore the command can not be and will not be executed.""".trimMargin()


    protected abstract fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem>
    

    /**
     * Returns whether this command is finished. If it is, then the command will be removed
     * from the command scheduler and [end] will be called which calls [endImpl]. This 'impl' 
     * method is called by the [isFinished] command in the parent [Abstract3838Command]. The 
     * parent class handles all the minutiae and boilerplate details allowing implementations 
     * in specific commands to focus on the core logic used to determine if a command is finished. 
     * Specifically, it checks if the command is finished due to
     *   - the subsystem not being available either because it is disabled or did not initialize correctly
     *   - an error occurring in either the command or the subsystem
     *   - this `isFinishedImpl` method returning true
     *   - the timeout period expiring
     *
     * Implementations do **not** need to call/check the [isTimedOut] method as that check is handled
     * by the parent `Abstract3838KtCommand` class. The parent `isFinished` method will first check 
     * the child's `isFinishedImpl` method and then the `isTimedOut` method. In the ***very rare*** event 
     * you do not want the parent Abstract class calling `isTimedOut`, set the   
     * [noParentIsTimedOutCheck] property to true in your [initializeImpl] method implementation. Please
     * read the documentation associated with that property for more detail.
     * 
     
     * Returning false will result in the command never ending automatically. It may still be
     * cancelled manually or interrupted by another command. Returning (hard-coded) true will
     * result in the command executing once and finishing immediately. It is recommended to use
     * the [AbstractInstant3838Command], [Instant3838NoSubsystemCommand], [Instant3838OneSubsystemCommand], 
     * [Instant3838TwoSubsystemCommand], [Instant3838ThreeSubsystemCommand] or the
     * [edu.wpi.first.wpilibj.command.InstantCommand] (added in 2017) for this. 
     *
     * @return whether this command is finished.
     *
     */
    @Throws(Exception::class)
    protected abstract fun isFinishedImpl(): Boolean

    @Throws(Exception::class)
    protected abstract fun initializeImpl()

    @Throws(Exception::class)
    protected abstract fun executeImpl()

    @Throws(Exception::class)
    protected abstract fun endImpl()

    @Throws(Exception::class)
    protected open fun interruptedImpl() = end()

    @DoesNotThrowExceptions
    final override fun initialize()
    {
        hasHadException = false
        initializedSuccessfully = false

        if (areAllSubsystemsAreEnabled())
        {
            initializeLaconicLogger.debug {"$name.initialize() called" }
            try
            {
                initializeImpl()
                commandTimedOut = false
                timeoutTimer.start()
                initializedSuccessfully = true
                logger.debug("Running {}", javaClass.simpleName)
            }
            catch (e: Exception)
            {
                hasHadException = true
                initializeLaconicLogger.error(e) {"An exception occurred when initializing the command '$name'. The command will be able to be run at all." }
            }
        }
        else
        {
            initializeLaconicLogger.warn {"Not all required subsystems for the command '$name' are enabled. Therefore the command can not be and will not be initialized or executed." }
        }
    }


    @DoesNotThrowExceptions
    final override fun execute()
    {
        super.execute()
        if (areAllSubsystemsAreEnabled())
        {
            try
            {
                executeLaconicLogger.trace{ "$name.execute() running" }
                executeImpl()
            }
            catch (e: Exception)
            {
                executeExceptionLaconicLogger.error(e) {"An exception occurred in $name.execute() Cause Summary: $e" }
                hasHadException = true
            }
        }
        else
        {
            if (!disableMessageLogged)
            {
                disabledMessage = createDisabledMessage()
                disabledPeriodicLogger.info { "In execute: $disabledMessage" }
                disableMessageLogged = true
            }
            disabledPeriodicLogger.debug { "In execute: $disabledMessage" }
        }
    }


    @DoesNotThrowExceptions
    final override fun isFinished(): Boolean
    {
        return try
        {
            // the isTimedOut() function logs that the command has timed out the first time it is called and the command timed out. 
            // We call it last so that log message does not appear for other situations. For example, if the command finished properly, 
            // or if there is a bad initialization, we do not want "command has timed out" logged later on
            !initializedSuccessfully || hasHadException || isFinishedImpl() || (!noParentIsTimedOutCheck && isTimedOut)
        }
        catch (e: Exception)
        {
            logger.error(e) { "An exception occurred when calling $name.isFinished(). Cause Summary: $e" }
            hasHadException = true
            true
        }
    }


    @DoesNotThrowExceptions
    final override fun end()
    {
        // Called once after isFinished returns true
        // do any clean up or post command work here
        try
        {
            endImpl()
        }
        catch (e: Exception)
        {
            if (areAllSubsystemsAreEnabled())
            {
                logger.error(e) { "An exception occurred when calling $name.end(). Cause Summary: $e" }
            }
        }

    }


    @DoesNotThrowExceptions
    final override fun interrupted()
    {
        try
        {
            logger.debug() { "interrupted() called for command '$name'" }
            interruptedImpl()
        }
        catch (e: Exception)
        {
            if (areAllSubsystemsAreEnabled())
            {
                logger.error(e) { "An exception occurred when calling $name.interrupted(). Cause Summary: $e" }
                hasHadException = true
            }
        }
    }

    /**
     * Returns whether or not the command has timed out. This overridden implementation
     * in [Abstract3838Command] has millisecond precision whereas the default
     * implementation in the WpiLib [Command] class only has second precision. While
     * we do not want to try and timeout commands to the millisecond as there will always be 
     * a few milliseconds of 'fluff' time, there are some use cases where we need a bit more 
     * fine grained timeouts with 1/2 or 1/4 second, or possibly even 1/8 second, 
     * precision rather than the a full second that is provided by the WpiLib `Command` class.
     *
     * 
     * This method is called by the `isFinished` implementation within this [Abstract3838Command]
     * class.
     * 
     * @return whether the time has expired
     */
    @Synchronized
    override fun isTimedOut(): Boolean
    {
        // The parent method is only accurate to the second. We want millisecond 
        // accuracy so we can have partial second, such a 1½ seconds, timeouts.
        // Thus we are fully overriding the parent method
        
        if (timeoutDuration == NO_TIMEOUT) return false
        if (commandTimedOut) return true

        timeRemainingLaconicLogger.logAt(timeoutLogLevel) { "Time remaining before timeout: ${timeoutTimer.timeRemaining()}" }
        
        commandTimedOut = timeoutTimer.isTimedOut()
        if (commandTimedOut && !(this is SleepCommand))
        {
            logger.info { "Command $name has timed out" }
        }
        
        return commandTimedOut
    }


    private fun registerRequiredSubsystems()
    {
        getRequiredSubsystems().forEach { subsystem ->
            requires(subsystem as Subsystem)
        }
    }

    @API
    protected open fun areAllSubsystemsAreEnabled(): Boolean
    {
        for (subsystem in getRequiredSubsystems())
        {
            if (!subsystem.isEnabled)
            {
                return false
            }
        }
        //All subsystems are enabled. So as long as we are initialized properly, we are good to go.
        return true
    }
}
