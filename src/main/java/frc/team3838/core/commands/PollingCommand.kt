package frc.team3838.core.commands

import com.google.common.collect.ImmutableSet
import frc.team3838.core.meta.API
import frc.team3838.core.subsystems.I3838Subsystem

/**
 * A command that simply calls the provided (construction parameter) [action] in the execute method on each loop.
 * By default, `false` is returned for the for [isFinished] method. This behavior can be modified by
 * providing a function for the [isFinishedCondition] construction parameter.
 *
 * An example use of this command would be keeping a status updated/current.
 *
 * Note that by default the action is **not** called during command initialization (i.e. when the `initialize` method
 * is called). This can be modified via the [runActionDuringInitialize] parameter.
 * 
 * When using in Java, the supplied lambda will need to return `Unit.INSTANCE`. For example:
 * 
 * ```
 * // Java
 * setDefaultCommand(new PollingCommand(() ->
 *      {
 *          SmartDashboard.putString("Speed", MathUtils.formatNumber(getSpeed(), 3));
 *          return Unit.INSTANCE;
 *      }));
 * ```
 *
 * @param action the action to run regularly when the [execute] method is called by the command scheduler
 * @param theRequiredSubsystems the subsystems required by the command. Defaults to an empty set.
 * @param runActionDuringInitialize whether to tun the action during the initialization of the command
 *                                  when the command scheduler calls the [initialize] method. Defaults to false
 * @param isFinishedCondition the condition to call in the [isFinished] method to determine if the command
 *                            is finished. The default is to always return `false`, thus looping indefinitely.
 * 
 * For an alternative base class that can be implemented/extended, see the [AbstractPollingCommand] class.
 * @see AbstractPollingCommand              
 */
@API
open class PollingCommand @JvmOverloads constructor(private val theRequiredSubsystems: Set<I3838Subsystem> = emptySet(),
                                                    private val runActionDuringInitialize: Boolean = false,
                                                    private val isFinishedCondition: () -> Boolean = { false },
                                                    private val action: () -> Unit) : Abstract3838Command()
{
    @API
    @JvmOverloads
    constructor(requiredSubsystem: I3838Subsystem,
                runActionDuringInitialize: Boolean = false,
                isFinishedCondition: () -> Boolean = { false },
                action: () -> Unit) : this(setOf(requiredSubsystem), runActionDuringInitialize, isFinishedCondition, action)

    @API
    constructor(action: () -> Unit, theRequiredSubsystems: Set<I3838Subsystem> = emptySet(), runActionDuringInitialize: Boolean = false) : this(theRequiredSubsystems, runActionDuringInitialize, action = action)

    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.copyOf(theRequiredSubsystems)

    override fun initializeImpl()
    {
        if (runActionDuringInitialize)
        {
            action.invoke()
        }
    }

    override fun executeImpl() = action.invoke()

    override fun isFinishedImpl(): Boolean = isFinishedCondition.invoke()

    override fun endImpl() { /* No Op */ }
}
