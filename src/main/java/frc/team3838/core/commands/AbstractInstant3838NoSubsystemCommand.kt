package frc.team3838.core.commands

import com.google.common.collect.ImmutableSet
import frc.team3838.core.subsystems.I3838Subsystem

/**
 * Runs the provided action one time.
 * @see Instant3838OneSubsystemCommand
 * @see Instant3838TwoSubsystemCommand
 * @see Instant3838ThreeSubsystemCommand
 */
abstract class AbstractInstant3838NoSubsystemCommand(name: String? = null) :
        AbstractInstant3838Command(name)
{
    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = NO_SUBSYSTEMS
}