package frc.team3838.core.commands

import com.google.common.collect.ImmutableSet
import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.subsystems.I3838Subsystem

abstract class Abstract3838NoSubsystemsCommand(
        /** The name of the command. Defaults to the simple class name if not set or if set to `null`. */
        name: String? = null,
        /** When the command should timeout. Default is no timeout. NOTE: setting this property after the command has started will
         * not change the timeout for the current run. The setting is read (and used) during command initialization. */
        timeoutDuration: TimeDuration = NO_TIMEOUT)
    : Abstract3838Command(name, timeoutDuration)
{
    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = NO_SUBSYSTEMS
}