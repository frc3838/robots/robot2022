package frc.team3838.core.commands.common

import frc.team3838.core.commands.AbstractInstant3838NoSubsystemCommand
import frc.team3838.core.utils.NO_OP


object NoOpCommand : AbstractInstant3838NoSubsystemCommand()
{
    override fun executeImpl() = NO_OP()
}