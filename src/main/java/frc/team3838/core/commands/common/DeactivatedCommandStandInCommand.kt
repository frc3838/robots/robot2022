package frc.team3838.core.commands.common

import edu.wpi.first.wpilibj.command.Command
import frc.team3838.core.commands.AbstractInstant3838NoSubsystemCommand
import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.logging.LaconicLeveledLogger
import frc.team3838.core.meta.API
import org.slf4j.event.Level

/**
 * A wrapper command that is used when a non null command is needed, but the actual command needed
 * cannot be instantiated.
 */
@API
class DeactivatedCommandStandInCommand(private val deactivatedCommandName: String) : AbstractInstant3838NoSubsystemCommand()
{
    private val logMsg: String = "The \"No Action\" Stand In command execution has been called in place of $deactivatedCommandName. " +
                                           "This is may be the because subsystem the command typically assigned to this action uses has been disabled."
                                           
    private val warnLaconicLogger = LaconicLeveledLogger(logger, TimeDuration.ofSeconds(20), Level.WARN)
    private val traceLaconicLogger = LaconicLeveledLogger(logger, TimeDuration.ofSeconds(5), Level.TRACE)

    @API
    constructor(command: Command) : this(command.javaClass)

    @API
    constructor(commandClass: Class<out Command?>) : this(commandClass.simpleName)

    @Throws(Exception::class)
    override fun initializeImpl()
    {
        logger.warn { logMsg }
    }

    @Throws(Exception::class)
    override fun executeImpl()
    {
        warnLaconicLogger.log{ logMsg }
        traceLaconicLogger.log{ logMsg }
    }
}