package frc.team3838.core.commands.common

import edu.wpi.first.wpilibj.GenericHID
import edu.wpi.first.wpilibj.buttons.JoystickButton
import frc.team3838.core.commands.AbstractInstant3838NoSubsystemCommand
import frc.team3838.core.meta.API
import mu.KotlinLogging

/**
 * A command that can be used (during development) to test a button, mostly to ensure
 * you have the correct button mapped. The command will log a message when executed.
 * 
 * @param buttonName the name of the button being tested
 * @param activity the activity , such as 'pushed', 'released', etc.
 */
@API
open class ButtonTesterCommand(private val buttonName: String, private val activity: String) : AbstractInstant3838NoSubsystemCommand()
{
    companion object
    {
        /**
         * @param buttonName the name of the button being tested
         * @param activity the activity , such as 'pushed', 'released', etc.
         */
        @JvmStatic
        fun create(buttonName: String, activity: String): ButtonTesterCommand = ButtonTesterCommand(buttonName, activity)
        
        /**
         * @param joystickButton the button being tested
         * @param activity the activity , such as 'pushed', 'released', etc.
         */
        @JvmStatic
        fun create(joystickButton: JoystickButton, activity: String): ButtonTesterCommand
        {
            var joystickPort: String
            var buttonNumber: String
            try
            {
                val field = JoystickButton::class.java.getDeclaredField("m_joystick")
                field.isAccessible = true
                val genericHid = field[joystickButton] as GenericHID
                val port = genericHid.port
                joystickPort = port.toString()
            }
            catch (e: Exception)
            {
                val logger = KotlinLogging.logger {}
                logger.debug(e){"Could not reflectively determine the joystick port. Cause Summary: $e"}
                joystickPort = "<undetermined>"
            }
            try
            {
                val field = JoystickButton::class.java.getDeclaredField("m_buttonNumber")
                field.isAccessible = true
                buttonNumber = (field[joystickButton] as Int).toString()
            }
            catch (e: Exception)
            {
                val logger = KotlinLogging.logger {}
                logger.debug(e){"Could not reflectively determine the joystick button number. Cause Summary: $e"}
                buttonNumber = "<undetermined>"
            }
            val buttonName = "Button # $buttonNumber on the joystick on port $joystickPort"
            return ButtonTesterCommand(buttonName, activity)
        }
    }
    
    override fun executeImpl()
    {
        logger.info { "Button Test: '$buttonName' :: $activity" }
    }
}