package frc.team3838.core.commands.log

import frc.team3838.core.commands.AbstractInstant3838NoSubsystemCommand
import frc.team3838.core.logging.logAt
import org.slf4j.event.Level

/**
 * Simple command that logs a statement.
 */
class LogStatementCommand @JvmOverloads constructor(private val message: String = "", private val level: Level = Level.INFO) : AbstractInstant3838NoSubsystemCommand()
{
    @Throws(Exception::class)
    override fun executeImpl()
    {
        logger.logAt(level, message)
    }
}