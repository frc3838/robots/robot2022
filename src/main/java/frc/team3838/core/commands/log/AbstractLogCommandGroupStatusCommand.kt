package frc.team3838.core.commands.log

import edu.wpi.first.wpilibj.command.Command
import frc.team3838.core.commands.AbstractInstant3838NoSubsystemCommand
import java.time.LocalTime
import java.time.format.DateTimeFormatter

abstract class AbstractLogCommandGroupStatusCommand protected constructor(private val commandToLog: Command) : 
        AbstractInstant3838NoSubsystemCommand()
{
    var statusTime: LocalTime? = null
        private set
    
    protected abstract val status: String
    
    override fun executeImpl()
    {
        this.statusTime = LocalTime.now()
        logger.info{"~~~~~ $status ${commandToLog.name} at ${timeFormatter.format(LocalTime.now())} $additionalMessage"}
    }

    protected open val additionalMessage: String
        get() = ""


    companion object
    {
        protected val timeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss.SSS")
    }

}