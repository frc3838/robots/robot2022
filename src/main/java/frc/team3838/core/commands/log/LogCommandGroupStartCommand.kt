package frc.team3838.core.commands.log

import edu.wpi.first.wpilibj.command.Command

class LogCommandGroupStartCommand(commandToLog: Command) : AbstractLogCommandGroupStatusCommand(commandToLog)
{
    override val status: String
        get() = "STARTING"
}