package frc.team3838.core.commands.log

import frc.team3838.core.commands.Abstract3838Command
import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.logging.LaconicLogger
import frc.team3838.core.meta.API
import frc.team3838.core.utils.NO_OP
import org.slf4j.Logger
import org.slf4j.event.Level


@API
abstract class LaconicLoggingCommand(private val laconicLogger: LaconicLogger,
                                     private val level: Level): Abstract3838Command()
{
    @API
    constructor(underlyingLogger: Logger, interval: TimeDuration, level: Level) : 
            this(LaconicLogger(underlyingLogger, interval), level)

    override fun isFinishedImpl(): Boolean = false

    override fun initializeImpl() = NO_OP()

    override fun executeImpl() = laconicLogger.logAt(level) { getMessage() }

    override fun endImpl() = NO_OP()
    
    abstract fun getMessage(): String
}