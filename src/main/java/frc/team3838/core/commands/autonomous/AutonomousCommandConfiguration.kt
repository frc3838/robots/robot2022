package frc.team3838.core.commands.autonomous

import edu.wpi.first.wpilibj.command.Command

interface AutonomousCommandConfiguration
{
    val command: Command

    val description: String
}