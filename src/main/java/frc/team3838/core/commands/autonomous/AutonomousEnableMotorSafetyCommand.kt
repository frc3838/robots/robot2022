package frc.team3838.core.commands.autonomous

import com.google.common.collect.ImmutableSet
import frc.team3838.core.commands.AbstractInstant3838Command
import frc.team3838.core.commands.NO_SUBSYSTEMS
import frc.team3838.core.subsystems.I3838Subsystem
import frc.team3838.core.subsystems.Subsystems
import frc.team3838.core.subsystems.drive.I3838DriveTrainSubsystem

class AutonomousEnableMotorSafetyCommand : AbstractInstant3838Command()
{
    private val driveTrainSubsystem: I3838DriveTrainSubsystem? = Subsystems.getDriveTrainSubsystem()

    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem>
    {
        Subsystems.getDriveTrainSubsystem()
        return if (driveTrainSubsystem != null)
        {
            ImmutableSet.of<I3838Subsystem>(driveTrainSubsystem)
        }
        else
        {
            logger.warn { "No DriveTrain subsystem found. AutonomousEnableMotorSafetyCommand will not do anything." }
            NO_SUBSYSTEMS
        }
    }

  
    @Throws(Exception::class)
    override fun executeImpl()
    {
        driveTrainSubsystem?.enableMotorSafetyPostAutonomousMode()
    }
}