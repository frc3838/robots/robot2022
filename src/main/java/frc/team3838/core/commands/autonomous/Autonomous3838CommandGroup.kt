package frc.team3838.core.commands.autonomous

import frc.team3838.core.commands.NO_TIMEOUT
import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.meta.API

@API
open class Autonomous3838CommandGroup(sequentialCommands: List<CommandGroupEntry> = emptyList(),
                                      name: String? = null,
                                      timeoutDuration: TimeDuration = NO_TIMEOUT):
        AbstractAutonomous3838CommandGroup(sequentialCommands, name, timeoutDuration)
{
    @API
    constructor(name: String? = null, timeoutDuration: TimeDuration = NO_TIMEOUT, vararg commandGroupEntries: CommandGroupEntry) :
            this(commandGroupEntries.asList(), name, timeoutDuration)

    @API
    constructor(timeoutDuration: TimeDuration = NO_TIMEOUT, vararg commandGroupEntries: CommandGroupEntry) :
            this(commandGroupEntries.asList(), timeoutDuration = timeoutDuration)

    @API
    constructor(name: String? = null, vararg commandGroupEntries: CommandGroupEntry) : this(commandGroupEntries.asList(), name = name)

    @API
    constructor(vararg commandGroupEntries: CommandGroupEntry) : this(commandGroupEntries.asList())
}