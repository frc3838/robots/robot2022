package frc.team3838.core.commands.autonomous

import edu.wpi.first.wpilibj.command.Command
import frc.team3838.core.meta.API
import org.apache.commons.lang3.builder.EqualsBuilder
import org.apache.commons.lang3.builder.HashCodeBuilder
import org.apache.commons.lang3.builder.ToStringBuilder
import org.apache.commons.lang3.builder.ToStringStyle

@API
class AutonomousCommandConfigurationImpl(override val description: String, override val command: Command) :
        AutonomousCommandConfiguration
{

    override fun toString(): String
    {
        return ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
            .append("description", description)
            .toString()
    }

    override fun equals(other: Any?): Boolean
    {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as AutonomousCommandConfigurationImpl
        return EqualsBuilder()
            .append(command, that.command)
            .append(description, that.description)
            .isEquals
    }

    override fun hashCode(): Int
    {
        return HashCodeBuilder(17, 37)
            .append(command)
            .append(description)
            .toHashCode()
    }

}