package frc.team3838.core.commands.autonomous

import frc.team3838.core.commands.common.SleepCommand
import frc.team3838.core.commands.drive.SimpleDriveCommand
import frc.team3838.core.config.time.PartialSeconds
import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.units.Speed
import frc.team3838.game.commands.LowerCollectorArm
import frc.team3838.game.commands.ShootSequenceCommand
import frc.team3838.game.commands.StartIntakeAndTransportMotorsCommand
import frc.team3838.game.commands.StartShooterAtSpeed
import frc.team3838.game.subsystems.BallCollectorSubsystem

// MITCH: Change autonomous values here - Shooter Speed, Drive Speed, Drive Timeout
const val AUTO_MODE_FIRST_SHOT_SHOOTER_SPEED = 1000 // 804,904, 80% bottom wheel, 850, 1000
const val AUTO_MODE_SECOND_SHOT_SHOOTER_SPEED = 1150 // 1000,1100,1150
const val AUTO_MODE_SPEED = -0.2
const val AUTO_MODE_DRIVE_TIME_IN_SECONDS = 2.3 // Needs to be a float number 2.5

object AutonomousModeShootAndMoveAndShoot: AbstractAutonomous3838CommandGroup(
        listOf(

                CommandGroupEntry(StartShooterAtSpeed(AUTO_MODE_FIRST_SHOT_SHOOTER_SPEED), AddAs.Sequential),
                CommandGroupEntry(LowerCollectorArm(), AddAs.Sequential),
                CommandGroupEntry(StartShooterAtSpeed(0 ), AddAs.Sequential),
                CommandGroupEntry(SleepCommand(TimeDuration.ofSeconds(0, PartialSeconds.HalfSecond)), AddAs.Sequential),
                CommandGroupEntry(StartIntakeAndTransportMotorsCommand(BallCollectorSubsystem.AUTO_DEFAULT_INTAKE_MOTOR_SPEED), AddAs.Sequential),
                // MITCH: Here is where you can add and change shooter speeds and delays
                CommandGroupEntry(StartShooterAtSpeed(AUTO_MODE_FIRST_SHOT_SHOOTER_SPEED), AddAs.Sequential),
                CommandGroupEntry(SleepCommand(TimeDuration.ofSeconds(0, PartialSeconds.HalfSecond)), AddAs.Sequential),
                CommandGroupEntry(ShootSequenceCommand(), AddAs.Sequential),
                CommandGroupEntry(SleepCommand(TimeDuration.ofSeconds(0, PartialSeconds.HalfSecond)), AddAs.Sequential),
                CommandGroupEntry(StartShooterAtSpeed(AUTO_MODE_SECOND_SHOT_SHOOTER_SPEED), AddAs.Sequential),
                CommandGroupEntry(SimpleDriveCommand(Speed(AUTO_MODE_SPEED), TimeDuration.ofSeconds(AUTO_MODE_DRIVE_TIME_IN_SECONDS)), AddAs.Sequential),
                CommandGroupEntry(SleepCommand(TimeDuration.ofSeconds(2, PartialSeconds.NoPartial)), AddAs.Sequential),
                CommandGroupEntry(ShootSequenceCommand(), AddAs.Sequential),
                CommandGroupEntry(StartIntakeAndTransportMotorsCommand(BallCollectorSubsystem.TELEOP_DEFAULT_INTAKE_MOTOR_SPEED), AddAs.Sequential),
                ))


object AutonomousModeShootAndMove: AbstractAutonomous3838CommandGroup(
        listOf(

                CommandGroupEntry(StartShooterAtSpeed(AUTO_MODE_FIRST_SHOT_SHOOTER_SPEED), AddAs.Sequential),
                CommandGroupEntry(LowerCollectorArm(), AddAs.Sequential),
                CommandGroupEntry(StartIntakeAndTransportMotorsCommand(BallCollectorSubsystem.AUTO_DEFAULT_INTAKE_MOTOR_SPEED), AddAs.Sequential),
                CommandGroupEntry(SleepCommand(TimeDuration.ofSeconds(1, PartialSeconds.OneSixteenthSecond)), AddAs.Sequential),
                CommandGroupEntry(ShootSequenceCommand(), AddAs.Sequential),
                CommandGroupEntry(SleepCommand(TimeDuration.ofSeconds(0, PartialSeconds.HalfSecond)), AddAs.Sequential),
                CommandGroupEntry(SimpleDriveCommand(Speed(AUTO_MODE_SPEED), TimeDuration.ofSeconds(AUTO_MODE_DRIVE_TIME_IN_SECONDS)), AddAs.Sequential),
                CommandGroupEntry(StartIntakeAndTransportMotorsCommand(BallCollectorSubsystem.TELEOP_DEFAULT_INTAKE_MOTOR_SPEED), AddAs.Sequential),

                ))


object AutonomousModeShootOnly: AbstractAutonomous3838CommandGroup(
        listOf(

                CommandGroupEntry(StartShooterAtSpeed(AUTO_MODE_FIRST_SHOT_SHOOTER_SPEED), AddAs.Sequential),
                CommandGroupEntry(LowerCollectorArm(), AddAs.Sequential),
                CommandGroupEntry(StartIntakeAndTransportMotorsCommand(BallCollectorSubsystem.AUTO_DEFAULT_INTAKE_MOTOR_SPEED), AddAs.Sequential),
                CommandGroupEntry(SleepCommand(TimeDuration.ofSeconds(1, PartialSeconds.OneSixteenthSecond)), AddAs.Sequential),
                CommandGroupEntry(ShootSequenceCommand(), AddAs.Sequential),
                CommandGroupEntry(SleepCommand(TimeDuration.ofSeconds(0, PartialSeconds.HalfSecond)), AddAs.Sequential),
                CommandGroupEntry(StartIntakeAndTransportMotorsCommand(BallCollectorSubsystem.TELEOP_DEFAULT_INTAKE_MOTOR_SPEED), AddAs.Sequential),

                ))

