package frc.team3838.core.commands.autonomous

import frc.team3838.core.commands.AbstractInstant3838NoSubsystemCommand

object AutonomousDoNothingCommand : AbstractInstant3838NoSubsystemCommand()
{
    private var haveLoggedMessage = false
    
    override fun initializeImpl()
    {
        haveLoggedMessage = false
    }

    override fun executeImpl()
    {
        if (!haveLoggedMessage)
        {
            logger.info("===========================================")
            logger.info("+++DO NOTHING AUTONOMOUS COMMAND RUNNING+++")
            logger.info("===========================================")
            haveLoggedMessage = true
        }
    }
}