package frc.team3838.core.commands.drive

import com.google.common.collect.ImmutableSet
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard
import frc.team3838.core.RobotProperties
import frc.team3838.core.commands.Abstract3838Command
import frc.team3838.core.commands.NO_TIMEOUT
import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.config.time.TimeDuration.Companion.of
import frc.team3838.core.logging.LaconicLogger
import frc.team3838.core.meta.API
import frc.team3838.core.subsystems.Abstract3838NavxSubsystem
import frc.team3838.core.subsystems.I3838Subsystem
import frc.team3838.core.subsystems.NavxSubsystem
import frc.team3838.core.subsystems.Subsystems
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem.RobotDirection
import frc.team3838.core.units.Angle
import frc.team3838.core.units.Speed
import frc.team3838.core.units.unaryMinus
import frc.team3838.core.utils.format
import java.util.concurrent.TimeUnit

/**
 * The base class for all drive commands. 
 * @param targetAngle The target angle to drive at. Defaults to 0.0 (i.e. driving straight).
 * @param speed The speed to drive at. Defaults to the value returned by [frc.team3838.core.subsystems.drive.I3838DriveTrainSubsystem.getMaxSpeedForAutonomous]
 * @param robotDirection The direction to drive. Defaults to [RobotDirection.Forward]
 * @param enableTuning Whether to enable tuning by putting a field on the smartdashboard to allow tweaking the p and adjustment angle values   
 * @param name The name of the command. If set to null (the default), the simple name of the class is used
 * @param timeoutDuration A timeout duration. Defaults to no timeout
 */
@API
open class DriveCommand constructor(
        @API protected val targetAngle: Angle = Angle(0.0),
        protected var speed: Speed = Speed(Subsystems.getDriveTrainSubsystem().maxSpeedForAutonomous),
        protected val robotDirection: RobotDirection = RobotDirection.Forward,
        /* Sets if tuning is enabled, meaning the p value is read off of the dashboard. */
        protected var enableTuning: Boolean = false,
        name: String? = null,
        timeoutDuration: TimeDuration = NO_TIMEOUT) : Abstract3838Command(name, timeoutDuration)
{
    @API
    constructor(robotDirection: RobotDirection,
                targetAngle: Angle = Angle(0.0), 
                speed: Speed = Speed(Subsystems.getDriveTrainSubsystem().maxSpeedForAutonomous),
                enableTuning: Boolean = false,
                name: String? = null,
                timeoutDuration: TimeDuration = NO_TIMEOUT
               ): this(targetAngle, speed, robotDirection, enableTuning, name, timeoutDuration)

    @API
    @JvmOverloads
    constructor(targetAngle: Double = 0.0,
                robotDirection: RobotDirection,
                speed: Double = Subsystems.getDriveTrainSubsystem().maxSpeedForAutonomous,
                name: String? = null,
                enableTuning: Boolean = false,
                timeoutDuration: TimeDuration = NO_TIMEOUT
               ): this(Angle(targetAngle), Speed(speed), robotDirection, enableTuning, name, timeoutDuration)


    init
    {
        initTuningOnSmartDashboard(name)
    }

    private val currentAngleLaconicLogger = LaconicLogger(logger, of(250, TimeUnit.MILLISECONDS))
    private val adjustmentAngleLaconicLogger = LaconicLogger(logger, of(250, TimeUnit.MILLISECONDS))
    private val commandCannotRunLaconicLogger = LaconicLogger(logger, of(30, TimeUnit.SECONDS))
    private val warningLaconicLogger = LaconicLogger(logger, of(30, TimeUnit.SECONDS))

    /** Whether the navX should be reset at during the initialization of the command. Defaults to `true`. */
    open var shouldResetNavX = true
    
    open var finishedFlag: Boolean = false
    
    @API
    protected var angleAtInitialization: Double = 0.0
    
    protected open val driveTrainSubsystem: Abstract3838DriveTrainSubsystem
         get() = Subsystems.getDriveTrainSubsystem()
    
    protected open val navxSubsystem: Abstract3838NavxSubsystem
         get() = NavxSubsystem.getInstance()
    
    protected var p: Double = RobotProperties.DEFAULT_P
        set(value)
        {
            field = value
            logger.info { "'p' set to ${p.format(5)} for Drive Command $name" }
        }

    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of(driveTrainSubsystem, navxSubsystem)

    @API
    protected val targetAngleFormatted: String by lazy { targetAngle.toString()}

    /** 
     * Indicates if the angle correction should be applied during the driveStraight() method call. 
     * The default implementation is to always return `true`. Subclasses however
     * many want/need to override to only correct when certain conditions are met, or to not
     * correct when certain conditions are met (such as for example not correcting for the last x inches 
     * or movement. 
     */
    protected open fun shouldApplyCorrection(): Boolean = true

    /** Indicates of this drive command can run by checking that the navX is not null and that all requires subsystems are enabled. */
    protected open fun canCommandRun(): Boolean = navxSubsystem.navx != null && areAllSubsystemsAreEnabled()
    
    
    override fun initializeImpl()
    {
        if (canCommandRun())
        {
            logger.debug{"$name enableTuning = $enableTuning"}
            if (enableTuning)
            {
                logger.warn{ "Enable Tuning is Enabled Reading navX offset angle and drive straight P from Dashboard for $name"}
                p = SmartDashboard.getNumber(SDB_NAME_P, RobotProperties.getDriveStraightP())
                logger.info { "'p' for $name Drive Command read from dashboard as ${p.format(5)}" }

                val adjAngle = SmartDashboard.getNumber(SDB_NAME_OFFSET_ANGLE, RobotProperties.getNavxAdjustmentAngle())
                navxSubsystem.navx?.reset()
                navxSubsystem.navx?.angleAdjustment = adjAngle
            }
            else
            {
                p = RobotProperties.getDriveStraightP()
                logger.info{"'p' for $name Drive Command read from properties file as ${p.format(5)}"}
            }
            
            if (shouldResetNavX)
            {
                logger.debug { "resetting navX for $name" }
                navxSubsystem.navx?.reset()
                angleAtInitialization = 0.0
            }
            else
            {
                angleAtInitialization = navxSubsystem.navx?.angle ?: 0.0
                logger.debug { "No navX reset for $name" }
            }

            logger.info{"Initializing $name"}
            logger.info("    using P of ${p.format(5)} and adjustment angle of ${navxSubsystem.navx?.angleAdjustment} for $name")
            finishedFlag = false
        }
        else
        {
            finishedFlag = true
            logger.warn{"COMMAND DISABLED: The $name command cannot and will not run. This is likely due to the navX or other subsystems not being fully enabled."}
        }
    }

    override fun executeImpl()
    {
        driveStraight()
    }
    
    @API
    protected fun driveStraight()
    {
        // based on code sample at http://wpilib.screenstepslive.com/s/4485/m/13809/l/599713-gyros-measuring-rotation-and-controlling-robot-driving-direction
        try
        {
            if (canCommandRun())
            {
                
                val currentAngle = navxSubsystem.navx!!.angle - angleAtInitialization
                
                if (enableTuning)
                {
                    val currentAngleFormatted = currentAngle.format()
                    SmartDashboard.putString(SDB_NAME_CURRENT_ANGLE, currentAngleFormatted)
                    currentAngleLaconicLogger.debug("Target Angle = {}; Current Angle = {}", targetAngleFormatted, currentAngleFormatted)
                }

                // REMEMBER the joysticks are negative for forward, and positive for reverse
                val theSpeed = if (robotDirection == RobotDirection.Forward) -this.speed else this.speed
                val adjustment = if (shouldApplyCorrection()) (targetAngle.value - currentAngle) * p else 0.0
                adjustmentAngleLaconicLogger.debug{ "$name : adjustment angle = $adjustment   current angle = ${currentAngle.format()}  target angle = $targetAngleFormatted  speed = $theSpeed"}
                
                driveTrainSubsystem.driveRobotViaArcadeControlRaw(adjustment, theSpeed.value)
            }
            else
            {
                commandCannotRunLaconicLogger.warn{ "COMMAND DISABLED: The $name command cannot and will not run. This is likely due to the navX or other subsystems not being fully enabled." }
            }
        }
        catch (t: Throwable)
        {
            warningLaconicLogger.warn(t){ "An Exception occurred in driveStraight() for $name. Cause Summary: $t"}
        }
    }

    override fun isFinishedImpl(): Boolean = canCommandRun() && finishedFlag

    override fun endImpl()
    {
        driveTrainSubsystem.stop()
        logger.info { "$name ended. Encoder Readings:  L = ${driveTrainSubsystem.leftDistanceInInches.format(3)} R = ${driveTrainSubsystem.rightDistanceInInches.format(3)}" }
    }
    
    private fun initTuningOnSmartDashboard(name: String?)
    {
        if (enableTuning)
        {
            val flagValue = -1000.0
            logger.info { "Tuning IS enabled for $name. Initializing the dashboard." }
            var currentP: Double = flagValue
            try
            {
                currentP = SmartDashboard.getNumber(SDB_NAME_P, flagValue)
            }
            catch (ignore: Throwable)
            {
            }

            if (currentP == flagValue)
            {
                // We need to initialize the smart dashboard as the value is not currently there
                logger.debug("Placing initial tuning value on Smartdashboard")
                SmartDashboard.putNumber(SDB_NAME_P, RobotProperties.DEFAULT_P)
                SmartDashboard.putNumber(SDB_NAME_OFFSET_ANGLE, 0.0)
            }
            else
            {
                SmartDashboard.putNumber(SDB_NAME_P, currentP)
                SmartDashboard.putNumber(SDB_NAME_OFFSET_ANGLE, SmartDashboard.getNumber(SDB_NAME_OFFSET_ANGLE, 0.0))
            }
        }
        else
        {
            logger.debug { "Tuning is not enabled for $name" }
        }
    }   
}