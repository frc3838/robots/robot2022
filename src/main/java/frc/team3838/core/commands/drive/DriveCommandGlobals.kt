package frc.team3838.core.commands.drive


const val SDB_NAME_P: String = "DriveStraight-P"
const val SDB_NAME_OFFSET_ANGLE: String = "DriveStraight-Offset"
const val SDB_NAME_CURRENT_ANGLE = "DriveStraight-CurrentAngle"