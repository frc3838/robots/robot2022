package frc.team3838.core.commands.drive

import com.google.common.collect.ImmutableSet
import frc.team3838.core.commands.AbstractInstant3838Command
import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.subsystems.I3838Subsystem
import frc.team3838.core.units.Speed
import frc.team3838.core.utils.NO_OP
import frc.team3838.core.utils.format
import java.util.function.DoubleSupplier

class SimpleDriveFromDashboardCommand(val speedSettingSupplier: DoubleSupplier,
                                      val timeoutInSecondsSupplier: DoubleSupplier,
                                     ): AbstractInstant3838Command("Run Drive Test")
{


    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of()

    override fun initializeImpl()
    {
        val speed = Speed(speedSettingSupplier.asDouble)
        val seconds = timeoutInSecondsSupplier.asDouble
        logger.info { "Running Drive test using settings of speed = ${speed.value.format(3)}  seconds: ${seconds.format(3)}" }
        val command = SimpleDriveCommand(speed,
                                         timeoutDuration = TimeDuration.ofSeconds(seconds),
                                         name = "Drive at ${speed.value.format(3)} for ${seconds.format(3)} seconds"
                                        )
        command.start()
    }

    override fun executeImpl() = NO_OP()
}