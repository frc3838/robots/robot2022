package frc.team3838.core.commands.drive;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import frc.team3838.core.commands.autonomous.Autonomous3838CommandGroup;
import frc.team3838.core.commands.autonomous.CommandGroupEntry;
import frc.team3838.core.commands.common.SleepCommand;
import frc.team3838.core.config.time.PartialSeconds;



public class AngleAdjustTestingCommand extends Autonomous3838CommandGroup
{
    @SuppressWarnings("UnusedDeclaration")
    protected final Logger logger = LoggerFactory.getLogger(getClass());
    
    
    public AngleAdjustTestingCommand()
    {
        super(new CommandGroupEntry(SleepCommand.ofSeconds(PartialSeconds.OneSixteenthSecond)),
//              new CommandGroupEntry(new DrivePidRotateNoResetCommand(-12, TimeDuration.ofSeconds(3))),
              new CommandGroupEntry(SleepCommand.ofSeconds(1))
             
        );
        throw new UnsupportedOperationException("Need to update the DrivePidRotateNoResetCommand and then uncomment code. in this constructor");
    }
}
