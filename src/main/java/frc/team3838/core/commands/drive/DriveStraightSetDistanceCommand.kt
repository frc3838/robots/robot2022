package frc.team3838.core.commands.drive

import frc.team3838.core.commands.NO_TIMEOUT
import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.subsystems.Subsystems
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem.RobotDirection
import frc.team3838.core.units.Angle
import frc.team3838.core.units.Inches
import frc.team3838.core.units.Speed


open class DriveStraightSetDistanceCommand(
        targetDistance: Inches,
        speed: Speed = Speed(Subsystems.getDriveTrainSubsystem().maxSpeedForAutonomous),
        robotDirection: RobotDirection = RobotDirection.Forward,
        name: String? = null,
        enableTuning: Boolean = false,
        timeoutDuration: TimeDuration = NO_TIMEOUT): DriveForSetDistanceCommand(targetDistance,
                                                                                Angle(0.0),
                                                                                speed,
                                                                                robotDirection,
                                                                                enableTuning,
                                                                                name,
                                                                                timeoutDuration)
{
    // Constructors for Java use
    @JvmOverloads
    constructor(targetDistanceInInches: Double,
                speed: Double = Subsystems.getDriveTrainSubsystem().maxSpeedForAutonomous,
                robotDirection: RobotDirection = RobotDirection.Forward,
                enableTuning: Boolean = false,
                name: String? = null,
                timeoutDuration: TimeDuration = NO_TIMEOUT): this(Inches(targetDistanceInInches),
                                                                  Speed(speed),
                                                                  robotDirection,
                                                                  name,
                                                                  enableTuning,
                                                                  timeoutDuration)

    constructor(targetDistanceInInches: Double, timeoutDuration: TimeDuration): 
            this(targetDistanceInInches, timeoutDuration = timeoutDuration, name = null)
    constructor(targetDistanceInInches: Double, robotDirection: RobotDirection = RobotDirection.Forward, timeoutDuration: TimeDuration): 
            this(targetDistanceInInches, robotDirection = robotDirection, timeoutDuration = timeoutDuration, name = null)
    constructor(targetDistanceInInches: Double, enableTuning: Boolean): 
            this(targetDistanceInInches, enableTuning = enableTuning, name = null)
}
