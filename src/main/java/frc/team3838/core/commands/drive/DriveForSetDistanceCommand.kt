package frc.team3838.core.commands.drive

import frc.team3838.core.commands.NO_TIMEOUT
import frc.team3838.core.config.time.PartialSeconds
import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.logging.LaconicLogger
import frc.team3838.core.meta.API
import frc.team3838.core.subsystems.Subsystems
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem.RobotDirection
import frc.team3838.core.units.Angle
import frc.team3838.core.units.Inches
import frc.team3838.core.units.Speed
import frc.team3838.core.units.compareTo
import frc.team3838.core.units.minus
import frc.team3838.core.units.plus
import frc.team3838.core.units.times
import frc.team3838.core.units.unaryMinus
import frc.team3838.core.utils.format
import kotlin.math.absoluteValue

@API
open class DriveForSetDistanceCommand constructor
(protected val targetDistance: Inches,
 targetAngle: Angle = Angle(0.0),
 speed: Speed = Speed(Subsystems.getDriveTrainSubsystem().maxSpeedForAutonomous),
 robotDirection: RobotDirection = RobotDirection.Forward,
 enableTuning: Boolean = false,
 name: String? = null,
 timeoutDuration: TimeDuration = NO_TIMEOUT
 ): DriveCommand(targetAngle, speed, robotDirection, enableTuning, name, timeoutDuration)
{
    @API
    constructor(robotDirection: RobotDirection,
                targetDistance: Inches,
                targetAngle: Angle = Angle(0.0),
                speed: Speed = Speed(Subsystems.getDriveTrainSubsystem().maxSpeedForAutonomous),
                enableTuning: Boolean = false,
                name: String? = null,
                timeoutDuration: TimeDuration = NO_TIMEOUT): this(targetDistance, targetAngle, speed, robotDirection, enableTuning, name, timeoutDuration)
    
    @API
    @JvmOverloads
    constructor(targetDistance: Double,
                robotDirection: RobotDirection,
                targetAngle: Double = 0.0,
                speed: Double = Subsystems.getDriveTrainSubsystem().maxSpeedForAutonomous,
                enableTuning: Boolean = false,
                name: String? = null,
                timeoutDuration: TimeDuration = NO_TIMEOUT): this(Inches(targetDistance), Angle(targetAngle), Speed(speed), robotDirection, enableTuning, name, timeoutDuration)

    @API
    protected var endDistance: Inches = Inches(0.0)

    @API
    protected val distanceRemainingLaconicLogger = LaconicLogger(logger, TimeDuration.ofSeconds(PartialSeconds.OneQuarterSecond))

    @API
    protected var lastLogSpeed = Speed(0.0)
    
    override fun initializeImpl()
    {
        if (canCommandRun())
        {
            driveTrainSubsystem.resetEncoders()
            super.initializeImpl()
            endDistance = calculateEndDistance()
            logger.info{"    $name with a target distance of $targetDistance"}
            logger.info{"    $name with an *INITIAL* end distance of $endDistance"}
            logger.info{"    $name with an target angle of $targetAngle"}
            logger.info{"    $name Encoder readings at Start: Left = ${driveTrainSubsystem.leftDistanceInInches.format()} Right = ${driveTrainSubsystem.rightDistanceInInches.format()}"}
        }
        
    }

    override fun executeImpl()
    {
        val remainingDistance = calculateRemainingDistance()
        distanceRemainingLaconicLogger.debug("$name Remaining distance: $remainingDistance")
        
        var theSpeed = when
        {
            remainingDistance > 24 -> speed
            remainingDistance > 18 -> speed * .75
            remainingDistance > 12 -> speed * .50
            remainingDistance >  8 -> speed * .25
            remainingDistance >  3 -> speed * .10
            remainingDistance <= 0 -> Speed(0.0)
            else                   -> speed // default value to satisfy when requirements
        }
        
        /*
        if (theSpeed > 0 && theSpeed < getMinSpeed())
        {
            theSpeed = getMinSpeed();
        }
         */
        
        if (theSpeed > 0 && theSpeed < driveTrainSubsystem.minSpeedForAutonomous)
        {
            theSpeed = Speed(driveTrainSubsystem.minSpeedForAutonomous)
        }
        
        if (logger.isDebugEnabled && lastLogSpeed != theSpeed)
        {
            logger.debug { "$name speed set to $theSpeed" }
            lastLogSpeed = theSpeed
        }
        driveStraight()
    }

    override fun isFinishedImpl(): Boolean
    {
//        return if (robotDirection == RobotDirection.Forward)
//                    driveTrainSubsystem.averageDistanceInInches >= endDistance
//                else
//                    driveTrainSubsystem.averageDistanceInInches <= endDistance
        return driveTrainSubsystem.averageDistanceInInches.absoluteValue >= endDistance
    }

    override fun shouldApplyCorrection(): Boolean
    {
        // Don't correct for the last 1/2 inch
        val remainingDistance: Inches = calculateRemainingDistance()
        return remainingDistance.absolute() >= 0.5 
    }

    @API
    protected fun calculateEndDistance(): Inches
    {
        var distance: Inches = targetDistance + driveTrainSubsystem.averageDistanceInInches
        if (robotDirection == RobotDirection.Reverse)
        {
            distance = -distance
        }
        return distance
    }
    
    @API 
    protected fun calculateRemainingDistance(): Inches = endDistance - driveTrainSubsystem.averageDistanceInInches
}

