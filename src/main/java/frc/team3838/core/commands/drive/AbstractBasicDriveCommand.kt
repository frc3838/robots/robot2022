package frc.team3838.core.commands.drive

import com.google.common.collect.ImmutableSet
import frc.team3838.core.commands.Abstract3838Command
import frc.team3838.core.commands.NO_TIMEOUT
import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.subsystems.I3838Subsystem
import frc.team3838.core.subsystems.drive.I3838DriveTrainSubsystem
import frc.team3838.game.subsystems.DriveTrainSubsystem


abstract class AbstractBasicDriveCommand(name: String? = null, timeoutDuration: TimeDuration = NO_TIMEOUT) 
    : Abstract3838Command(name, timeoutDuration)
{
    //@Suppress("USELESS_ELVIS")
    protected val driveTrainSubsystem: I3838DriveTrainSubsystem = DriveTrainSubsystem.getInstance()// ?: NoOpDriveTrainSubsystem.getInstance()

    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of(DriveTrainSubsystem.getInstance())

    override fun initializeImpl() { /* no op */ }

    override fun isFinishedImpl(): Boolean = true

    override fun endImpl() { /* no op */ } 
}