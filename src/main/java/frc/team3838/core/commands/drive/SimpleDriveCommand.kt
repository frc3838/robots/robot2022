package frc.team3838.core.commands.drive

import com.google.common.collect.ImmutableSet
import frc.team3838.core.commands.Abstract3838Command
import frc.team3838.core.config.time.PartialSeconds
import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.config.time.TimeDuration.Companion.of
import frc.team3838.core.logging.LaconicLogger
import frc.team3838.core.meta.API
import frc.team3838.core.subsystems.I3838Subsystem
import frc.team3838.core.subsystems.Subsystems
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem.RobotDirection
import frc.team3838.core.units.Speed
import frc.team3838.core.units.unaryMinus
import frc.team3838.core.utils.format
import java.util.concurrent.TimeUnit

/**
 * A VERY basic drive command that simple turns on the drive train at the specified speed.
 * Therefore, it **MUST** be given a timeout. For safety reasons, the timeout defaults to
 * 1/2 second in case it is not set. **NOTE:** This command does NOT use the navX to attempt
 * to drive straight.
 * @param speed The speed to drive at. Defaults to the value returned by [frc.team3838.core.subsystems.drive.I3838DriveTrainSubsystem.getMaxSpeedForAutonomous]
 * @param robotDirection The direction to drive. Defaults to [RobotDirection.Forward]
 * @param name The name of the command. If set to null (the default), the simple name of the class is used
 * @param timeoutDuration A timeout duration. Defaults to no 1/2 second
 */
@API
open class SimpleDriveCommand(
    protected var speed: Speed = Speed(Subsystems.getDriveTrainSubsystem().maxSpeedForAutonomous),
    protected val timeoutDuration: TimeDuration = TimeDuration.ofSeconds(PartialSeconds.HalfSecond),
    name: String? = null,
    protected val robotDirection: RobotDirection = RobotDirection.Forward
                             ) : Abstract3838Command(name, timeoutDuration)
{
    @API
    @JvmOverloads
    constructor(
                robotDirection: RobotDirection,
                speed: Double = Subsystems.getDriveTrainSubsystem().maxSpeedForAutonomous,
                name: String? = null,
                timeoutDuration: TimeDuration = TimeDuration.ofSeconds(PartialSeconds.HalfSecond)
               ): this(Speed(speed), timeoutDuration, name, robotDirection)

    private val timeoutDurationMs: Long

    init
    {
        timeoutDurationMs = timeoutDuration.toMillis()
    }

    private val commandCannotRunLaconicLogger = LaconicLogger(logger, of(30, TimeUnit.SECONDS))
    private val warningLaconicLogger = LaconicLogger(logger, of(30, TimeUnit.SECONDS))

    protected open val driveTrainSubsystem: Abstract3838DriveTrainSubsystem
         get() = Subsystems.getDriveTrainSubsystem()

    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of(driveTrainSubsystem)

    open var finishedFlag: Boolean = false

    open var startTimeMs = 0L

    /** Indicates of this drive command can run by checking that the navX is not null and that all requires subsystems are enabled. */
    protected open fun canCommandRun(): Boolean = areAllSubsystemsAreEnabled()
    
    
    override fun initializeImpl()
    {
        if (canCommandRun())
        {
            logger.info{"Initializing $name  Running at speed ${speed.value.format(3)} for $timeoutDuration "}
            finishedFlag = false
            startTimeMs = System.currentTimeMillis()
        }
        else
        {
            finishedFlag = true
            logger.warn{"COMMAND DISABLED: The $name command cannot and will not run. This is likely due to the navX or other subsystems not being fully enabled."}
        }
    }

    override fun executeImpl()
    {
        drive()
    }
    
    @API
    protected fun drive()
    {
        // based on code sample at http://wpilib.screenstepslive.com/s/4485/m/13809/l/599713-gyros-measuring-rotation-and-controlling-robot-driving-direction
        try
        {
            if (canCommandRun())
            {
                // REMEMBER the joysticks are negative for forward, and positive for reverse
                val theSpeed = if (robotDirection == RobotDirection.Forward) -this.speed else this.speed
                driveTrainSubsystem.driveRobotViaArcadeControlRaw(0.0, theSpeed.value)
            }
            else
            {
                commandCannotRunLaconicLogger.warn{ "COMMAND DISABLED: The $name command cannot and will not run. This is likely due to the navX or other subsystems not being fully enabled." }
            }
        }
        catch (t: Throwable)
        {
            warningLaconicLogger.warn(t){ "An Exception occurred in driveStraight() for $name. Cause Summary: $t"}
        }
    }

    override fun isFinishedImpl(): Boolean = canCommandRun() && finishedFlag && ((System.currentTimeMillis() - startTimeMs) >= timeoutDurationMs)

    override fun endImpl()
    {
        driveTrainSubsystem.stop()
        logger.info { "$name ended. Encoder Readings:  L = ${driveTrainSubsystem.leftDistanceInInches.format(3)} R = ${driveTrainSubsystem.rightDistanceInInches.format(3)}" }
    }
}