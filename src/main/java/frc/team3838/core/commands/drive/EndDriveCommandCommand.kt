package frc.team3838.core.commands.drive

import frc.team3838.core.commands.AbstractInstant3838NoSubsystemCommand
import frc.team3838.core.meta.API

/**
 * A command that when executed ends a drive command that is in process by
 * setting its [DriveCommand.finishedFlag] to `true`.
 */
@API
open class EndDriveCommandCommand(private val driveCommand: DriveCommand,
                                  name:String? = null): AbstractInstant3838NoSubsystemCommand(name)
{
    override fun executeImpl()
    {
        driveCommand.finishedFlag = true
        logger.info { "Ending ${driveCommand.name} via $name" }
    }
}