package frc.team3838.core.subsystems;

import com.google.common.collect.ImmutableList;



public abstract class Abstract3838SingleUsbCameraSubsystem extends Abstract3838UsbCameraBase
{

    protected Abstract3838SingleUsbCameraSubsystem()
    {
        super();
    }


    protected Abstract3838SingleUsbCameraSubsystem(int deviceNumber)
    {
        super(ImmutableList.of(new CameraConfig(deviceNumber)));
    }


    @Override
    protected void initDefaultCommandImpl() throws Exception
    {
        // no op
    }
    
}

