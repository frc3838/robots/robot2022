package frc.team3838.core.subsystems;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableList;

import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.cscore.UsbCamera;
import edu.wpi.first.cscore.VideoSink;
import edu.wpi.first.cscore.VideoSource;
import edu.wpi.first.cscore.VideoSource.ConnectionStrategy;



public abstract class Abstract3838TwoUsbCameraSubsystem extends Abstract3838UsbCameraBase
{
    private final Logger logger = LoggerFactory.getLogger(getClass());


    protected VideoSink server;
    protected UsbCamera primaryCamera;
    protected UsbCamera secondaryCamera;
    protected final CameraConfig primaryCameraConfig;
    protected final CameraConfig secondaryCameraConfig;


    protected Abstract3838TwoUsbCameraSubsystem(CameraConfig primaryCameraConfig, CameraConfig secondaryCameraConfig)
    {
        super(ImmutableList.of(primaryCameraConfig, secondaryCameraConfig));
        this.primaryCameraConfig = primaryCameraConfig;
        this.secondaryCameraConfig = secondaryCameraConfig;
    }

    @Override
    protected void initSubsystem() throws Exception
    {

        primaryCamera = CameraServer.startAutomaticCapture(primaryCameraConfig.deviceNumber);
        secondaryCamera = CameraServer.startAutomaticCapture(secondaryCameraConfig.deviceNumber);
        primaryCamera.setConnectionStrategy(ConnectionStrategy.kKeepOpen);
        secondaryCamera.setConnectionStrategy(ConnectionStrategy.kKeepOpen);

        if ((primaryCamera == null) || (secondaryCamera == null))
        {
            logger.warn("Cameras did not initialize properly. One or both are null. Primary = '{}'  Secondary = '{}",
                        ((primaryCamera == null) ? "null" : (primaryCamera.getInfo().name + " on " + primaryCamera.getInfo().dev)),
                        ((secondaryCamera == null) ? "null" : (secondaryCamera.getInfo().name + " on " + secondaryCamera.getInfo().dev)));
        }
        server = CameraServer.getServer();
        server.setSource(primaryCamera);
    }

//    @Override
//    protected void initSubsystem() throws Exception
//    {
//        super.initSubsystem();
//
//        for (UsbCamera usbCamera : cameras)
//        {
//            CameraServer.getInstance().startAutomaticCapture(usbCamera);
//            usbCamera.setConnectionStrategy(ConnectionStrategy.kKeepOpen);
//            final int deviceNumber = usbCamera.getInfo().dev;
//            switch (deviceNumber)
//            {
//                case 0:
//                    primaryCamera = usbCamera;
//                    break;
//                case 1:
//                    secondaryCamera = usbCamera;
//                    break;
//            }
//        }
//
//        if ((primaryCamera == null) || (secondaryCamera == null))
//        {
//            logger.warn("Cameras did not initialize properly. One or both are null. Primary = '{}'  Secondary = '{}",
//                        ((primaryCamera == null) ? "null" : (primaryCamera.getInfo().name + " on " + primaryCamera.getInfo().dev)),
//                        ((secondaryCamera == null) ? "null" : (secondaryCamera.getInfo().name + " on " + secondaryCamera.getInfo().dev)));
//        }
//        server = CameraServer.getInstance().getServer();
//        server.setSource(primaryCamera);
//    }


    public void toggleCamera()
    {
        final VideoSource currentSource = server.getSource();
        if (primaryCamera.equals(currentSource))
        {
            switchToSecondaryCamera();
        }
        else
        {
            switchToPrimaryCamera();
        }
    }

    public void switchToPrimaryCamera()
    {
        server.setSource(primaryCamera);
    }


    public void switchToSecondaryCamera()
    {
        server.setSource(secondaryCamera);
    }

}
