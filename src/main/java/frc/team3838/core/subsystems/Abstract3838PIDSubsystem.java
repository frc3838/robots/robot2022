package frc.team3838.core.subsystems;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.command.PIDSubsystem;



public abstract class Abstract3838PIDSubsystem extends PIDSubsystem implements I3838Subsystem
{
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    private static final Logger LOG = LoggerFactory.getLogger(Abstract3838PIDSubsystem.class);

    protected boolean enabled = false;

    private boolean hasBeenInitialized = false;

    protected Abstract3838PIDSubsystem(String name, double p, double i, double d)
    {
        super(name, p, i, d);
    }


    protected Abstract3838PIDSubsystem(String name, double p, double i, double d, double f)
    {
        super(name, p, i, d, f);
    }


    protected Abstract3838PIDSubsystem(String name, double p, double i, double d, double f, double period)
    {
        super(name, p, i, d, f, period);
    }


    protected Abstract3838PIDSubsystem(double p, double i, double d)
    {
        super(p, i, d);
    }


    protected Abstract3838PIDSubsystem(double p, double i, double d, double period, double f)
    {
        super(p, i, d, period, f);
    }


    protected Abstract3838PIDSubsystem(double p, double i, double d, double period)
    {
        super(p, i, d, period);
    }


    protected abstract void initDefaultCommandImpl() throws Exception;

    @SuppressWarnings("Duplicates")
    @Override
    public final void initializeTheSubsystem()
    {
        if (!hasBeenInitialized)
        {
            hasBeenInitialized = true;
            final Class<? extends Abstract3838PIDSubsystem> clazz = getClass();
            logger.debug("Checking if '{}' is enabled", clazz.getSimpleName());
            enabled = Subsystems.checkIfEnabled(clazz);
            if (enabled)
            {
                try
                {
                    LOG.info("Initializing '{}' by calling its initSubsystem() method", getName());
                    initSubsystem();
                }
                catch (Exception e)
                {
                    LOG.warn("An exception occurred when initializing {}. It will be DISABLED. Cause summary: {}", getName(), e.toString(), e);
                    disableSubsystem();
                }
            }
            else
            {
                LOG.warn("{} is set as disabled, and will not be fully initialized.", getName());
            }
        }
    }


    @Override
    protected void initDefaultCommand()
    {
        if (isEnabled())
        {
            try
            {
                initDefaultCommandImpl();
            }
            catch (Exception e)
            {
                logger.warn("Unable to initialize default command for {}. Cause Summary: {}", getName(), e.toString(), e);
            }
        }

    }

    public boolean isEnabled() { return enabled; }


    @Override
    public void disableSubsystem()
    {
        LOG.warn("{} is being disabled.", getName());
        enabled = false;
    }


    /**
     * Called in the constructor if the subsystem is enabled.
     * If a subsystem does not have any initialization work to do during construction, simple create
     * a no op (no operation) implementation. That is, an empty method. implementations can throw
     * Exceptions. The caller in the super class, {@link Abstract3838PIDSubsystem} will catch the
     * Exception and then set the subsystem to disabled.
     *
     * @throws Exception if any issues occur during initialization. The super constructor
     *                   in {@link Abstract3838PIDSubsystem} will catch the Exception and then set the subsystem to
     *                   disabled.
     */
    protected abstract void initSubsystem() throws Exception;
}
