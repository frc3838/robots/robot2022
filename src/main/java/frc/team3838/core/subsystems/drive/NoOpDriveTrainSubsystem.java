package frc.team3838.core.subsystems.drive;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import frc.team3838.core.hardware.EncoderConfig;
import frc.team3838.core.hardware.NoOpMotorController;
import frc.team3838.core.meta.API;



//TODO fix Subsystems so this can be present.
@API
public class NoOpDriveTrainSubsystem extends Abstract3838DriveTrainSubsystem
{
    private NoOpDriveTrainSubsystem()
    {
        super(new NoOpMotorController(),
              new NoOpMotorController(),
              DriveTrainControlConfigImpl.getNoOpDriveTrainControlConfig());
    }


    @Override
    protected void initSubsystem() throws Exception
    {
        // no op
    }


    @Nullable
    @Override
    protected EncoderConfig getLeftEncoderConfig()
    {
        return null;
    }


    @Nullable
    @Override
    protected EncoderConfig getRightEncoderConfig()
    {
        return null;
    }
    
    
    @Override
    public double getMinSpeedForAutonomous() { return 0.0; }
    
    
    @Override
    public double getMaxSpeedForAutonomous() { return 0.0; }
    
     /**
     * *** THE SINGLETON FILED NEEDS TO BE LAST FIELD DEFINED! ****<br /><br />
     * So we put it at the end of the class to be sure it is the last field declared.
     * All static fields/properties need to be initialized prior to the constructor running.
     * If the singleton field (or a static init block) is put in the middle of the field declarations
     * (which can happen when the IDE automatically creates new Fields at the end of the list) a *very*
     * subtle and very hard to track down bug can be introduced. We were bitten by this once during competition.
     */
    @Nonnull
    private static final NoOpDriveTrainSubsystem singleton = new NoOpDriveTrainSubsystem();


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new ${NAME}()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     ${NAME} subsystem = new ${NAME}();
     * </pre>
     * do this:
     * <pre>
     *     ${NAME} subsystem = ${NAME}.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    @Nonnull
    public static NoOpDriveTrainSubsystem getInstance() {return singleton;}
}
