package frc.team3838.core.config.time;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;



@SuppressWarnings("unused")
public enum PartialSeconds
{
    /** No partial value (i.e. 0 milliseconds). */
    NoPartial
        {
            @Override
            public long toMills()
            {
                return 0;
            }
        },
    /** 1/16 of a second (63 milliseconds). */
    OneSixteenthSecond
        {
            @Override
            public long toMills()
            {
                return 63;
            }
        },
    /** 1/8 of a second (125 milliseconds). */
    OneEighthSecond
        {
            @Override
            public long toMills()
            {
                return 125;
            }
        },
    /** 3/16 of a second (188 milliseconds). */
    ThreeSixteenthSecond
        {
            @Override
            public long toMills()
            {
                return 188;
            }
        },
    /** 1/4 of a second (250 milliseconds). */
    OneQuarterSecond
        {
            @Override
            public long toMills()
            {
                return 250;
            }
        },
    /** 5/16 of a second (313 milliseconds). */
    FiveSixteenthSecond
        {
            @Override
            public long toMills()
            {
                return 313;
            }
        },
    /** 3/8 of a second (375 milliseconds). */
    ThreeEightsSecond
        {
            @Override
            public long toMills()
            {
                return 375;
            }
        },
    /** 7/16 of a second (438 milliseconds). */
    SevenSixteenthSecond
        {
            @Override
            public long toMills()
            {
                return 438;
            }
    },
    /** 1/2 of a second (500 milliseconds). */
    HalfSecond
        {
            @Override
            public long toMills()
            {
                return 500;
            }
        },
    /** 9/16 of a second (563 milliseconds). */
    NineSixteenthSecond
        {
            @Override
            public long toMills()
            {
                return 563;
            }
        },
    /** 5/8 of a second (625 milliseconds). */
    FiveEightsSecond
        {
            @Override
            public long toMills()
            {
                return 625;
            }
        },
    /** 11/16 of a second (688 milliseconds). */
    ElevenSixteenthSecond
        {
            @Override
            public long toMills()
            {
                return 688;
            }
        },
    /** 3/4 of a second (750 milliseconds). */
    ThreeQuartersSecond
        {
            @Override
            public long toMills()
            {
                return 750;
            }
        },
    /** 13/16 of a second (813 milliseconds). */
    ThirteenSixteenthSecond
        {
            @Override
            public long toMills()
            {
                return 813;
            }
        },
    /** 7/8 of a second (875 milliseconds). */
    SevenEightsSecond
        {
            @Override
            public long toMills()
            {
                return 875;
            }
        },
    /** 15/16 of a second (938 milliseconds). */
    FifteenSixteenthSecond
        {
            @Override
            public long toMills()
            {
                return 938;
            }
        },
    /**  A full second (1,000 milliseconds). */
    FullSecond
        {
            @Override
            public long toMills()
            {
                return 1000;
            }
        },
        ;


    public abstract long toMills();
    
    public static long sumToMillis(PartialSeconds partialSeconds)
    {
        return sumToMillis(0, partialSeconds);
    }
    
    public static long sumToMillis(long seconds, PartialSeconds... partialSeconds)
    {
        return TimeUnit.SECONDS.toMillis(seconds) + Arrays.stream(partialSeconds).mapToLong(PartialSeconds::toMills).sum();
    }
}
