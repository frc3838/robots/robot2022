= FRC 3838 - Robot 2022
v1.0, 2022-02-25
:toc:
:icons: font
:sectanchors:
:sectlinks:

== Joystick Button Assignments


image::Joystick-Mappings.svg[]

(If in IntelliJ IDEA, you may need to open the image directly in the project's root folder)


== Reference Documentation

* https://docs.wpilib.org/en/stable/docs/yearly-overview/yearly-changelog.html[New for 2022 WPILib Changelog]
* https://first.wpi.edu/wpilib/allwpilib/docs/release/java/index.html[WPILib Javadocs]
* https://codedocs.revrobotics.com/java/index.html[RevRobotics Java docs]
    ** https://codedocs.revrobotics.com/java/com/revrobotics/cansparkmax[CANSparkMax]
* https://drive.google.com/file/d/1c3IkFHGkUmMh2eFiwPF-YzwYw0eePHYS/view?usp=sharing[HeadFirst Design Patterns, Chapter 10 State Pattern]
    ** https://drive.google.com/file/d/1GvDAN7Ig0I2zp7c4u9rqmQuK05TzlZ8O/view?usp=sharing[Sample Code]
* https://www.kauailabs.com/public_files/navx-mxp/apidocs/java/com/kauailabs/navx/frc/package-summary.html[navX Java Docs]
    ** https://pdocs.kauailabs.com/navx-mxp/software/roborio-libraries/java/[navX Java Software Info]
    ** https://www.kauailabs.com/support/navx-mxp/kb/faq.php?id=48[navX Update Firmware instructions]


== Development Notes

.Development Notes
[source,text]
----
Drive System:   SparkMax NEO, CAN ID 20, PDP Channel 12, Left Side Intake/Back
Drive System:   SparkMax NEO, CAN ID 21, PDP Channel 13, Left Side Shooter/Front
Drive System:   SparkMax NEO, CAN ID 22, PDP Channel 14, Right Side Intake/Back
Drive System:   SparkMax NEO, CAN ID 23, PDP Channel 15, Right Side Shooter/Front

42 counts per rev at the motor, and a 6 inch wheel, plus the gear ratio


Shooter System:  SparkMax NEO550 5:1 Gearbox, CAN ID 30, PDP ?,  Lift / Firing Motor / Cam
Shooter System:  Talon SRX with 775Pro Motor & Talon Tach, CAN ID ?, PDP ?, Top First Wheel Motor
Shooter System:  Talon SRX with 775Pro Motor & Talon Tach, CAN ID ?, PDP ?, Bottom Second Wheel Motor


Intake System:  Talon SRX with Cim Motor, CAN ID 10, PDP ?, Raise and Lower Intake Winch
Intake System:  Talon SRX with Bag Motor, CAN ID 11, PDP ?, Intake Motor
Intake System:  Talon SRX with Bag Motor, CAN ID 12, PDP ?, Transport Motor


The breech can only hold one ball.


https://store.ctr-electronics.com/talon-tach/
https://andymark-weblinc.netdna-ssl.com/media/W1siZiIsIjIwMTkvMDcvMTIvMTIvNTcvMTEvZjQ4Y2VlY2QtMTJiOS00N2Q0LTgxMmQtNTI2YmNhMGQxYmY5L1RhbG9uIFRhY2ggVXNlcidzIEd1aWRlLnBkZiJdXQ/Talon%20Tach%20User%27s%20Guide.pdf?sha=fab5a428119ceb13

https://www.chiefdelphi.com/t/talon-tach-programming/375666
https://github.com/CrossTheRoadElec/Phoenix-Examples-Languages/blob/master/Java%20General/Tachometer/src/main/java/frc/robot/Robot.java


Shoot sequence start the
210 counts

Have to make sure the shooter is down




When the arm is raised

Intake has two positions, up and down
Sensors to provide up and down

When arm is down we start both the intake motor and transport motor.

The ball will go from the intake to the transport


Need to be able to eject a ball

Ball in the b


Intake/Collector

Transport Talon SRX

Shooter

Winch


For climb need a drive at a fixed speed


Autonomous

Sequence 1

1. Fire at specific velocity
2. Lower intake
3. Move fixed distance and/or to take the ball
4. Move back to a known distance away
4. Then shoot the ball


Tyes

Sat  Feb  26 -
Tu   March 1 - Calibrate Shooter Low
Th   March 3 - Calibrate Shooter Low
Sat  March 5 - we program autonomous
Tues March 8 - driver practice


CU    Intake "Collector Arms UP"    with IR sensor DIO  0  (i.e. Game start position)
CD    Intake "Collector Arms DOWN"  with IR sensor DIO  1  (i.e. Collecting on floor position)
TI    Transporter "Ball IN"	        with IR sensor DIO  2  (i.e.  Ball transferred from collector to Transporter)
TU    Transporter "Ball UP"         with IR sensor DIO  3  (i.e. Ball @ top of the transport before falling into Shooter)
SI    Shooter     "Ball IN"         with IR sensor DIO  4  (i.e. Ball is in chamber ready to fire) (edited)
----


Notes

----
16:19:21.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3481.0  lowerVel = 3502.0  upper RPM: 7518.960000000001   RPM: 7564.320000000001  upper.get: 0.00  lower.get: 0.00
16:19:23.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3481.0  lowerVel = 3508.0  upper RPM: 7518.960000000001   RPM: 7577.280000000001  upper.get: 0.00  lower.get: 0.00
16:19:25.356 EST TRACE f.t.g.s.Shooter2022Subsystem$initDefaultCommandImpl$1 :: .execute() running
16:19:25.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3483.0  lowerVel = 3501.0  upper RPM: 7523.280000000001   RPM: 7562.160000000001  upper.get: 0.00  lower.get: 0.00
16:19:27.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3485.0  lowerVel = 3501.0  upper RPM: 7527.6   RPM: 7562.160000000001  upper.get: 0.00  lower.get: 0.00
16:19:29.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3485.0  lowerVel = 3500.0  upper RPM: 7527.6   RPM: 7560.000000000001  upper.get: 0.00  lower.get: 0.00
16:19:30.356 EST TRACE f.t.g.s.Shooter2022Subsystem$initDefaultCommandImpl$1 :: .execute() running
16:19:31.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3486.0  lowerVel = 3499.0  upper RPM: 7529.76   RPM: 7557.84  upper.get: 0.00  lower.get: 0.00
16:19:33.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3483.0  lowerVel = 3498.0  upper RPM: 7523.280000000001   RPM: 7555.68  upper.get: 0.00  lower.get: 0.00
16:19:35.356 EST TRACE f.t.g.s.Shooter2022Subsystem$initDefaultCommandImpl$1 :: .execute() running
16:19:35.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3486.0  lowerVel = 3500.0  upper RPM: 7529.76   RPM: 7560.000000000001  upper.get: 0.00  lower.get: 0.00
16:19:37.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3484.0  lowerVel = 3500.0  upper RPM: 7525.4400000000005   RPM: 7560.000000000001  upper.get: 0.00  lower.get: 0.00
16:19:39.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3483.0  lowerVel = 3500.0  upper RPM: 7523.280000000001   RPM: 7560.000000000001  upper.get: 0.00  lower.get: 0.00
16:19:40.356 EST TRACE f.t.g.s.Shooter2022Subsystem$initDefaultCommandImpl$1 :: .execute() running
16:19:41.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3482.0  lowerVel = 3501.0  upper RPM: 7521.120000000001   RPM: 7562.160000000001  upper.get: 0.00  lower.get: 0.00
16:19:43.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3487.0  lowerVel = 3500.0  upper RPM: 7531.92   RPM: 7560.000000000001  upper.get: 0.00  lower.get: 0.00
16:19:45.356 EST TRACE f.t.g.s.Shooter2022Subsystem$initDefaultCommandImpl$1 :: .execute() running
16:19:45.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3487.0  lowerVel = 3499.0  upper RPM: 7531.92   RPM: 7557.84  upper.get: 0.00  lower.get: 0.00
16:19:47.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3481.0  lowerVel = 3501.0  upper RPM: 7518.960000000001   RPM: 7562.160000000001  upper.get: 0.00  lower.get: 0.00
16:19:49.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3485.0  lowerVel = 3497.0  upper RPM: 7527.6   RPM: 7553.52  upper.get: 0.00  lower.get: 0.00
16:19:50.356 EST TRACE f.t.g.s.Shooter2022Subsystem$initDefaultCommandImpl$1 :: .execute() running
16:19:51.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3484.0  lowerVel = 3499.0  upper RPM: 7525.4400000000005   RPM: 7557.84  upper.get: 0.00  lower.get: 0.00
16:19:53.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3486.0  lowerVel = 3498.0  upper RPM: 7529.76   RPM: 7555.68  upper.get: 0.00  lower.get: 0.00
16:19:55.356 EST TRACE f.t.g.s.Shooter2022Subsystem$initDefaultCommandImpl$1 :: .execute() running
16:19:55.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3482.0  lowerVel = 3500.0  upper RPM: 7521.120000000001   RPM: 7560.000000000001  upper.get: 0.00  lower.get: 0.00
16:19:57.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3482.0  lowerVel = 3504.0  upper RPM: 7521.120000000001   RPM: 7568.64  upper.get: 0.00  lower.get: 0.00
16:19:59.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3480.0  lowerVel = 3500.0  upper RPM: 7516.8   RPM: 7560.000000000001  upper.get: 0.00  lower.get: 0.00
16:20:00.356 EST TRACE f.t.g.s.Shooter2022Subsystem$initDefaultCommandImpl$1 :: .execute() running
16:20:01.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3484.0  lowerVel = 3501.0  upper RPM: 7525.4400000000005   RPM: 7562.160000000001  upper.get: 0.00  lower.get: 0.00
16:20:03.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3481.0  lowerVel = 3500.0  upper RPM: 7518.960000000001   RPM: 7560.000000000001  upper.get: 0.00  lower.get: 0.00
16:20:04.376 EST INFO  com.kauailabs.navx.frc.Tracer            :: navX-Sensor DISCONNECTED!!!.
16:20:05.356 EST TRACE f.t.g.s.Shooter2022Subsystem$initDefaultCommandImpl$1 :: .execute() running
16:20:05.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3478.0  lowerVel = 3501.0  upper RPM: 7512.4800000000005   RPM: 7562.160000000001  upper.get: 0.00  lower.get: 0.00
16:20:05.392 EST INFO  com.kauailabs.navx.frc.Tracer            :: navX-Sensor Connected.
16:20:07.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3480.0  lowerVel = 3500.0  upper RPM: 7516.8   RPM: 7560.000000000001  upper.get: 0.00  lower.get: 0.00
16:20:09.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3479.0  lowerVel = 3496.0  upper RPM: 7514.64   RPM: 7551.360000000001  upper.get: 0.00  lower.get: 0.00
16:20:10.356 EST TRACE f.t.g.s.Shooter2022Subsystem$initDefaultCommandImpl$1 :: .execute() running
16:20:11.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3480.0  lowerVel = 3499.0  upper RPM: 7516.8   RPM: 7557.84  upper.get: 0.00  lower.get: 0.00
16:20:13.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3482.0  lowerVel = 3501.0  upper RPM: 7521.120000000001   RPM: 7562.160000000001  upper.get: 0.00  lower.get: 0.00
16:20:15.356 EST TRACE f.t.g.s.Shooter2022Subsystem$initDefaultCommandImpl$1 :: .execute() running
16:20:15.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3482.0  lowerVel = 3500.0  upper RPM: 7521.120000000001   RPM: 7560.000000000001  upper.get: 0.00  lower.get: 0.00
16:20:17.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3480.0  lowerVel = 3501.0  upper RPM: 7516.8   RPM: 7562.160000000001  upper.get: 0.00  lower.get: 0.00
16:20:19.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3482.0  lowerVel = 3500.0  upper RPM: 7521.120000000001   RPM: 7560.000000000001  upper.get: 0.00  lower.get: 0.00
16:20:20.356 EST TRACE f.t.g.s.Shooter2022Subsystem$initDefaultCommandImpl$1 :: .execute() running
16:20:21.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3482.0  lowerVel = 3501.0  upper RPM: 7521.120000000001   RPM: 7562.160000000001  upper.get: 0.00  lower.get: 0.00
16:20:23.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3482.0  lowerVel = 3502.0  upper RPM: 7521.120000000001   RPM: 7564.320000000001  upper.get: 0.00  lower.get: 0.00
16:20:25.356 EST TRACE f.t.g.s.Shooter2022Subsystem$initDefaultCommandImpl$1 :: .execute() running
16:20:25.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3488.0  lowerVel = 3499.0  upper RPM: 7534.080000000001   RPM: 7557.84  upper.get: 0.00  lower.get: 0.00
16:20:27.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3480.0  lowerVel = 3501.0  upper RPM: 7516.8   RPM: 7562.160000000001  upper.get: 0.00  lower.get: 0.00
16:20:29.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3478.0  lowerVel = 3502.0  upper RPM: 7512.4800000000005   RPM: 7564.320000000001  upper.get: 0.00  lower.get: 0.00
16:20:30.356 EST TRACE f.t.g.s.Shooter2022Subsystem$initDefaultCommandImpl$1 :: .execute() running
16:20:31.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3483.0  lowerVel = 3499.0  upper RPM: 7523.280000000001   RPM: 7557.84  upper.get: 0.00  lower.get: 0.00
16:20:33.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3483.0  lowerVel = 3500.0  upper RPM: 7523.280000000001   RPM: 7560.000000000001  upper.get: 0.00  lower.get: 0.00
16:20:35.356 EST TRACE f.t.g.s.Shooter2022Subsystem$initDefaultCommandImpl$1 :: .execute() running
16:20:35.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3485.0  lowerVel = 3503.0  upper RPM: 7527.6   RPM: 7566.4800000000005  upper.get: 0.00  lower.get: 0.00
16:20:37.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3484.0  lowerVel = 3501.0  upper RPM: 7525.4400000000005   RPM: 7562.160000000001  upper.get: 0.00  lower.get: 0.00
16:20:39.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3480.0  lowerVel = 3500.0  upper RPM: 7516.8   RPM: 7560.000000000001  upper.get: 0.00  lower.get: 0.00
16:20:40.356 EST TRACE f.t.g.s.Shooter2022Subsystem$initDefaultCommandImpl$1 :: .execute() running
16:20:41.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3480.0  lowerVel = 3498.0  upper RPM: 7516.8   RPM: 7555.68  upper.get: 0.00  lower.get: 0.00
16:20:43.376 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3481.0  lowerVel = 3499.0  upper RPM: 7518.960000000001   RPM: 7557.84  upper.get: 0.00  lower.get: 0.00
16:20:45.361 EST TRACE f.t.g.s.Shooter2022Subsystem$initDefaultCommandImpl$1 :: .execute() running
16:20:45.382 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3481.0  lowerVel = 3500.0  upper RPM: 7518.960000000001   RPM: 7560.000000000001  upper.get: 0.00  lower.get: 0.00
16:20:47.381 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3486.0  lowerVel = 3499.0  upper RPM: 7529.76   RPM: 7557.84  upper.get: 0.00  lower.get: 0.00
16:20:49.386 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3487.0  lowerVel = 3499.0  upper RPM: 7531.92   RPM: 7557.84  upper.get: 0.00  lower.get: 0.00
16:20:50.361 EST TRACE f.t.g.s.Shooter2022Subsystem$initDefaultCommandImpl$1 :: .execute() running
16:20:51.391 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3486.0  lowerVel = 3497.0  upper RPM: 7529.76   RPM: 7553.52  upper.get: 0.00  lower.get: 0.00
16:20:53.391 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3488.0  lowerVel = 3501.0  upper RPM: 7534.080000000001   RPM: 7562.160000000001  upper.get: 0.00  lower.get: 0.00
16:20:55.366 EST TRACE f.t.g.s.Shooter2022Subsystem$initDefaultCommandImpl$1 :: .execute() running
16:20:55.391 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3483.0  lowerVel = 3500.0  upper RPM: 7523.280000000001   RPM: 7560.000000000001  upper.get: 0.00  lower.get: 0.00
16:20:57.396 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3484.0  lowerVel = 3500.0  upper RPM: 7525.4400000000005   RPM: 7560.000000000001  upper.get: 0.00  lower.get: 0.00
16:20:59.396 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3483.0  lowerVel = 3498.0  upper RPM: 7523.280000000001   RPM: 7555.68  upper.get: 0.00  lower.get: 0.00
16:21:00.366 EST TRACE f.t.g.s.Shooter2022Subsystem$initDefaultCommandImpl$1 :: .execute() running
16:21:01.396 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3484.0  lowerVel = 3501.0  upper RPM: 7525.4400000000005   RPM: 7562.160000000001  upper.get: 0.00  lower.get: 0.00
16:21:03.396 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3488.0  lowerVel = 3503.0  upper RPM: 7534.080000000001   RPM: 7566.4800000000005  upper.get: 0.00  lower.get: 0.00
16:21:05.366 EST TRACE f.t.g.s.Shooter2022Subsystem$initDefaultCommandImpl$1 :: .execute() running
16:21:05.396 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3486.0  lowerVel = 3502.0  upper RPM: 7529.76   RPM: 7564.320000000001  upper.get: 0.00  lower.get: 0.00
16:21:07.398 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3488.0  lowerVel = 3502.0  upper RPM: 7534.080000000001   RPM: 7564.320000000001  upper.get: 0.00  lower.get: 0.00
16:21:09.401 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3486.0  lowerVel = 3502.0  upper RPM: 7529.76   RPM: 7564.320000000001  upper.get: 0.00  lower.get: 0.00
16:21:10.366 EST TRACE f.t.g.s.Shooter2022Subsystem$initDefaultCommandImpl$1 :: .execute() running
16:21:11.401 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3484.0  lowerVel = 3501.0  upper RPM: 7525.4400000000005   RPM: 7562.160000000001  upper.get: 0.00  lower.get: 0.00
16:21:13.402 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3485.0  lowerVel = 3501.0  upper RPM: 7527.6   RPM: 7562.160000000001  upper.get: 0.00  lower.get: 0.00
16:21:15.366 EST TRACE f.t.g.s.Shooter2022Subsystem$initDefaultCommandImpl$1 :: .execute() running
16:21:15.406 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3484.0  lowerVel = 3500.0  upper RPM: 7525.4400000000005   RPM: 7560.000000000001  upper.get: 0.00  lower.get: 0.00
16:21:17.406 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3489.0  lowerVel = 3500.0  upper RPM: 7536.240000000001   RPM: 7560.000000000001  upper.get: 0.00  lower.get: 0.00
16:21:19.406 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3483.0  lowerVel = 3502.0  upper RPM: 7523.280000000001   RPM: 7564.320000000001  upper.get: 0.00  lower.get: 0.00
16:21:20.366 EST TRACE f.t.g.s.Shooter2022Subsystem$initDefaultCommandImpl$1 :: .execute() running
16:21:21.406 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3483.0  lowerVel = 3499.0  upper RPM: 7523.280000000001   RPM: 7557.84  upper.get: 0.00  lower.get: 0.00
16:21:23.406 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3482.0  lowerVel = 3501.0  upper RPM: 7521.120000000001   RPM: 7562.160000000001  upper.get: 0.00  lower.get: 0.00
16:21:25.366 EST TRACE f.t.g.s.Shooter2022Subsystem$initDefaultCommandImpl$1 :: .execute() running
16:21:25.406 EST DEBUG f.t.c.subsystems.Abstract3838Subsystem   :: upperVel = 3483.0  lowerVel = 3500.0  upper RPM: 7523.280000000001   RPM: 7560.000000000001  upper.get: 0.00  lower.get: 0.00
----